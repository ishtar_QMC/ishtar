# ISHTAR
[![license](https://img.shields.io/badge/license-APACHE2-green)](https://gitlab.com/esp42/chilli/chilli_jl/-/blob/main/LICENSE)
[![language](https://img.shields.io/badge/language-C++-blue)](https://cplusplus.com/)
[![version](https://img.shields.io/badge/version-0.0.1-green)]()
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10497098.svg)](https://doi.org/10.5281/zenodo.10497098)

**I**maginary-time **S**tochastic **H**igh-performance **T**ool for **A**b initio **R**esearch (ISHTAR)

The ISHTAR code offers a vast variarity   
of path-integral Monte-Carlo (PIMC) implementations   
with the possibility to calculate observables    
like the static structure factor,   
the static dielectric function,   
the local field correction and many more.   

# Documentation 

The [ISHTAR documentation](https://ishtar_qmc.gitlab.io/ishtar/) is available online. 

# Implementation 

The ISHTAR code is written in C++ (C++17 standard) and 
utilizes the following packages 

- BOOST 
- GSL 
- HDF5

# Installation :play_pause:

ISHTAR utilizes CMake for the build process. 

A basic installation can be build using the following 
steps 
```bash 
git clone https://gitlab.com/ishtar_QMC/ishtar
cd ishtar/src/
mkdir build 
cd build 
cmake .. 
make 
```

# Workflow 

To run a ISHTAR calculation you need to prepare a input file, i.e.,    
[simulation.cfg](https://gitlab.com/ishtar_QMC/ishtar/-/blob/main/src/simulation.cfg?ref_type=heads).   
You need to execute the ISHTAR binary in the folder   
where your [simulation.cfg](https://gitlab.com/ishtar_QMC/ishtar/-/blob/main/src/simulation.cfg?ref_type=heads) is situated.   

# Authors 

- Tobias Dornheim (group leader, main developer)
- Maximilian Böhme
- Sebastian Schwalbe (HDF5) 

# Project Status :green_book:

```
"With great power comes great responsibilty."
Spider-Man 
```    


NOTABENE: The code is under development,    
thus use it with great care :exclamation:

The code is currently in its v0.0.1 version.   
Following code releases will covering examples    
and respective documentations.    

Please, don't heasitate to contact   
the authors if you encouter any problem.  


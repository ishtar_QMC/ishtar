#pragma once
/*
 * Contains class Parameters 
 * 
 */

#include <string>
#include <memory>

#include "particles/pair_action_lookup_table.hpp"
#include "particles/ion_container.hpp"

bool exist_check(const std::string& name)
{
	std::ifstream f(name.c_str());
	return f.good();
}

class Parameters
{
public:
	


	//##################################################################################################################################
	// ### Algorithmic parameters:
	//##################################################################################################################################

	int omp_threads; // number of OMP threads, for single-node parallelization
	int seed; // seed for the RNG

	bool PBC_detailed_balance; // Explicitly take into account the fact that you cannot cancel from a periodic sum of Gaussians in PBC?



	

	int video_width; // width (in pixel) for PBPIMC 3D video
	double video_SR; // Sphere radius for PBPIMC 3D video
	bool video_label;

	
	
	// Observables

	bool measure_force_on_ions; // for PBC systems
	bool measure_density_3D; // for PBC systems



	bool c2p_control; // measure the center-two particle CF (Thomsen and Bonitz, PRE 2015)
	int c2p_buffer; // buffer for the c2p observable


	bool monopole_control; // measure the monopole imaginary-time correlation function (breathing mode!)?
	bool ITCF_control;     // measure ITCF and related observables (PBC)?
	bool sk_control;       // measure the static structure factor and related observables?
	bool cf_control;       // measure pair correlation functions in real space?
	bool permutation_control; // measure a histogram of sampled permutations?
	bool quadratic_ITCF;   // measure the three-body ITCF?




	bool canonical_PIMC; // This flag constrains certain worm updates such as Advance or Recede when we want a purely canonical simulation




	int n_traps; // number of systems which are traps (not to be set by the user)
	int n_multi; // number of simulation_types that are not standard PIMC (not to be set by the user)




	int n_buffer; // Buffer size for observables (accumulation before writing to the disk)
	int tau_ac; // autocorrelation time, i.e., space between measuring observables to avoid correlations
	int n_equil; // Equilibration time before we start to measure stuff
	int total_measurements; // total number of measurements to be obtained within the PIMC simulation
	int binning_level; // bin measured observables (only makes sense for "write_all" activated)
	bool write_all; // write all measurements into a hdf5 file?


	int n_bins; // number of bins for density measuremets, S(q), etc

	bool debug; // Execute the program in debug mode?
	int status; // print a status message every "status" Monte Carlo steps


	double move_scale; // re-scaling the random interval to displace particles







	//##################################################################################################################################
	// ### System parameters (including sign-problem related things)
	//##################################################################################################################################


	int system_type; // Coulomb HO, Ewald HEG, ideal Fermi, ...
	int simulation_type; // what kind of Monte Carlo simulation are we running? e.g. PB-PIMC, canonical ensemble standard PIMC, ...




	double c_bar; // controls the ratio of G- and Z-sector


	int n_bead; // Number of imaginary-time propagators



	double beta; // inverse temperature
	double epsilon; // imaginary time step; epslion = beta / n_bead



	// Parameters for the homogeneous electron gas
	double rs, theta;


	double kappa_int; // ewald parameters for interaction energy and force
	double madelung; // madelung constant
	double volume;


	int r_index; // number of terms to be included in R-space summation (Ewald)
	int k_index; // number of terms to be included in G-space summation (Ewald)

	int n_boxes; // number of boxes to be considered in the kinetic matrix elements



	double lambda; // interaction coupling parameter
	int N; // particle number
	double length; // length of simulation cell, or length scale for obsevables in infinite "trap" systems

	double mu; // chemical potential; for grandcanonical ensemble, advance, recede


	int dim; // dimensionality of the system


	int n_species; // number of different particle species (not to be directly set by the user)



	int N_up; // number of spin-up particles
	int N_down; // number of spin-down particles

	int N_tot; // total number of particles, e.g. N_up + N_down + N_spec_2 (not to be set by the user)

	// Specified the number of particles for both species. Species 1 is considered to be
	// Fermions thus, N_spec_1 = N_tot
	int N_spec_1;
	// Since charge neutrality has to be reached N_spec_2 = N_up + N_down
	int N_spec_2;
	double charge_spec_1;
	double charge_spec_2;


	// Both species have to have a
	double mass_spec_1;
	double mass_spec_2;



	// a container to handle ionic coordinates
	ion_container ions;
	std::string snap_path;
	void set_ion_container(ion_container& ions_);







	// .... for the perturbed HEG/two-component, etc
	double vq; // amplitude of the perturbation
	int pq_x, pq_y, pq_z; // indices of q-vector

	// Now we even allow for the possibility of having multiple perturbations simultaneously!
	double vq2, vq3;
	int pq_x2, pq_y2, pq_z2;
	int pq_x3, pq_y3, pq_z3;





	// Parameters for the fourth-order Chin approximation; t0 and a1 are the only free parameters to be specified by the user
	double a1,t0,u0,v1,v2,t1;




	double xi; // for the xi-extrapolation (Xiong+Xiong)




    // ### Additional constraints etc...

	// Parameters for the artificial potential favouring a particular particle number in the grandcanonical ensemble
	bool N_potential_control;
	double sigma_target; // variance


	// Artifical potential between head and tail
	bool head_tail_potential;
	double head_tail_potential_eta;


	// New parameters for the artifical pair-permutation weight
	double pp_kappa;
	double pp_delta;
	bool pp_control;
	

	

	// ### Some parameters for the Bogoliubov inequality scheme by Hirshberg et al:
	double repulsive_c;
	double repulsive_eta;
	double repulsive_scale;
	int repulsive_choice; // which form of the repulsive potential do we wanna use?

	


	
	
	void print_to_screen();
	
	
	// Pre-calculate the Ewald summation exponential functions
	void fill_Ewald_exp_store();
	

	std::vector<std::vector<std::vector <double> > >Ewald_exp_store;
	double get_Ewald_exp(int nx, int ny, int nz);
	
	


	// The lookup table files
    // A two component System requires two HTDM lookup tables.
    std::string lookup_table_file1;
    std::string lookup_table_file2;
    std::string lookup_table_file3;

    // HTDM grid parameters
    int n_q, n_s, polydeg, n_lambda;
    double q_max, q_min;
    double red_mass, kap_rho, charge_rho, dq;
    std::shared_ptr<pair_action_lookup_table> table_ptr;
    void init_lookup_table();


};




void Parameters::init_lookup_table()
{
    q_max = std::sqrt(3) * length / 2;
    q_min = 0.0;
    n_q = ceil(((q_max - q_min) / (dq * length ) + 1 )  );

    std::string coeff_file_name = "pair_action_coefficients.csv";
    std::string coeff_deriv_file_name = "pair_action_deriv_coefficients.csv";

    if(exist_check(coeff_file_name)&&exist_check(coeff_deriv_file_name))
    {
        std::cout << "pair_action_coefficient.csv file found! \n";
        std::cout << "Loading data and interpolate!\n";
        table_ptr = std::make_shared<pair_action_lookup_table>(coeff_file_name , coeff_deriv_file_name, charge_spec_1, charge_spec_2, mass_spec_1, mass_spec_2, length,
                                                               beta, n_s, n_q, polydeg, n_bead, 5000, 1200, 1e-12, n_lambda);

    }
    else
    {
        std::cout << "Error! pair_action_coefficients.csv and pair_action_deriv_coefficients.csv not found!\n";
        std::cout << "PELICAN can not continue and aborts!\n";
        exit(1);
    }
}


void Parameters::print_to_screen()
{
	
	std::cout << "####################################################################\n";
	std::cout << "Printing all the parameters to the screen: \n";
	std::cout << "####################################################################\n";
	std::cout << "debug: " << debug << "\t status: " << status << "\n";
	std::cout << "omp_threads: " << omp_threads << "\t seed: " << seed << "\n";
	std::cout << "n_buffer: " << n_buffer << "\t tau_ac: " << tau_ac << "\t total_measurements: " << total_measurements << "\nbinning_level: " << binning_level << "\t write_all: " << write_all << "\t n_equil: " << n_equil << "\n";
	std::cout << "####################################################################\n";
	std::cout << "beta: " << beta << "\t lambda: " << lambda << "\n";
	std::cout << "dim: " << dim << "\t length: " << length << "\n";
	std::cout << "n_bead: " << n_bead << "\t N: " << N <<  "\n";
	std::cout << "chemical potential: " << mu << "\t system_type: " << system_type << "\n";
	std::cout << "--N_tot: " << N_tot << "\t N_up: " << N_up << "\t N_down: " << N_down << "\n";
	std::cout << "c_bar: " << c_bar << "\t move_scale: " << move_scale << "\n";
	std::cout << "####################################################################\n";
	std::cout << "c2p_control: " << c2p_control << "\t c2p_buffer: " << c2p_buffer << "\n";
	std::cout << "monopole_control: " << monopole_control << "\t permutation_control: " << permutation_control << "\n";
	std::cout << "ITCF_control: " << ITCF_control << "\t sk_control: " << sk_control << "\n";
	std::cout << "n_bins: " << n_bins << "\n";
	std::cout << "####################################################################\n";
	std::cout << "Chin approximation parameters: \n";
	std::cout << "a1: " << a1 << "\tt0: " << t0 << "\n";
	std::cout << "u0: " << u0 << "\tv1: " << v1 << "\n";
	std::cout << "v2: " << v2 << "\tt1: " << t1 << "\n";
	std::cout << "####################################################################\n";
	std::cout << "SIMULATION_TYPE: " << simulation_type << "\t n_multi: " << n_multi << "\n";
	std::cout << "n_species: " << n_species << "\n";
	std::cout << "####################################################################\n";
	std::cout << "N_potential_control: " << N_potential_control << "\t sigma_target: " << sigma_target << "\n";
	std::cout << "####################################################################\n";
	std::cout << "pp_control: " << pp_control << "\t pp_kappa: " << pp_kappa << "\t pp_delta: " << pp_delta << "\n";
	std::cout << "####################################################################\n";
	std::cout << "repulsive: c: " << repulsive_c << "\t eta: " << repulsive_eta << "\n";
	std::cout << "repulsive_choice: " << repulsive_choice << "\n";
	std::cout << "repulsive_scale: " << repulsive_scale << "\n";
	std::cout << "####################################################################\n";
	std::cout << "xi: " << xi << "\n";
	std::cout << "####################################################################\n";


	if( (system_type == 13) || (system_type == 16) )
	{
        std::cout << "#################### Printing ion coordinates ######################\n";
        std::cout << "####################################################################\n";
        ions.print_ion_positions();
        std::cout << "####################################################################\n";

    }

	if( system_type >= n_traps ) // Print params for the homogeneous electron gas?
	{
		std::cout << "volume: " << volume << "\n";
		std::cout << "rs: " << rs << "\t theta: " << theta << "\n";
		std::cout << "kappa_int: " << kappa_int << "\n";
		std::cout << "madelung: " << madelung << "\n";
		std::cout << "n_boxes: " << n_boxes << "\n";
		std::cout << "r_index: " << r_index << "\t k_index: " << k_index << "\n";
		std::cout << "pq_x: " << pq_x << "\t pq_y: " << pq_y << "\t pq_z: " << pq_z << "\t vq: " << vq << "\n";
		std::cout << "pq_x2: " << pq_x2 << "\t pq_y2: " << pq_y2 << "\t pq_z2: " << pq_z2 << "\t vq2: " << vq2 << "\n";
		std::cout << "pq_x3: " << pq_x3 << "\t pq_y3: " << pq_y3 << "\t pq_z3: " << pq_z3 << "\t vq3: " << vq3 << "\n";
		std::cout << "+++PBC_detailed_balance: " << PBC_detailed_balance << "\n";
	}
	std::cout << "measure_density_3D: " << measure_density_3D << "\tmeasure_force_on_ions: " << measure_force_on_ions << "\n";
}
















void Parameters::fill_Ewald_exp_store()
{
	const double pi = 3.1415926535897932384626433832795028841971693993751058;
	std::vector<double> tmp( 2*k_index+1, 0.0 );
	std::vector<std::vector <double> > tmp2( 2*k_index+1, tmp );


	Ewald_exp_store.assign( 2*k_index+1, tmp2 );
	
	int cnt = k_index;
	double inverse_length = 1.0 / length;
	
	double kappa_sq_inv = 1.0 / ( kappa_int * kappa_int );
	
	for(int nx=-cnt;nx<cnt+1;nx++)
	{
		double G_x = inverse_length * nx;
		for(int ny=-cnt;ny<cnt+1;ny++)
		{
			double G_y = inverse_length * ny;
			for(int nz=-cnt;nz<cnt+1;nz++)
			{
				double G_z = inverse_length * nz;
				
				double G_sq = G_x*G_x + G_y*G_y + G_z*G_z;
				
				double element = exp( -pi*pi * kappa_sq_inv * G_sq ) / G_sq;
				
				int my_x = nx + k_index;
				int my_y = ny + k_index;
				int my_z = nz + k_index;
				
				Ewald_exp_store[my_x][my_y][my_z] = element;
				
				
				
			} // end nz
		}
	} // end nx
	
}




double Parameters::get_Ewald_exp(int nx, int ny, int nz)
{
	int my_x = nx + k_index;
	int my_y = ny + k_index;
	int my_z = nz + k_index;
	
	return Ewald_exp_store[my_x][my_y][my_z];
}

void Parameters::set_ion_container(ion_container& ions_)
{
    ions = ions_;
}

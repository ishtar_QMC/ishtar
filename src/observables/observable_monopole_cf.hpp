/*
 * Contains class: observable_density_response
 */







template <class inter> class observable_monopole_cf
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	std::vector<std::vector<double> > k_vectors; // store all k-vectors
	int n_k; // number of k-vectors
	double L; // box-length of the simulation cell
	
	int n_buffer;  // buffer size
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	
	int n_bead;
	
	
	
	std::string bose_name = "boson_monopole_cf";
	std::string fermi_name = "fermion_monopole_cf";
	
	// Methods:
	observable_monopole_cf(){ } // TBD: implement constructor
	
	void init( int binning_level, int new_n_bead, bool new_write_all, int new_n_cycle, int new_n_buffer ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;

};




// initializes stuff
template <class inter> void observable_monopole_cf<inter>::init( int binning_level, int new_n_bead, bool new_write_all, int new_n_cycle, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_buffer = new_n_buffer;
	
	
	n_bead = new_n_bead;

	
	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_bead, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_bead, new_n_buffer, binning_level );
	
}




// perform the actual measurements
template <class inter> void observable_monopole_cf<inter>::measure( config_ensemble* ens )
{

	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
	
		// determine the particle number
		int N = ens->N_tot; 

		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );
		
		
		
		
		// Create a list with the monopole estimations for each time slice:

		std::vector< double >tmp_monopole( ens->params.n_bead, 0.0 );
		

		for(int iSlice=0;iSlice<ens->params.n_bead;iSlice++)
		{
			
			// On each slice, loop over all the particles
			for(int iSpecies=0;iSpecies<ens->n_species;iSpecies++)
			{
				for(int i=0;i<ens->species_config[ iSpecies ].beadlist[ iSlice ].size();i++)
				{
					// obtain the particle ID:
					int id = ens->species_config[ iSpecies ].beadlist[ iSlice ][ i ];
					
					double ans = 0.0;
					for(int iDim=0;iDim<ens->params.dim;iDim++)
					{
						double coord = ens->species_config[ iSpecies ].beads[ id ].get_coord( iDim );
						ans = ans + coord*coord;
					}
					
					
					tmp_monopole[ iSlice ] += ans;

					
				}
			} // end loop over all the particles
			
		} // end loop iSlice
		
		
	
		
		
		// Now multiply them for different time-slices, average over all P shifted combinations:
		
		
		for(int m=0;m<ens->params.n_bead;m++) // loop over the P different imaginary time-difference arguments
		{
			
			double C_m = 0.0;
			
			for(int i=0;i<ens->params.n_bead;i++) // loop over the P different estimations to be averaged over
			{
				int i_partner = i + m;
				if( i_partner >= ens->params.n_bead ) i_partner = i_partner - ens->params.n_bead;
				
				double monopole_i = tmp_monopole[i];
				double monopole_i_partner = tmp_monopole[i_partner];
				
				C_m += monopole_i*monopole_i_partner;
			}
			
			C_m = C_m / double( ens->params.n_bead );
			
			boson_array->add_value( m, C_m );
			fermion_array->add_value( m, C_m*ens->get_sign() );
			
			
		} // end loop over the P different values of c(tau) to be estimated
		

		
	} // end measurement cycle condition
	
}













/*
 * Contains class: observable_radial_density
 */


template <class inter> class observable_radial_density
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	double seg; // bin_width of the radial density
	int n_buffer;  // buffer size
	int n_seg; // number of bins for the radial density
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	double inv_seg;
	
	
	std::string bose_name = "boson_density";
	std::string fermi_name = "fermion_density";
	
	// Methods:
	observable_radial_density(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg ); // initialize stuff
	
	void measure( config_ensemble* ens );
	
	
	
	// measurement properties:
	std::vector<double>tmp;
	
	bool write_all;

};




// initializes stuff
template <class inter> void observable_radial_density<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg )
{
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	n_buffer = new_n_buffer;
	seg = new_seg;
	inv_seg = 1.0/seg;
	
	tmp.resize(n_seg);

	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg, new_n_buffer, binning_level );
	
}




// perform the actual measurements
template <class inter> void observable_radial_density<inter>::measure( config_ensemble* ens )
{
	

	
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		// determine the particle number
		int N = ens->N_tot;		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );

		
		
		for(auto c=(*ens).species_config.begin();c!=(*ens).species_config.end();c++) // loop over all particle species
		{
			for(int i=0;i<(*c).params.n_bead;i++) // loop over all the main slices
			{
				for(int k=0;k<c->beadlist[ i ].size();k++) // loop over all particles on a particular slice
				{
					int id = (*c).beadlist[i][k];
					double my_r = sqrt( (*c).beads[id].get_r_squared() );
					int my_bin = my_r*inv_seg;
					if(my_bin < n_seg)
					{
						tmp[my_bin] += 1.0;
					}
				}
			}
				
		} // end loop over all species
			

			
		// Submit value of each bin to the estimator_array_set
		double inv_fac = 1.0/double( (*ens).params.n_bead );
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();

			boson_array->add_value(index, (*i)*inv_fac);
			(*fermion_array).add_value(index, (*ens).get_sign()*(*i)*inv_fac);
				
			// Reset the tmp vector before the next measurement
			(*i) = 0.0;
		}

		
	} // end measurement cycle condition
	
}













#pragma once
/*
 * Contains class: observable_standard_PIMC_Kelbg_two_component
 */


template <class inter> class observable_standard_PIMC_Kelbg_two_component
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
	
	
	std::string bose_name = "boson_Kelbg_two_component";
	std::string fermi_name = "fermion_Kelbg_two_component";
	
	// Methods:
	observable_standard_PIMC_Kelbg_two_component(){ } 
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );
	
	
	void measure( config_ensemble* ensemble, inter* my_interaction ); // perform the actual measurements
	double Kelbg_derivative( Bead* a, Bead* b, Parameters* p );
	
	bool write_all;
	
	

	
	
	
//
};












// constructor:: initializes stuff
template <class inter> void observable_standard_PIMC_Kelbg_two_component<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	
	std::cout << "------------n_buffer: " << new_n_buffer << std::endl;
	
}









template <class inter> double observable_standard_PIMC_Kelbg_two_component<inter>::Kelbg_derivative( Bead* a, Bead* b, Parameters* p )
{
	double ans = 0.0;
	auto a_coords = a->get_all_coords();
	auto b_coords = b->get_all_coords();
	
	
	double length = p->length;
	

		
	std::vector<double> diff_vec = {a_coords[0] - b_coords[0], a_coords[1] - b_coords[1], a_coords[2] - b_coords[2]};
	
	double delta_r_sq = 0.0;
	
	// Compute the nearest image pair between bead and ion
	for (int dim = 0; dim < p->dim; dim++)
	{
		if (diff_vec[dim] > 0.5 * length)
		{
			diff_vec[dim] -= length;
		}

		if (diff_vec[dim] < -0.5 * length)
		{
			diff_vec[dim] += length;
		}
            
		delta_r_sq += diff_vec[dim]*diff_vec[dim];
	}
        
	// Compute the thermal wave length parameter of the Kelbg potential: 
	double Kelbg_lambda = sqrt(0.5*p->epsilon*( 1.0/a->get_mass() + 1.0/b->get_mass() ));
		
	double x = sqrt(delta_r_sq)/Kelbg_lambda;
		
	ans += ( erf(x)-1.0 )/Kelbg_lambda;
		

	
	return -  sqrt(M_PI) * 0.50 * a->get_charge() * b->get_charge() * ans / p->beta; 
}







template <class inter> void observable_standard_PIMC_Kelbg_two_component<inter>::measure( config_ensemble* ensemble, inter* my_interaction )
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		
		cnt = 0;
	
		
		
		
		// total number of particles in the system:
		int N = ensemble->N_tot;
		
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
		
		double kinetic_sum = 0.0;
		
		
		double kinetic_sum_m_e = 0.0;
		double kinetic_sum_m_p = 0.0;
		
		double m_e = ensemble->params.mass_spec_1;
		double m_p = ensemble->params.mass_spec_2;
		
		
		// obtain some constants
		double beta = ensemble->params.beta;
		double epsilon = ensemble->params.epsilon;
		int n_bead = ensemble->params.n_bead;
		
		
		// also obtain data for the external potential:
		double ext_pot_sum = 0.0;
		
		// contribution of the beta-derivative of a pair potential (for the ion-snapshot classes):
		double beta_derivative_contribution = 0.0;
		
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // loop over all the particle species 
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];

			
			// Carry out the double sum with the nearest neighbor convention:
			
			for(int iSlice=0;iSlice<n_bead;iSlice++)
			{
				for(int k=0;k<config->beadlist[iSlice].size();k++)
				{
					int k_i_id = config->beadlist[ iSlice ][ k ];
					int k_i_next_id = config->beads[ k_i_id ].get_next_id();
					
// 					if ( ensemble->params.system_type == 16 )  beta_derivative_contribution += full_Kelbg_derivative( &(*config).beads[ k_i_id ], &(*ensemble).params );
					
					
					if( ensemble->params.system_type == 17 ) // only Kelbg derivative cross-terms between electrons and protons
					{
					
						if( k < 2 ) // Kelbg derivative contributions
						{
							worm_configuration* ion_config = &(*ensemble).species_config[ 2 ];
							for(int q=0;q<ion_config->beadlist[ iSlice ].size();q++)
							{
								int q_id = ion_config->beadlist[ iSlice ][ q ];
								
								beta_derivative_contribution += Kelbg_derivative( &(*config).beads[ k_i_id ], &(*ion_config).beads[ q_id ], &(*ensemble).params ); 
								
							} // end loop q
							
							
						} // end "am I going over ions" if clause
					
					}
					else if ( ensemble->params.system_type == 19 ) // derivative cross terms with all the electrons
					{
						for(int w=1+k;w<config->beadlist[iSlice].size();w++) // loop over all the electrons from the same species
						{
							int w_i_id = config->beadlist[ iSlice ][ w ];
							
							beta_derivative_contribution += Kelbg_derivative( &(*config).beads[ k_i_id ], &(*config).beads[ w_i_id ], &(*ensemble).params ); 
							
						}
						
						
						
						for(int wSpecies=iSpecies+1;wSpecies<ensemble->species_config.size();wSpecies++)
						{
							worm_configuration* wconfig = &(*ensemble).species_config[ wSpecies ];
							for(int w=0;w<wconfig->beadlist[iSlice].size();w++)
							{
								int w_i_id = wconfig->beadlist[ iSlice ][ w ];
								
								beta_derivative_contribution += Kelbg_derivative( &(*config).beads[ k_i_id ], &(*wconfig).beads[ w_i_id ], &(*ensemble).params );
							}
							
							
						}
					
					} // end system_type condition
					
					
					
					// calculate the external potential:
					ext_pot_sum += my_interaction->ext_pot( &(*config).beads[ k_i_id ] );
					
					double r_squared = 0.0;
					
					for(int iDim=0;iDim<config->params.dim;iDim++)
					{
						double x_i = config->beads[ k_i_id ].get_coord( iDim );
						double x_i_next = config->beads[ k_i_next_id ].get_coord( iDim );
						double x_diff = x_i - x_i_next;
						
						// For PBC, apply the nearest neighbor convention:
						if( config->params.system_type >= config->params.n_traps )
						{
							while( x_diff < -config->params.length*0.50 )
							{
								x_diff = x_diff + config->params.length;
							}
							
							while( x_diff > config->params.length*0.50 )
							{
								x_diff = x_diff - config->params.length;
							}
						}
						
						r_squared += x_diff*x_diff;
						
					} // end loop iDim
					
					
					double addend = r_squared; // * config->beads[ k_i_id ].get_mass( );
					
//  					std::cout << "iSpecies: " << iSpecies << "\tmass: " << config->beads[ k_i_id ].get_mass() << "\tdiff_sq: " << r_squared << "\taddend: " << addend << std::endl;
					
					kinetic_sum += addend;
					
					if( iSpecies < 2 )
					{
						kinetic_sum_m_e += addend;
					}
					else
					{
						kinetic_sum_m_p += addend;
					}
					
					
					
				} // end loop k (all particles)
				

			} // end loop iSlice

			
		} // end loop iSpecies
		
		
		
		
		
		
		// TBD TBD TBD The kinetic contribution needs to be properly adapted in the case of protons
		// We want an extra slot for this!
		
		
		
		int full_N = 0;
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			full_N += ensemble->species_config[iSpecies].beadlist[0].size();
		}
		
		
 		
		
 		double kinetic_part_old = ensemble->params.dim * full_N * n_bead * 0.50 / beta - n_bead * 0.50 * kinetic_sum / ( beta*beta );
		

		
		
		
		
		
		
		// Do bookkeeping for the individual contributions to the kinetic energy
		double kinetic_part_m_e = ensemble->params.dim * N * n_bead * 0.50 / beta - n_bead * 0.50 * kinetic_sum_m_e * m_e / ( beta*beta );
		double kinetic_part_m_p = ensemble->params.dim * ( full_N - N ) * n_bead * 0.50 / beta - n_bead * 0.50 * kinetic_sum_m_p * m_p / ( beta*beta );
		
		
		
		double kinetic_mass_derivative_m_e = ensemble->total_m_e_pair_derivative * m_e / beta;
		double kinetic_mass_derivative_m_p = ensemble->total_m_p_pair_derivative * m_p / beta;
		
		
		
		

		
		
		
		
		
		double kinetic_part = kinetic_part_m_e + kinetic_part_m_p;
		
		
		// The mass derivatives only count to the estimation of the kinetic energy individually!
		kinetic_part_m_e += kinetic_mass_derivative_m_e;
		kinetic_part_m_p += kinetic_mass_derivative_m_p; 
		
		
		
		
// 		std::cout << "kinetic_part_m_e: " << kinetic_part_m_e << "\tfirst: " << ensemble->params.dim * N * n_bead * 0.50 / beta << "\tsecond: " << - n_bead * 0.50 * kinetic_sum_m_e * m_e / ( beta*beta ) << "\tthird: " << kinetic_mass_derivative_m_e << "\n";
// 		std::cout << "kinetic_part_m_p: " << kinetic_part_m_p << "\tfirst: " << ensemble->params.dim * ( full_N - N ) * n_bead * 0.50 / beta << "\tsecond: " << - n_bead * 0.50 * kinetic_sum_m_p * m_p / ( beta*beta ) << "\tthird: " << kinetic_mass_derivative_m_p << "\n";
		
// 		std::cout << "N: " << N << "\tfull_N: " << full_N << std::endl;
// 		std::cout << "kinetic_part_old: " << kinetic_part_old << "\tnew_reduced: " << kinetic_part_m_e-kinetic_mass_derivative_m_e << "\tnew: " << kinetic_part_m_e << "\n";
		
		
		double potential_part = ensemble->total_energy / double( n_bead );
		
		ext_pot_sum = ext_pot_sum / double( n_bead );
		double interaction_part = potential_part - ext_pot_sum;
		
		double total = kinetic_part + potential_part;
		
		
		
		
		
		
		
		
		
		
		
		
		beta_derivative_contribution = beta_derivative_contribution * ensemble->params.beta / double( n_bead );
		
		
		if( ( ensemble->params.system_type == 20 ) || ( ensemble->params.system_type == 14 ) || ( ensemble->params.system_type == 21 ) || ( ensemble->params.system_type == 22 ) ) beta_derivative_contribution = ensemble->total_pair_derivative;
		
		
		total -= beta_derivative_contribution;
		
		interaction_part -= ( beta_derivative_contribution - kinetic_mass_derivative_m_e - kinetic_mass_derivative_m_p );
		potential_part -= ( beta_derivative_contribution - kinetic_mass_derivative_m_e - kinetic_mass_derivative_m_p );

		
		
		
		
		
		
		
		
		
		

		
		
		
		
		
		double ensemble_sign = ensemble->get_sign();
		
		
		
		
		
		
		boson_array->add_value( 0, ensemble_sign ); 
		fermion_array->add_value( 0, ensemble_sign );
		
		boson_array->add_value( 1, ext_pot_sum ); 
		fermion_array->add_value( 1, ext_pot_sum*ensemble_sign );
		
		boson_array->add_value( 2, potential_part ); 
		fermion_array->add_value( 2, potential_part*ensemble_sign );
		
		boson_array->add_value( 3, kinetic_part ); 
		fermion_array->add_value( 3, kinetic_part*ensemble_sign );
		
		boson_array->add_value( 4, total ); 
		fermion_array->add_value( 4, total*ensemble_sign );
		
		boson_array->add_value( 5, beta_derivative_contribution );
		fermion_array->add_value( 5, beta_derivative_contribution*ensemble_sign );
		
	
		
		boson_array->add_value( 6, kinetic_part_m_e );
		fermion_array->add_value( 6, kinetic_part_m_e*ensemble_sign );
		
		boson_array->add_value( 7, kinetic_mass_derivative_m_e );
		fermion_array->add_value( 7, kinetic_mass_derivative_m_e*ensemble_sign );
		
		boson_array->add_value( 8, kinetic_part_m_p );
		fermion_array->add_value( 8, kinetic_part_m_p*ensemble_sign );
		
		boson_array->add_value( 9, kinetic_mass_derivative_m_p );
		fermion_array->add_value( 9, kinetic_mass_derivative_m_p*ensemble_sign );



		

		
		
	}

}


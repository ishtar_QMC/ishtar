/*
 * Contains class: observable_momentum
 */







template <class inter> class observable_momentum
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	std::vector<std::vector<double> > k_vectors; // store all k-vectors
	int n_k; // number of k-vectors
	double L; // box-length of the simulation cell
	
	int n_buffer;  // buffer size
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	
	std::string bose_name = "boson_momentum";
	std::string fermi_name = "fermion_momentum";
	
	// Methods:
	observable_momentum(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;
	
	double pre_factor;

};




// initializes stuff
template <class inter> void observable_momentum<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_buffer = new_n_buffer;
	
	n_k = new_n_k;
	L = new_L;
	
	pre_factor = 1.0/(2.0*pi*L);
	
	// Fill the k-vectors
	// TBD TBD TBD -> init needs to know the dimensionality of the system in advance!
	create_k_vectors( &k_vectors, n_k, L );

	
	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_k+1, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_k+1, new_n_buffer, binning_level );
	
}




// perform the actual measurements
template <class inter> void observable_momentum<inter>::measure( config_ensemble* ens )
{

// 	if( ens->G_species == ens->params.momentum_species ) // The measurement cycle is only activated when we are in a configuration where the "right" species is off-diagonal
// 	{
		if( cnt < n_cycle ) // Check, if enough configs have been skipped
		{
			cnt++;
		}
		else
		{
			cnt = 0; // Reset cycle counter and perform the actual measurement
		
		
			if( ens->params.dim < 3 ) return; // ATM, S(k) is defined only for 3D systems
		
			// determine the particle number
			int N = ens->N_tot + 1000000 + 33333*ens->G_species; 

			// obtain a pointer to the appropriate estimator_array within the storage:
			estimator_array* boson_array = boson_storage.pointer( N );
			estimator_array* fermion_array = fermion_storage.pointer( N );
			

			Bead* head = &(*ens).species_config[ ens->G_species ].beads[ (*ens).species_config[ ens->G_species ].head_id ];
			Bead* tail = &(*ens).species_config[ ens->G_species ].beads[ (*ens).species_config[ ens->G_species ].tail_id ];
			
			
			
			
			double factor = pow( pre_factor, ens->params.dim );
			for(int iK=0;iK<n_k+1;iK++) // loop over all k-vectors
			{
				double cos_arg = 0.0;
				if( iK > 0 )
				{
					for(int iDim=0;iDim<ens->params.dim;iDim++)
					{
						cos_arg += (k_vectors[ iK-1 ][ iDim ]* (head->get_coord(iDim)- tail->get_coord(iDim)));
					}
				}
				
				
				double ans = cos(cos_arg)*factor;
				
				boson_array->add_value(iK, ans);
				fermion_array->add_value(iK, ans*ens->get_sign() );
			}


			
		} // end measurement cycle condition
// 	}
	
}













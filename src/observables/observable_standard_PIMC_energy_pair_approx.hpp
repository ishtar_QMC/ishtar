#pragma once

/**
* Contains the class for the energy estimator
*/

template <class inter>
class observable_standard_PIMC_energy_pair_approx
{
public:

    // Basic properties:

    estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector

    int n_buffer;  // buffer size
    int n_energy; // number of slots
    int n_cycle; // number of configs to be skipped between measurements

    int cnt = 0; // cycle counter for configs


    std::string bose_name = "boson_pair";
    std::string fermi_name = "fermion_pair";

    // Methods:
    observable_standard_PIMC_energy_pair_approx() = default;
    void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer );


    void measure( config_ensemble* ensemble, inter* my_interaction ); // perform the actual measurements


    bool write_all;


};


template<class inter>
void observable_standard_PIMC_energy_pair_approx<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
    n_cycle  = new_n_cycle;
    n_energy = new_n_seg;
    n_buffer = new_n_buffer;
    write_all = new_write_all;

    boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
    fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

    std::cout << "------------n_buffer: " << new_n_buffer << std::endl;

}


template<class inter>
void observable_standard_PIMC_energy_pair_approx<inter>::measure(config_ensemble *ensemble, inter *my_interaction)
{
    if( cnt < n_cycle )
    {
        cnt++;
    }
    else{

        cnt = 0;


        // total number of particles in the system:
        int N = ensemble->N_tot;



        // obtain a pointer to the appropriate estimator_array within the storage:
        estimator_array* boson_array = boson_storage.pointer(N);
        estimator_array* fermion_array = fermion_storage.pointer(N);


        double kinetic_sum = 0.0;
        // obtain some constants
        double beta = ensemble->params.beta;
        double epsilon = ensemble->params.epsilon;
        int n_bead = ensemble->params.n_bead;


        // also obtain data for the external potential:
        double ext_pot_sum = 0.0;

        for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // loop over all the particle species
        {
            worm_configuration* config = &(*ensemble).species_config[ iSpecies ];


            // Carry out the double sum with the nearest neighbor convention:

            for(int iSlice=0;iSlice<n_bead;iSlice++)
            {
                for(int k=0;k<config->beadlist[iSlice].size();k++)
                {
                    int k_i_id = config->beadlist[ iSlice ][ k ];
                    int k_i_next_id = config->beads[ k_i_id ].get_next_id();


                    // calculate the external potential:
                    ext_pot_sum += my_interaction->ext_pot( &(*config).beads[ k_i_id ] );

                    double r_squared = 0.0;

                    for(int iDim=0;iDim<config->params.dim;iDim++)
                    {
                        double x_i = config->beads[ k_i_id ].get_coord( iDim );
                        double x_i_next = config->beads[ k_i_next_id ].get_coord( iDim );
                        double x_diff = x_i - x_i_next;

                        // Apply the nearest neighbor convention:
                       if( config->params.system_type >= config->params.n_traps )
                        {
                            while( x_diff < -config->params.length*0.50 )
                            {
                                x_diff = x_diff + config->params.length;
                            }

                            while( x_diff > config->params.length*0.50 )
                            {
                                x_diff = x_diff - config->params.length;
                            }
                        }

                        r_squared += x_diff*x_diff;

                    } // end loop iDim

                    kinetic_sum += r_squared;

                } // end loop k (all particles)


            } // end loop iSlice


        } // end loop iSpecies


        double pair_action_part = ensemble->total_pair_derivative;
        double N_tot = ensemble->params.N_tot;

        double kinetic_term = n_bead / (2.* beta * beta) * kinetic_sum;


        // See Overleaf document eq. (80). Here we take all masses m=1.0 since we are only concerened with electrons.
        double const_term = - 3. * N_tot / ( 2 * beta) * n_bead;
        //double prefac = std::pow(2*M_PI * beta / double(n_bead), -1.5 * N_tot);
        double second_term = (kinetic_term - 1. / double(n_bead) * ensemble->total_energy - ensemble->total_pair_derivative);
        double total = const_term + second_term;
        double real_kin_term = const_term + kinetic_term;
        // Assign minus sign since we want to calculate -d/d\beta Z
        total *= -1;



        double kinetic_part = ensemble->params.dim * N * n_bead * 0.50 / beta - n_bead * 0.50 * kinetic_sum / ( beta*beta );
        double potential_part =  ensemble->total_energy / double( n_bead ) + pair_action_part;
        ext_pot_sum = ensemble->total_energy / double( n_bead );
        double interaction_part = potential_part - ext_pot_sum;


        double ensemble_sign = ensemble->get_sign();

        (*boson_array).add_value( 0, ensemble_sign );
        (*fermion_array).add_value( 0, ensemble_sign*real_kin_term );

        (*boson_array).add_value( 1, ext_pot_sum );
        (*fermion_array).add_value( 1, ext_pot_sum*ensemble_sign );

        (*boson_array).add_value( 2, potential_part );
        (*fermion_array).add_value( 2, potential_part*ensemble_sign );

        (*boson_array).add_value( 3, kinetic_part );
        (*fermion_array).add_value( 3, kinetic_part*ensemble_sign );

        (*boson_array).add_value( 4, total );
        (*fermion_array).add_value( 4, total*ensemble_sign );



    }
};



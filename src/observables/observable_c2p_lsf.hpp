/*
 * Contains class: observable_c2p_lsf: The center-two partice CF by Hauke Thomsen, now extended to measure correlations within the superfluid component!
 */


template <class inter> class observable_c2p_lsf
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	double seg; // bin_width of the radial distance
	double angle_seg; // bin width of the angle
	int n_buffer;  // buffer size
	
	int n_seg; // number of radial and angular bins
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	double inv_norm;

		
	std::string bose_name = "boson_c2p_lsf";
	std::string fermi_name = "fermion_c2p_lsf";
	
	// Methods:
	observable_c2p_lsf(){ };
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, worm_configuration* c ); // initialize stuff
	
	void measure(worm_configuration *c); // perform the actual measurements
	
	
	
	// measurement properties:
	std::vector<double>tmp;
	std::vector<double>I_cl;
	
	
	std::vector<std::vector<double>> A_loc_data;
	
	
	bool write_all;

};




// initializes stuff
template <class inter> void observable_c2p_lsf<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, worm_configuration* c )
{
	std::cout << "Initializing the C2P_LSF observable\n";
	
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	n_buffer = new_n_buffer;
	
	seg = (*c).params.length / double(n_seg);
	angle_seg = pi / double(n_seg);

	
	tmp.resize(n_seg*n_seg*n_seg); // Store the 3D array in 1D

	write_all = false; // For C2P, we never compute error bars! (memory issues) // new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg*n_seg*n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg*n_seg*n_seg, new_n_buffer, binning_level );
	
	
	inv_norm = 1.0 / (seg * seg * angle_seg);
	
	
	
	for(int i=0;i<n_seg;i++)
	{
		double I = 0.50*pi*pow( seg, 4) *( pow( i+1, 4 ) - pow( i, 4) );
		I = I / ( pi * seg * seg * ( 1.0 + 2.0*i ) );
		
		I_cl.push_back( I );
	}
	
	std::vector<double> A_anc( n_seg, 0.0 );
	A_loc_data.assign( n_seg, A_anc );
	
	std::cout << "Initialized.\n";
	
}




// perform the actual measurements
template <class inter> void observable_c2p_lsf<inter>::measure( worm_configuration *c )
{
	
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		// determine the particle number
		int N = c->beadlist[0].size();
		

		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);		
		estimator_array* fermion_array = fermion_storage.pointer(N);
		
		long int my_n_add = boson_array->get_n_add();
		
		
		double Az = 0.0;
		
		for(auto t=A_loc_data.begin();t!=A_loc_data.end();t++) (*t).assign( n_seg, 0.0 );
		
		// Create a histogram of density bins
		for(int i=0;i<c->params.n_bead;i++) // loop over all the main slices
		{
			
			for(int k1=0;k1<N;k1++) // loop over all particles_1 on a particular slice
			{
				int id1 = c->beadlist[i][k1];
				int next_id1 = c->beads[ id1 ].get_next_id();
				
				double x1 = c->beads[id1].get_coord(0);
				double y1 = c->beads[id1].get_coord(1);
					
				double x1_next = c->beads[next_id1].get_coord(0);
				double y1_next = c->beads[next_id1].get_coord(1);
				
				double A_tmp1 = 0.50 * ( x1*y1_next - y1*x1_next );
				Az += A_tmp1;
					
				for(int  k2=1+k1;k2<N;k2++)
				{
				
					int id2 = c->beadlist[i][k2];
					

					int next_id2 = c->beads[ id2 ].get_next_id();
				
					double r1 = sqrt( c->beads[id1].get_r_squared() );
					double r2 = sqrt( c->beads[id2].get_r_squared() );
					
					double x2 = c->beads[id2].get_coord(0);
					double y2 = c->beads[id2].get_coord(1);
					
					double x2_next = c->beads[next_id2].get_coord(0);
					double y2_next = c->beads[next_id2].get_coord(1);
					
					
					double A_tmp2 = 0.50 * ( x2*y2_next - y2*x2_next );
					
					
					
					
					
					
					double cos_arg = (x1*x2 + y1*y2) / (r1 * r2);
					double theta = acos( cos_arg );
					
					if( cos_arg <= -1.0 ) theta = pi;
					if( cos_arg >= 1.0 ) theta = 0.0;
					
					int angle_bin = theta / angle_seg;
					int r_bin1 = r1 / seg;
					int r_bin2 = r2 / seg;
					
					
					if( (r_bin1 < n_seg) && (r_bin2 < n_seg) && (angle_bin < n_seg) ) // Are the particles within the given range ?
					{
						double tmp_A_tmp1 = A_tmp1 / (r1*r1);//I_cl[ r_bin1 ];
						A_tmp2 = A_tmp2 / (r2*r2);// I_cl[ r_bin2 ];
						
						int index1 = r_bin2*n_seg*n_seg + r_bin1*n_seg + angle_bin;
						int index2 = r_bin1*n_seg*n_seg + r_bin2*n_seg + angle_bin;
						
						tmp[ index1 ] += tmp_A_tmp1 * A_tmp2;
						tmp[ index2 ] += tmp_A_tmp1 * A_tmp2;
						
					} // end check if particles are within range
				
				} // end loop particle two
				
			} // end loop particle one

			
		} // end loop over all time slices
		


		
		// Submit value of each bin to the estimator_array_set
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();

			double addend = Az*Az*(*i)*16.0 / pow( c->params.beta, 2 );
			
			(*boson_array).add_value(index, addend*inv_norm);
			(*fermion_array).add_value(index, c->my_sign * addend *inv_norm);
			
			// Reset the tmp vector before the next measurement
			(*i) = 0.0;
		}

	}
	
}













// Contains the class pair_action_toolbox

template <class inter> class pair_action_toolbox
{
public:
	
	
	std::vector<double> get_total_pair_action( config_ensemble* ensemble );
	
	void init( Parameters* new_p );
	
	std::vector<double> change_pair_action( int old_id_0, int old_id_1, Bead* new_0, Bead* new_1, int species, config_ensemble* ensemble );
	
	std::vector<double> delete_pair_action( int old_id_0, int old_id_1, int species, config_ensemble* ensemble );
	
	std::vector<double> add_pair_action( Bead* new_0, Bead* new_1, int excl_0, int excl_1, int species, config_ensemble* ensemble );
	
	std::vector<double> get_total_ext_pot_action( config_ensemble* ensemble );
	
	
	
	
	
	
	// ### New infrastructure for the multi-moves
	
	std::vector<double> multi_change_pair_action( config_ensemble* ensemble, std::vector<int>& species_list, std::vector<int>& id_list_0, std::vector<int>& id_list_1, std::vector<Bead>* bead_list_0, std::vector<Bead>* bead_list_1 );
	
	
private:
	inter my_interaction;
	Parameters* p;
};




template <class inter> void pair_action_toolbox<inter>::init( Parameters* new_p )
{
	p = new_p;
	my_interaction.init( p );
	
	std::cout << "''''''''' pair_action_toolbox has been initialized '''''''''''''\n";
}








// OLD - NEW, as always!
template <class inter> std::vector<double> pair_action_toolbox<inter>::multi_change_pair_action( config_ensemble* ensemble, std::vector<int>& species_list, std::vector<int>& id_list_0, std::vector<int>& id_list_1, std::vector<Bead>* bead_list_0, std::vector<Bead>* bead_list_1 )
{
	
	double ans_old = 0.0;
	double ans_new = 0.0;
	
	double ans_old_derivative = 0.0;
	double ans_new_derivative = 0.0;
	
	double ans_old_m_e_derivative = 0.0;
	double ans_new_m_e_derivative = 0.0;
	
	double ans_old_m_p_derivative = 0.0;
	double ans_new_m_p_derivative = 0.0;
	
	
	int time = (*bead_list_0)[ 0 ].get_time();
	
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
		
		for(int i=0;i<config->beadlist[ time ].size();i++)
		{
			int i_ID = config->beadlist[ time ][ i ];
			
			if( i_ID != config->head_id ) // the head does not contribute to the action in this way
			{
				Bead* i_Bead = &(*config).beads[ i_ID ];
				int next_i_ID = i_Bead->get_next_id();
				Bead* next_i_Bead = &(*config).beads[ next_i_ID ];
				
				
				for(int l=0;l<species_list.size();l++) // loop over all the changed beads
				{
					if( ( i_ID != id_list_0[l] ) || ( iSpecies != species_list[l] ) ) // No interaction with one-self!
					{
						
						
						// Do not count interactions between different changed beads:
						bool NotInList = true;
						for(int n=0;n<id_list_0.size();n++)
						{
							if( ( iSpecies == species_list[n] ) && ( i_ID == id_list_0[n] ) ) NotInList = false;
						}
						
						if( NotInList )
						{
						
						
							Bead* old_0 = &(*ensemble).species_config[ species_list[l] ].beads[ id_list_0[l] ];
							Bead* old_1 = &(*ensemble).species_config[ species_list[l] ].beads[ id_list_1[l] ];
							
							Bead* new_0 = &((*bead_list_0)[l]);
							Bead* new_1 = &((*bead_list_1)[l]);
							
							std::vector<double> old_action = my_interaction.pair_action( old_0, old_1, i_Bead, next_i_Bead );
							std::vector<double> new_action = my_interaction.pair_action( new_0, new_1, i_Bead, next_i_Bead );
							
							ans_new += new_action[0];
							ans_new_derivative += new_action[1];
							ans_new_m_e_derivative += new_action[2];
							ans_new_m_p_derivative += new_action[3];
							
							ans_old += old_action[0];
							ans_old_derivative += old_action[1];
							ans_old_m_e_derivative += old_action[2];
							ans_old_m_p_derivative += old_action[3];
							
							
						} // end NotInList if clause
						
						
					} // end "self-interaction" if clause
					
				} // end loop l "all changed beads"
				
		
		
			} // end tail if clause
		
		
		} // end loop i
		
	} // end loop iSpecies
	
	
	
		
	std::vector<double> result;
	result.push_back( ans_old-ans_new );
	result.push_back( ans_old_derivative-ans_new_derivative );
	result.push_back( ans_old_m_e_derivative-ans_new_m_e_derivative );
	result.push_back( ans_old_m_p_derivative-ans_new_m_p_derivative );
	

	return result;
	
}












template <class inter> std::vector<double> pair_action_toolbox<inter>::add_pair_action( Bead* new_0, Bead* new_1, int excl_0, int excl_1, int species, config_ensemble* ensemble )
{
	double ans_new = 0.0;
	double ans_new_derivative = 0.0;
	double ans_new_m_e_derivative = 0.0;
	double ans_new_m_p_derivative = 0.0;
	
	// Firstly, compute the action within the same species
	worm_configuration* config = &(*ensemble).species_config[species];
	int time = new_0->get_time();
	
	// loop over all partcles of species:
	for(int i=0;i<config->beadlist[time].size();i++)
	{
		int i_id = config->beadlist[time][i];
		if( ( i_id != excl_0 ) && ( i_id != config->head_id ) ) // envoked, if only new_1 is really new 
		{
			Bead* i_id_bead = &(*config).beads[ i_id ];
			int next_id = i_id_bead->get_next_id();
			
			if( next_id != excl_1 ) // envoked, if only new_0 is really new 
			{
				Bead* next_id_bead = &(*config).beads[ next_id ];
				
				std::vector<double> new_action = my_interaction.pair_action( i_id_bead, next_id_bead, new_0, new_1 );
			
				ans_new += new_action[0];
				ans_new_derivative += new_action[1];
				ans_new_m_e_derivative += new_action[2];
				ans_new_m_p_derivative += new_action[3];
		
			} // end excl_1 clause
		} // end excl_0 clause
		
	} // end loop i
	
	
	// loop over all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != species )
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
			
			// loop over all particles of iSpecies:
			for(int i=0;i<config->beadlist[time].size();i++)
			{
				int i_id = config->beadlist[time][i];
				Bead* i_id_bead = &(*config).beads[ i_id ];
				int next_id = i_id_bead->get_next_id();
				Bead* next_id_bead = &(*config).beads[ next_id ];
				
				std::vector<double> new_action = my_interaction.pair_action( new_0, new_1, i_id_bead, next_id_bead );
				
				ans_new += new_action[0];
				ans_new_derivative += new_action[1];
				ans_new_m_e_derivative += new_action[2];
				ans_new_m_p_derivative += new_action[3];
			
			} // end loop i
		}
	} // end loop iSpecies
	
	
	std::vector<double> result;
	result.push_back( ans_new );
	result.push_back( ans_new_derivative );
	result.push_back( ans_new_m_e_derivative );
	result.push_back( ans_new_m_p_derivative );
	
	return result;
}




















template <class inter> std::vector<double> pair_action_toolbox<inter>::delete_pair_action( int old_id_0, int old_id_1, int species, config_ensemble* ensemble )
{
	double ans_old = 0.0;
	double ans_old_derivative = 0.0;
	double ans_old_m_e_derivative = 0.0;
	double ans_old_m_p_derivative = 0.0;
	
	// Let us first compute the action from the same species
	worm_configuration* config = &(*ensemble).species_config[species];
	Bead* old_bead_0 = &(*config).beads[ old_id_0 ];
	Bead* old_bead_1 = &(*config).beads[ old_id_1 ];
	int time = old_bead_0->get_time();
	
	// loop over all particles of this species:
	for(int i=0;i<config->beadlist[time].size();i++)
	{
		int i_id = config->beadlist[time][i];
		if( ( i_id != old_id_0 ) && ( i_id != config->head_id ) )
		{
			Bead* i_id_bead = &(*config).beads[ i_id ];
			int next_id = i_id_bead->get_next_id();
			Bead* next_id_bead = &(*config).beads[ next_id ];
			
			std::vector<double> old = my_interaction.pair_action( old_bead_0, old_bead_1, i_id_bead, next_id_bead );
			
			ans_old += old[0];
			ans_old_derivative += old[1];
			ans_old_m_e_derivative += old[2];
			ans_old_m_p_derivative += old[3];
		}
	}
	
	
	// loop over all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != species )
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
			
			// loop over all particles of iSpecies:
			for(int i=0;i<config->beadlist[time].size();i++)
			{
				int i_id = config->beadlist[time][i];
				Bead* i_id_bead = &(*config).beads[ i_id ];
				int next_id = i_id_bead->get_next_id();
				Bead* next_id_bead = &(*config).beads[ next_id ];
				
				std::vector<double> old = my_interaction.pair_action( old_bead_0, old_bead_1, i_id_bead, next_id_bead );
				
				ans_old += old[0];
				ans_old_derivative += old[1];
				ans_old_m_e_derivative += old[2];
				ans_old_m_p_derivative += old[3];
			
			} // end loop i
		}
	} // end loop iSpecies
	
	std::vector<double> result;
	result.push_back( ans_old );
	result.push_back( ans_old_derivative );
	result.push_back( ans_old_m_e_derivative );
	result.push_back( ans_old_m_p_derivative );
	
	return result;
}









// Compute the change in the pair action when beads of species have been replaced
// as always, OLD - NEW
template <class inter> std::vector<double> pair_action_toolbox<inter>::change_pair_action( int old_id_0, int old_id_1, Bead* new_0, Bead* new_1, int species, config_ensemble* ensemble )
{
	double ans_old = 0.0;
	double ans_new = 0.0;
	
	double ans_old_derivative = 0.0;
	double ans_new_derivative = 0.0;
	
	double ans_old_m_e_derivative = 0.0;
	double ans_new_m_e_derivative = 0.0;
	
	double ans_old_m_p_derivative = 0.0;
	double ans_new_m_p_derivative = 0.0;
	
	
	// loop over all particles from the same species
	worm_configuration* config = &(*ensemble).species_config[species];
	Bead* old_bead_0 = &(*config).beads[ old_id_0 ];
	Bead* old_bead_1 = &(*config).beads[ old_id_1 ];
	int time = old_bead_0->get_time();
	
	for(int i=0;i<config->beadlist[time].size();i++)
	{
		int i_id = config->beadlist[time][i];
		
		
		if( ( i_id != config->head_id ) && ( i_id != old_id_0 ) )
		{
			Bead* i_id_bead = &(*config).beads[ i_id ];
			int next_id = i_id_bead->get_next_id();
			Bead* next_bead = &(*config).beads[ next_id ];
			
			std::vector<double> old_action = my_interaction.pair_action( old_bead_0, old_bead_1, i_id_bead, next_bead );
			std::vector<double> new_action = my_interaction.pair_action( new_0, new_1, i_id_bead, next_bead );
			
			ans_old += old_action[0];
			ans_new += new_action[0];
			
			ans_old_derivative += old_action[1];
			ans_new_derivative += new_action[1];
			
			ans_old_m_e_derivative += old_action[2];
			ans_new_m_e_derivative += new_action[2];
			
			ans_old_m_p_derivative += old_action[3];
			ans_new_m_p_derivative += new_action[3];
			
			
		} // end if clause
		
	} // end loop i
	
	
	// loop over all other species
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != species )
		{
			// loop over all particles in this species
			worm_configuration* config = &(*ensemble).species_config[iSpecies];
			for(int i=0;i<config->beadlist[time].size();i++)
			{
				int i_id = config->beadlist[time][i];
				if( i_id != config->head_id )
				{
					Bead* i_id_bead = &(*config).beads[i_id];
					int next_id = i_id_bead->get_next_id();
					Bead* next_bead = &(*config).beads[ next_id ];
					
					std::vector<double> old_action = my_interaction.pair_action( old_bead_0, old_bead_1, i_id_bead, next_bead );
					std::vector<double> new_action = my_interaction.pair_action( new_0, new_1, i_id_bead, next_bead );
					
					ans_old += old_action[0];
					ans_new += new_action[0];
					
					ans_old_derivative += old_action[1];
					ans_new_derivative += new_action[1];
					
					ans_old_m_e_derivative += old_action[2];
					ans_new_m_e_derivative += new_action[2];
					
					ans_old_m_p_derivative += old_action[3];
					ans_new_m_p_derivative += new_action[3];
				
				} // end if clause
			} // end loop i
		
		
		} // end species-condition
	} // end loop iSpecies
	
	std::vector<double> result;
	result.push_back( ans_old-ans_new );
	result.push_back( ans_old_derivative-ans_new_derivative );
	result.push_back( ans_old_m_e_derivative-ans_new_m_e_derivative );
	result.push_back( ans_old_m_p_derivative-ans_new_m_p_derivative );
	
	
	
	
	

	
	return result;
	
}











// Compute the full ext_pot_action of the system (all species):
template <class inter> std::vector<double> pair_action_toolbox<inter>::get_total_ext_pot_action( config_ensemble* ensemble )
{
	double ans = 0.0;
	double ans_derivative = 0.0;
	
	
// 	std::vector<double> IAV2 = get_total_pair_action( ensemble );		
// 	std::cout << "***************INLINE: " << IAV2[0] << "\t" << IAV2[1] << "\t" << IAV2[2] << "\t" << IAV2[3] << "\n";
	
	
	// loop over all imaginary time slices:
	for(int iSlice=0;iSlice<ensemble->params.n_bead;iSlice++)
	{
		//loop over all the particle-species:
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
			
//  			std::vector<double> IAV2 = get_total_pair_action( ensemble );		
//  			std::cout << "***************INLINE: " << IAV2[0] << "\t" << IAV2[1] << "\t" << IAV2[2] << "\t" << IAV2[3] << "\n";
			
			// loop over all particles of this species on slice iSlice:
			for(int i=0;i<config->beadlist[ iSlice ].size();i++)
			{
				int i_id = config->beadlist[ iSlice ][ i ];
				Bead* i_Bead = &(*config).beads[ i_id ];
				
				if( i_id != config->head_id ) // The head does not enjoy the privilege of an ext_pot_action as the starting point!
				{
					Bead* next_i_Bead = &(*config).beads[ i_Bead->get_next_id() ];
					std::vector<double> myResult = my_interaction.ext_pot_action( i_Bead, next_i_Bead );
					
					ans += myResult[0];
					ans_derivative += myResult[1];
				}
				
// 				std::cout << "i_id: " << i_id << "\t next: " << i_Bead->get_next_id() << "\n";
				
			} // end loop i
			
			
		} // end loop iSpecies
		
	} // end loop iSlice
	
	
// 	std::vector<double> IAV12 = get_total_pair_action( ensemble );
// 	std::cout << "Before-Final***************INLINE: " << IAV12[0] << "\t" << IAV12[1] << "\t" << IAV12[2] << "\t" << IAV12[3] << "\n";
	
	std::vector<double> result;
	result.push_back( ans );
	result.push_back( ans_derivative );
	
	
	
	
	
// 	std::vector<double> IAV2 = get_total_pair_action( ensemble );
//  	std::cout << "Final***************INLINE: " << IAV2[0] << "\t" << IAV2[1] << "\t" << IAV2[2] << "\t" << IAV2[3] << "\n";
	
	
	return result;
}






// Compute the entire pair action of all species:
template <class inter> std::vector<double> pair_action_toolbox<inter>::get_total_pair_action( config_ensemble* ensemble )
{
	double ans = 0.0;
	double ans_derivative = 0.0;
	double ans_m_e_derivative = 0.0;
	double ans_m_p_derivative = 0.0;
	
	
	std::vector<double> ans_vector( p->n_bead, 0.0 );
	std::vector<double> ans_derivative_vector( p->n_bead, 0.0 );
	std::vector<double> ans_m_e_derivative_vector( p->n_bead, 0.0 );
	std::vector<double> ans_m_p_derivative_vector( p->n_bead, 0.0 );
	
	
	#pragma omp parallel 
	{
		#pragma omp for
		for(int iSlice=0;iSlice<p->n_bead;iSlice++)
		{
			// loop over all particle species
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				worm_configuration* config = &(*ensemble).species_config[ iSpecies ];
				
				// loop over all particles of iSpecies:
				for(int i=0;i<config->beadlist[iSlice].size();i++)
				{
					int id = config->beadlist[iSlice][i];
					Bead* id_bead = &(*config).beads[id];
					if( id != config->head_id ) // the head does not contribute in this way
					{
						int next_id = config->beads[ id ].get_next_id();
						Bead* next_id_bead = &(*config).beads[ next_id ];
						
						// loop over all other particles of iSpecies:
						for(int k=1+i;k<config->beadlist[iSlice].size();k++)
						{
							int k_id = config->beadlist[iSlice][k];
							Bead* k_id_bead = &(*config).beads[k_id];
							if( k_id != config->head_id )
							{
								int k_next_id = k_id_bead->get_next_id();
								Bead* k_next_id_bead = &(*config).beads[ k_next_id ];
								
								std::vector<double> i_k_contribution = my_interaction.pair_action( id_bead, next_id_bead, k_id_bead, k_next_id_bead );
								
								// ### OpenMP:: ans += i_k_contribution[0];
								// ### OpenMP:: ans_derivative += i_k_contribution[1];
								
								// ### OpenMP:: ans_m_e_derivative += i_k_contribution[2];
								// ### OpenMP:: ans_m_p_derivative += i_k_contribution[3];
								
								
								ans_vector[iSlice] += i_k_contribution[0];
								ans_derivative_vector[iSlice] += i_k_contribution[1];
								ans_m_e_derivative_vector[iSlice] += i_k_contribution[2];
								ans_m_p_derivative_vector[iSlice] += i_k_contribution[3];
								
								
							}
							
							
							
						} // end loop k
						
						
						
						// loop over all other species
						for(int kSpecies=iSpecies+1;kSpecies<ensemble->species_config.size();kSpecies++)
						{
							worm_configuration* k_config = &(*ensemble).species_config[ kSpecies ];
							
							// loop over all particles of species kSpecies
							for(int k=0;k<k_config->beadlist[iSlice].size();k++)
							{
								int k_id = k_config->beadlist[iSlice][k];
								Bead* k_id_bead = &(*k_config).beads[k_id];
								if( k_id != k_config->head_id )
								{
									int k_next_id = k_id_bead->get_next_id();
									Bead* k_next_id_bead = &(*k_config).beads[ k_next_id ];
									
									std::vector<double> i_k_contribution = my_interaction.pair_action( id_bead, next_id_bead, k_id_bead, k_next_id_bead );
									
									// ### OpenMP:: ans += i_k_contribution[0];
									// ### OpenMP:: ans_derivative += i_k_contribution[1];
									
									// ### OpenMP:: ans_m_e_derivative += i_k_contribution[2];
									// ### OpenMP:: ans_m_p_derivative += i_k_contribution[3];
									
									
									ans_vector[iSlice] += i_k_contribution[0];
									ans_derivative_vector[iSlice] += i_k_contribution[1];
									ans_m_e_derivative_vector[iSlice] += i_k_contribution[2];
									ans_m_p_derivative_vector[iSlice] += i_k_contribution[3];
									
								}
								
								
								
							} // end loop k
							
							
							
						} // end loop kSpecies
						
						
						
						
					} // end head-if-clause
					
					
					
					
				} // end loop i
				
			} // end loop iSpecies
		
		} // end loop iSlice
		
		
	}
	
	
	// ### OpenMP Post Processing Loop
	
	
	for(int iSlice=0;iSlice<p->n_bead;iSlice++)
	{
		ans += ans_vector[iSlice];
		ans_derivative += ans_derivative_vector[iSlice];
		ans_m_e_derivative += ans_m_e_derivative_vector[iSlice];
		ans_m_p_derivative += ans_m_p_derivative_vector[iSlice];
	}
	
	
	
	std::vector<double> result;
	result.push_back( ans );
	result.push_back( ans_derivative );
	result.push_back( ans_m_e_derivative );
	result.push_back( ans_m_p_derivative );
	
	return result;
}









































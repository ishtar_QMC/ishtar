#pragma once

#include <vector>


class ion_species
{

public:
    ion_species(int charge_, double mass_,double x_, double y_, double z_) : charge(charge_), mass(mass_),coords({x_, y_, z_}){};
    ion_species(int charge_, double mass_, std::vector<double>& coords_) : charge(charge_), mass(mass_), coords(coords_) {};
    ion_species(): charge(0), mass(1.0), coords({0.0,0.0,0.0}) {};
    std::vector<double>& get_coords();
    int get_charge() const;
    void print();
    double get_mass();

private:
    int charge;
    double mass;
    std::vector<double> coords;

};


std::vector<double>& ion_species::get_coords()
{
 return coords;
}

int ion_species::get_charge() const
{
 return charge;
}

void ion_species::print()
{
    std::cout << "Charge " << charge << ", Mass " << mass << ", x=" << coords[0] << ", y=" << coords[1] << ", z=" << coords[2] << std::endl;
}

double ion_species::get_mass()
{
    return mass;
}
/*
* Contains the selection_toolbox class
* 
* The selection_toolbox contains:
*  - random_slice_type: selects a slice type (A,B,main) Uniformly at random and returns the kind and the memory addresses of blist and nlist
*/





template <class inter> class selection_toolbox
{
public:
	
	//TBD:remove scale from select_bead_from_matrix
	
	
	// Initialize the selection toolbox
	void init(worm_configuration* new_config);
	
	
	// Uniformly select the kind of slice, return the kind and memory addresses of beadlist and next_list
	int random_slice_type( std::vector<std::vector <int> > **blist, std::vector<int> **nlist );
	// TBD: Remove random_slice_type2 and use random_slice_type !!!!
	int random_slice_type2( std::vector<std::vector <int> > **blist, std::vector<int> **nlist );
	
	// Select a bead from beadlist according to the diffusion matrix,
	// return the id in beads[...].
	// special decides whether to include head or tail into the selection, special=true means it is possible
	// Bead* b is the bead on the prev. slice
	int select_bead_from_blist(Bead* b, std::vector<int>* blist, int next, double scale, double* norm, int* number, bool special);
	int PBC_select_bead_from_blist(Bead* b, std::vector<int>* blist, int next, double scale, double* prob, int* number, bool special);
	
	// The same as select_bead_from_blist, but diffusion matrix elements are obtained from Matrix 'm'
	int select_bead_from_matrix(double scale, Bead* b, bool reverse, Matrix* m, std::vector<int>* blist, int next, double* norm, int* number, bool special);
	
	
	
	
	
	// Select the target bead for the swap move, where Bead 'head' is the current head bead
	// return the id
	int select_target_bead(Bead* head, double* norm, int* number);
	
	int select_target_bead_PBC(Bead* head, double* prob, int* number);
	
	// calculate the normalization for the reverse selection of a target bead (on the slice of the tail, starting from Bead 'new_head'
	double get_reverse_target_norm(Bead* new_head);
	
	
	
	
	/*
	 * calculate the normalization to select a bead from blist, starting from Bead 'start'
	 * end_id denotes a bead in blist which has been changed to 'end' during the same update, which must be taken into account for the selection
	 */
	double get_reverse_norm(std::vector<int>* blist, int end_id, int next, Bead* start, Bead* end, double scale, bool special);
	
	
	// calculate the selection prob. for the backwards move of an update, in PBC:
	double get_selection_prob_reverse( std::vector<int>* blist, int end_id, int next, Bead* start, Bead* end, double scale, bool special );

	
	
	// The same as get_reverse_norm, but the diffusion matrix elements are obtained from Matrix 'm'
	double get_reverse_norm_from_matrix(std::vector<int>* blist, int ref_number, int next, bool reverse, Matrix* m, bool special);
	
	
	// ancilla function to select a random element from a pre-existing rho_list with the normalization 'norm'
	int select_bead_from_rholist(std::vector<double>* rho_list, double norm);
	
	
	
	
	
	
	
	
	double reverse_target_PBC( int target_id, Bead* start, double scale, bool debug );
	
	
	

	
	
	
private:
	
	worm_configuration* config;
	inter my_interaction;

	
	// Constant stuff
	const double one_third = 1.0/3.0;
	const double two_third = 2.0/3.0;
	
	std::vector<int>tmp_id_list;
	std::vector<double>tmp_rho_list;
	std::vector<int>tmp_number_list;
};






template<class inter> double selection_toolbox<inter>::get_reverse_target_norm(Bead* new_head)
{
	
	double ans;
	
	int target_time = (*config).beads[(*config).tail_id].get_time(); // target time is time of tail
	int target_kind = (*config).tail_kind; // target kind is kind of tail (A,B,main)
	
	// obtain the imaginary time of the head bead
	double head_real_time = (*config).beads[(*config).head_id].get_real_time();
	
	// obtain the imaginary time of the tail/target bead
	double target_real_time = (*config).beads[(*config).tail_id].get_real_time();

	// calculate the scale factor for the rho function
	double tau_diff = target_real_time - head_real_time;
	if(tau_diff < 0.0) tau_diff += (*config).params.beta;
	
	double scale = tau_diff / (*config).params.epsilon;
	
	// obtain a pointer to the target beadlist
	std::vector<int> *target_beadlist;
	int target_next = (*config).tail_id; // Do not include the bead of the tail into the selection process
	
	
	if( target_kind == 1 )
	{ // ancilla slice A
		target_beadlist = &(*config).beadlist_A[target_time];
	}
	else if( target_kind == 2 )
	{ // ancilla slice B
		target_beadlist = &(*config).beadlist_B[target_time];
	}
	else
	{ // main slice
		target_beadlist = &(*config).beadlist[target_time];
	}
	
	
	ans = get_reverse_norm(target_beadlist, -1, target_next, new_head, new_head, scale, false);
	
	
	
	
	return ans;
}













template<class inter> int selection_toolbox<inter>::select_target_bead_PBC(Bead* head, double* prob, int* number)
{
	int target_time = (*config).beads[(*config).tail_id].get_time(); // target time is time of tail
	int target_kind = (*config).tail_kind; // target kind is kind of tail (A,B,main)
	
	// obtain the imaginary time of the head bead
	double head_real_time = (*config).beads[(*config).head_id].get_real_time();
	
	// obtain the imaginary time of the tail/target bead
	double target_real_time = (*config).beads[(*config).tail_id].get_real_time();

	// calculate the scale factor for the rho function
	double tau_diff = target_real_time - head_real_time;
	if(tau_diff < 0.0) tau_diff += (*config).params.beta;
	
	double scale = tau_diff / (*config).params.epsilon;
	

	// obtain a pointer to the target beadlist
	std::vector<int> *target_beadlist;
	int target_next = (*config).tail_id; // Do not include the bead of the tail into the selection process
	
	
	if( target_kind == 1 )
	{ // ancilla slice A
		target_beadlist = &(*config).beadlist_A[target_time];
	}
	else if( target_kind == 2 )
	{ // ancilla slice B
		target_beadlist = &(*config).beadlist_B[target_time];
	}
	else
	{ // main slice
		target_beadlist = &(*config).beadlist[target_time];
	}
	
	
	
	
	return PBC_select_bead_from_blist( head, target_beadlist, target_next, scale, prob, number, false ); // the last argument, special=false, means that the tail cannot be choosen here!
// 	return select_bead_from_blist(head, target_beadlist, target_next, scale, norm, number, false);

}













template<class inter> int selection_toolbox<inter>::select_target_bead(Bead* head, double* norm, int* number)
{
	int target_time = (*config).beads[(*config).tail_id].get_time(); // target time is time of tail
	int target_kind = (*config).tail_kind; // target kind is kind of tail (A,B,main)
	
	// obtain the imaginary time of the head bead
	double head_real_time = (*config).beads[(*config).head_id].get_real_time();
	
	// obtain the imaginary time of the tail/target bead
	double target_real_time = (*config).beads[(*config).tail_id].get_real_time();

	// calculate the scale factor for the rho function
	double tau_diff = target_real_time - head_real_time;
	if(tau_diff < 0.0) tau_diff += (*config).params.beta;
	
	double scale = tau_diff / (*config).params.epsilon;
	

	// obtain a pointer to the target beadlist
	std::vector<int> *target_beadlist;
	int target_next = (*config).tail_id; // Do not include the bead of the tail into the selection process
	
	
	if( target_kind == 1 )
	{ // ancilla slice A
		target_beadlist = &(*config).beadlist_A[target_time];
	}
	else if( target_kind == 2 )
	{ // ancilla slice B
		target_beadlist = &(*config).beadlist_B[target_time];
	}
	else
	{ // main slice
		target_beadlist = &(*config).beadlist[target_time];
	}
	
	
	
	
	
	return select_bead_from_blist(head, target_beadlist, target_next, scale, norm, number, false);

}






template<class inter> double selection_toolbox<inter>::get_selection_prob_reverse( std::vector<int>* blist, int end_id, int next, Bead* start, Bead* end, double scale, bool special )
{
	double ans = 0.0;
	double my_prob; // Value of the matrix element from the actually chosen transition
	for(int l=0;l<(*config).params.N;l++) // loop over all particles on the affected slice
	{
		int lid = (*blist)[l]; // bead id on the end_slice
		
		if( ( lid != next ) && ( ( ( lid != (*config).head_id  ) && ( lid != (*config).tail_id ) ) || special ) )
		{
			double element = my_interaction.rho( start, &(*config).beads[lid], scale );
			if( lid == end_id ) // the bead 'end_id' on blist has been changed to Bead 'end'
			{
				element = my_interaction.rho( start, end, scale );
				my_prob = element;
			}

			ans += element;
		}
	}
	
	return my_prob/ans;
}







template<class inter> double selection_toolbox<inter>::reverse_target_PBC( int target_id, Bead* start, double scale, bool debug )
{
	
	int tail_time = config->beads[ config->tail_id ].get_time();
	std::vector<int> *blist;
	
	if( config->tail_kind == 1 )
	{
		blist = &(*config).beadlist_A[ tail_time ];
	}
	else if( config->tail_kind == 2 )
	{
		blist = &(*config).beadlist_B[ tail_time ];
	}
	else
	{
		blist = &(*config).beadlist[ tail_time ];
	}
	
	double norm = 0.0;
	double target;
	for(int i=0;i<config->params.N;i++)
	{
		int id = (*blist)[i];
		if( id != config->tail_id )
		{
			double element = my_interaction.rho( start, &(*config).beads[ id ], scale );
			if( id == target_id ) target = element;
			norm += element;
			if( debug ) std::cout << "***id: " << id << "\t element: " << element << "\t norm: " << norm << "\t scale: " << scale << "\n";
		}
	}
	
	if( debug ) 
	{
		std::cout << "------target: " << target << "\t norm: " << norm << "\t distance: " << my_interaction.distance( start, &(*config).beads[ target_id ] ) << "\n";
		std::cout << "    target_id: " << target_id << "\t tail_id: " << config->tail_id << "\n";
		std::cout << "    head_real_time: " << config->beads[ config->head_id ].get_real_time() << "\t tail_real_time: " << config->beads[ config->tail_id ].get_real_time() << "\n";
	}
	
	return target / norm;
}






template<class inter> double selection_toolbox<inter>::get_reverse_norm(std::vector<int>* blist, int end_id, int next, Bead* start, Bead* end, double scale, bool special)
{
	double norm = 0.0;
	for(int l=0;l<(*config).params.N;l++) // loop over all particles on the affected slice
	{
		
		int lid = (*blist)[l]; // bead id on the end_slice
		
		if( ( lid != next ) && ( ( ( lid != (*config).head_id  ) && ( lid != (*config).tail_id ) ) || special ) )
		{
// 			double element = rho_fun(start, &(*config).beads[lid], &(*config).params, scale);
			double element = my_interaction.rho( start, &(*config).beads[lid], scale );
			if( lid == end_id ) // the bead 'end_id' on blist has been changed to Bead 'end'
			{
// 				element = rho_fun(start, end, &(*config).params, scale);
				element = my_interaction.rho( start, end, scale );
			}

			norm += element;

		}
	}
	
	return norm;
}
		
		
		











template <class inter> int selection_toolbox<inter>::select_bead_from_rholist(std::vector<double>* rho_list, double norm)
{
	double ans = 0.0;
	double choice = norm*rdm();
	
	for(int i=0;i<(*rho_list).size();i++)
	{
		if( (ans <= choice) && (ans+fabs( (*rho_list)[i] ) >= choice) )
		{
			return i;
		}
		ans += fabs( (*rho_list)[i] );
	}
	
	
	std::cout << "Fatal error in select_bead_from_rholist, nothing has been selected.\n";
	exit(0);
	
}








// Select a bead from beadlist according to the diffusion matrix and return the id
template <class inter>  int selection_toolbox<inter>::select_bead_from_blist(Bead* b, std::vector<int>* blist, int next, double scale, double* norm, int* number, bool special)
{
	
	double tmp_norm = 0.0; // normalization
	int tcnt = 0; // counter of existing beads
	
	
	
	// loop over all bead in blist and calculate the diffusion element
	for(int i=0;i<(*blist).size();i++)
	{
		if( (*blist)[i] != next ) // check, whether this particular bead exists
		{
			// special indicates the last slice of the artificial trajectory. Only if special, we include head or tail into the trajectory, as fixed end-points
			if( special || ( ( (*config).tail_id != (*blist)[i] ) && ( (*config).head_id != (*blist)[i] ) ) )
			{
		
				// calculate the diffusion matrix element
// 				double element = rho_fun(b, &(*config).beads[(*blist)[i]], &(*config).params, scale);
				double element = my_interaction.rho( b, &(*config).beads[(*blist)[i]], scale );
				

				
				// Update the norm
				tmp_norm += element;
				
				// add the connection to the temporary lists
				tmp_rho_list[tcnt] = element;
				tmp_id_list[tcnt] = (*blist)[i];
				tmp_number_list[tcnt] = i;
			
				
				
				tcnt++; // increase the exisiting beads counter
				

				
			}
			
		}
		
	} // end loop over all existing beads
	
	
	
	// Select a bead from the temporary lists:
	int select = select_bead_from_rholist(&tmp_rho_list, tmp_norm);
	
	
	// Assign all information to be returned:
	(*norm) = tmp_norm;
	(*number) = tmp_number_list[select];
	
	return tmp_id_list[select];
	
}





//TBD: Remove this and use random_slice_type!!!!

// Uniformly select a type of slice (i.e., A,B,main) and return kind and memory addresses of blist and nlist
template <class inter>  int selection_toolbox<inter>::random_slice_type2(std::vector<std::vector <int> > **blist, std::vector<int> **nlist)
{
	double choice = rdm();
	int kind;

	
	if(choice <= one_third)
	{
		kind = 1; // A
	}
	else if(choice <= two_third)
	{
		kind = 2; // start with ancilla slice B
	}
	else{
		kind = 3; // start main
	}
	
	
	(*blist) = (*config).get_blist_pointer(kind);
	(*nlist) = (*config).get_nlist_pointer(kind);
	

	return kind;
}









// Uniformly select a type of slice (i.e., A,B,main) and return kind and memory addresses of blist and nlist
template <class inter>  int selection_toolbox<inter>::random_slice_type(std::vector<std::vector <int> > **blist, std::vector<int> **nlist)
{
	double choice = rdm();
	int kind;

	
	if(choice <= one_third)
	{
		kind = 3; // select the main slice
	}
	else if(choice <= two_third)
	{
		kind = 1; // start with ancilla slice A
	}
	else{
		kind = 2; // start with ancilla slice B
	}
	
	
	(*blist) = (*config).get_blist_pointer(kind);
	(*nlist) = (*config).get_nlist_pointer(kind);
	

	return kind;
}





template <class inter> void selection_toolbox<inter>::init(worm_configuration* new_config)
{
	config = new_config;
	
	// Initialize the temporary lists and leave them unchanged to prevent dynamic memory allocation
	tmp_id_list.resize((*config).params.N);
	tmp_rho_list.resize((*config).params.N);
	tmp_number_list.resize((*config).params.N);


	// Initialize the inter class:
	my_interaction.init( &(*config).params );
}









template<class inter> double selection_toolbox<inter>::get_reverse_norm_from_matrix(std::vector<int>* blist, int ref_number, int next, bool reverse, Matrix* m, bool special){
	double norm = 0.0;
	for(int l=0;l<(*config).params.N;l++) // loop over all particles on the affected slice
	{
		
		int lid = (*blist)[l]; // bead id on the ref slice
		
		if( ( lid != next ) && ( ( ( lid != (*config).head_id  ) && ( lid != (*config).tail_id ) ) || special ) )
		// only existing beads are included into the normalization
		// head and tail can only be included if special = true, i.e. if this is a fixed end-point of the trajectory
		{
			
			int start_number, end_number;
			if(!reverse)
			{
				start_number = ref_number;
				end_number = (*config).beads[lid].get_number();
			}
			else{
				end_number = ref_number;
				start_number = (*config).beads[lid].get_number();
			}
			
			double element = (*m).get_value(end_number, start_number);

			norm += element;

		}
	}
	
	return norm;
}






// TBD
// This function does NOT work for update pb-pimc-deform, when the starting point is the head, which is possible
// Fix this e.g. by passing an id or by not allowing such a thing in deform
template<class inter> int selection_toolbox<inter>::select_bead_from_matrix(double scale, Bead* b, bool reverse, Matrix* m, std::vector<int>* blist, int next, double* norm, int* number, bool special)
{
	double tmp_norm = 0.0; // Initialize the normalization
	int tcnt = 0; // counter of exisiting beads
	
	int ref_number = (*b).get_number();
	
	// loop over all bead in blist and calculate the diffusion element
	for(int i=0;i<(*blist).size();i++)
	{
		if( (*blist)[i] != next ) // check, whether this particular bead exists
		{
			// special indicates the last slice of the artificial trajectory. Only if special, we include head or tail into the trajectory, as fixed end-points
			if( special || ( ( (*config).tail_id != (*blist)[i] ) && ( (*config).head_id != (*blist)[i] ) ) )
			{
		
				// calculate the diffusion matrix element
// 				double element = rho_fun(b, &(*config).beads[(*blist)[i]], &(*config).params, scale);
 				double element2 = my_interaction.rho( b, &(*config).beads[(*blist)[i]], scale );
				
				int start_number, end_number;
				
				if(!reverse)
				{
					start_number = ref_number;
					end_number = (*config).beads[(*blist)[i]].get_number();
				}
				else
				{
					end_number = ref_number;
					start_number = (*config).beads[(*blist)[i]].get_number();
				}
				
				double element = (*m).get_value(end_number, start_number);


				
				
				
// 				element = element2;
				
				
				
				// Update the norm
				tmp_norm += element;
				
				// add the connection to the temporary lists
				tmp_rho_list[tcnt] = element;
				tmp_id_list[tcnt] = (*blist)[i];
				tmp_number_list[tcnt] = i;
			
				
				
				tcnt++; // increase the exisiting beads counter
				

				
			}
			
		}
		
	} // end loop over all existing beads
	
	
	
	// Select a bead from the temporary lists:
	int select = select_bead_from_rholist(&tmp_rho_list, tmp_norm);
	
	
	// Assign all information to be returned:
	(*norm) = tmp_norm;
	(*number) = tmp_number_list[select];
	
	return tmp_id_list[select];
	
	
	
	
	
	
}






// Select a bead from beadlist according to the diffusion matrix and return the id
template <class inter> int selection_toolbox<inter>::PBC_select_bead_from_blist( Bead* b, std::vector<int>* blist, int next, double scale, double* prob, int* number, bool special )
{
	
	double tmp_norm = 0.0; // normalization
	int tcnt = 0; // counter of existing beads
	
// 	std::cout << "********************************************************************, scale = " << scale << "\n";
	
	// loop over all bead in blist and calculate the diffusion element
	for(int i=0;i<(*blist).size();i++)
	{
		if( (*blist)[i] != next ) // check, whether this particular bead exists
		{
			// special indicates the last slice of the artificial trajectory. Only if special, we include head or tail into the trajectory, as fixed end-points
			if( special || ( ( (*config).tail_id != (*blist)[i] ) && ( (*config).head_id != (*blist)[i] ) ) )
			{
		
				// calculate the diffusion matrix element
				double element = my_interaction.rho( b, &(*config).beads[(*blist)[i]], scale );
				

				
				// Update the norm
				tmp_norm += element;
				
				// add the connection to the temporary lists
				tmp_rho_list[tcnt] = element;
				tmp_id_list[tcnt] = (*blist)[i];
				tmp_number_list[tcnt] = i;
			
// 				std::cout << "+++norm: " << tmp_norm << "\t id: " << tmp_id_list[tcnt] << "\t element: " << element << "\n";
				
				tcnt++; // increase the exisiting beads counter
				

				
			}
			
		}
		
	} // end loop over all existing beads
	
	
	
	// Select a bead from the temporary lists:
	int select = select_bead_from_rholist(&tmp_rho_list, tmp_norm);
	
	
	// Assign all information to be returned:
	(*prob) = tmp_rho_list[select] / tmp_norm;
	(*number) = tmp_number_list[select];
	
	return tmp_id_list[select];
	
}









#pragma once


// ###############################################################################################
// ############## PBC with COULOMB EWALD interaction -> homogeneous electron gas #################
// ###############################################################################################

class perturbed_ewald_HEG
{
public:
    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b){ return 0.0; };
    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };

    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);

    // Return the spatial difference between two positions within the simulation cell
    double get_diff(double a, double b);





    // Calculate the Madelung constant
    double get_madelung_constant(double tol, int* max_it, double kappa, int max_index);





    // Calculate the interaction energy between two beads to find optimum kappa value
    double Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);

    // modified Ewald pair interaction with a specified r_index:
    double Ewald_pair_index(Bead* a, Bead* b, int cnt);

    // Calculate the Ewald force between two beads to find the optimum kappa value
    double Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);
    double Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);


	// Return the pair-action compotent of the total action. Zero in this case
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 ){ std::vector result{0.0,0.0,0.0,0.0}; return result; }


	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }






private:


    // Pointer to the Parameters
    Parameters* p;




};








void perturbed_ewald_HEG::init( Parameters* new_p )
{
    p = new_p;
}



// Handle the PBC
double perturbed_ewald_HEG::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}


double perturbed_ewald_HEG::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
// 	double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}



// External potential is Madelung contribution
double perturbed_ewald_HEG::ext_pot(Bead* a)
{
    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double cos_arg = ( 2.0*pi/(*p).length * ( (*p).pq_x*x_a + (*p).pq_y*y_a + (*p).pq_z*z_a ) );
    double cos_arg2 = ( 2.0*pi/p->length * ( p->pq_x2*x_a + p->pq_y2*y_a + p->pq_z2*z_a ) );
    double cos_arg3 = ( 2.0*pi/p->length * ( p->pq_x3*x_a + p->pq_y3*y_a + p->pq_z3*z_a ) );


    return 0.50*(*p).madelung + 2.0*( cos( cos_arg )*(*p).vq + cos( cos_arg2 )*p->vq2 + cos( cos_arg3 )*p->vq3 );
}


// There is no external force
void perturbed_ewald_HEG::ext_force_on_a(Bead* a, std::vector<double>* force)
{

    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double pre_factor = 2.0*(*p).vq;
    double tpl = 2.0*pi/(*p).length;

    double pre_factor2 = 2.0*p->vq2;
    double pre_factor3 = 2.0*p->vq3;



    (*force)[0] = pre_factor * tpl * (*p).pq_x * sin( tpl * (*p).pq_x * x_a );
    (*force)[1] = pre_factor * tpl * (*p).pq_y * sin( tpl * (*p).pq_y * y_a );
    (*force)[2] = pre_factor * tpl * (*p).pq_z * sin( tpl * (*p).pq_z * z_a );

    (*force)[0] += pre_factor2 * tpl * p->pq_x2 * sin( tpl * p->pq_x2 * x_a );
    (*force)[1] += pre_factor2 * tpl * p->pq_y2 * sin( tpl * p->pq_y2 * y_a );
    (*force)[2] += pre_factor2 * tpl * p->pq_z2 * sin( tpl * p->pq_z2 * z_a );

    (*force)[0] += pre_factor3 * tpl * p->pq_x3 * sin( tpl * p->pq_x3 * x_a );
    (*force)[1] += pre_factor3 * tpl * p->pq_y3 * sin( tpl * p->pq_y3 * y_a );
    (*force)[2] += pre_factor3 * tpl * p->pq_z3 * sin( tpl * p->pq_z3 * z_a );

}


// We measure the direct distance, not the closest to the next image.
double perturbed_ewald_HEG::get_diff(double a, double b)
{
    double tmp = a - b;

    return tmp;
}




double perturbed_ewald_HEG::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
// 		double tmp = get_diff( (*a).get_coord(i), (*b).get_coord(i) );
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        while( tmp > (*p).length*0.5 )
        {
            tmp = tmp - (*p).length;
        }
        while( tmp < -(*p).length*0.5 )
        {
            tmp = tmp + (*p).length;
        }


        ans += tmp * tmp;
    }
    return ans;
}



double perturbed_ewald_HEG::distance(Bead* a, Bead* b)
{
    return sqrt( distance_sq(a, b) );
}









// Ewald summation part


// Calculate the entire Madelung constant and set number of required iterations:
double perturbed_ewald_HEG::get_madelung_constant(double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;
            double R_x_sq = R_x * R_x;
            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;
                double R_y_sq = R_y * R_y;
                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;
                    double R_z_sq = R_z * R_z;


                    // Do not include the zero G-vector
                    if( !( ( nx == 0 ) && ( ny == 0 ) && ( nz == 0 ) ) )
                    {

                        double G_sq = G_x_sq + G_y_sq + G_z_sq;
                        double r_abs = sqrt( R_x_sq + R_y_sq + R_z_sq );

                        ans_k += exp( -pi*pi*G_sq/kappa_sq ) / (pi * G_sq);
                        ans_r += erfc( kappa * r_abs ) / r_abs;

                    } // end zero vector cond.


                } // end loop nz
            }
        } // end loop nx

// 		std::cout << "k_cnt: " << cnt << "\t ans: " << ans << "\n";

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r - 2.0*kappa/sqrt(pi);




    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt-1;

    // Return reciprocal part of Madelung const:
    return ans;

}










void perturbed_ewald_HEG::pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force)
{

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa = (*p).kappa_int;
    double kappa_sq = kappa * kappa;

    int cnt = (*p).r_index;

    (*force)[0] = 0.0;
    (*force)[1] = 0.0;
    (*force)[2] = 0.0;

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;

        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;

            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;

                double R_z = nz * (*p).length;

                double r_x = (*a).get_coord(0) - (*b).get_coord(0);
                double r_y = (*a).get_coord(1) - (*b).get_coord(1);
                double r_z = (*a).get_coord(2) - (*b).get_coord(2);

                double r_x_priam = r_x + R_x;
                double r_y_priam = r_y + R_y;
                double r_z_priam = r_z + R_z;

                double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                double R_priam = sqrt( R_priam_sq );

                double erfc_part = erfc( kappa * R_priam );
                double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                (*force)[0] += r_x_priam * tmp2;
                (*force)[1] += r_y_priam * tmp2;
                (*force)[2] += r_z_priam * tmp2;

                double G_sq = G_x_sq + G_y_sq + G_z_sq;

                double product = r_x * G_x + r_y * G_y + r_z * G_z;

                double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
                double tmp = g_fac * sin( product * 2.0*pi ) * my_exp / G_sq;
// 				double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                {
                    (*force)[0] += tmp * G_x;
                    (*force)[1] += tmp * G_y;
                    (*force)[2] += tmp * G_z;
                }



            } // end nz
        } // end ny
    } // end nx;


}



double perturbed_ewald_HEG::pair_interaction(Bead* a, Bead* b)
{
    double G_fac = 2.0*pi/p->length;

    double ans_r = 0.0;
    double ans_k = 0.0;

    double kappa = p->kappa_int;

    double dx = a->get_coord(0) - b->get_coord(0);
    double dy = a->get_coord(1) - b->get_coord(1);
    double dz = a->get_coord(2) - b->get_coord(2);



    // Pre-compute the cosine-factors for the reciprocal part:
    std::vector<double> GX,GY,GZ;
    for(int iK=0;iK<p->k_index+1;iK++)
    {
        GX.push_back( cos(iK*G_fac*dx) );
        GY.push_back( cos(iK*G_fac*dy) );
        GZ.push_back( cos(iK*G_fac*dz) );
    }


    double ans_k_new = 0.0;
    // Evaluate the sum in reciprocal space:
    for(int iX=-p->k_index;iX<p->k_index+1;iX++)
    {
        int x_index = abs( iX );
        for(int iY=-p->k_index;iY<p->k_index+1;iY++)
        {
            int y_index = abs( iY );
            for(int iZ=-p->k_index;iZ<p->k_index+1;iZ++)
            {
                int z_index = abs( iZ );

//  				double G_sq = ( iX*iX + iY*iY + iZ*iZ ) / ( p->length * p->length );

                double cos_term = GX[ x_index ]*GY[ y_index ]*GZ[ z_index ];
// 				double my_exp = exp( -pi*pi*G_sq/(kappa*kappa) );
                double my_exp = p->get_Ewald_exp(iX, iY, iZ);

                if( ( iX != 0 ) || ( iY != 0 ) || ( iZ != 0 ) )
                {
                    ans_k_new +=  cos_term * my_exp / pi;
                }

            }
        }
    }

    int max_r = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    // Evaluate the sum in real space:
    for(int iX=-max_r;iX<max_r+1;iX++)
    {
        double x = dx + p->length*iX;
        for(int iY=-max_r;iY<max_r+1;iY++)
        {
            double y = dy + p->length*iY;
            for(int iZ=-max_r;iZ<max_r+1;iZ++)
            {
                double z = dz + p->length*iZ;

                double r_abs = sqrt( x*x + y*y + z*z );


                if( r_abs <= cutoff ) ans_r += erfc( r_abs*kappa ) / r_abs;
            }
        }
    }

    double ans = ans_k_new/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans;

}



double perturbed_ewald_HEG::Ewald_pair_index(Bead* a, Bead* b, int cnt)
{
    double ans;

    double ans_r = 0.0;
    double ans_k = 0.0;

// 	int cnt = (*p).r_index;
    double inverse_length = 1.0 / (*p).length;
    double kappa = (*p).kappa_int;
    double kappa_sq = kappa*kappa;

    for(int nx=-cnt;nx<cnt+1;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<cnt+1;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<cnt+1;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;
                double R_z = nz * (*p).length;

                double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );


                // Do not include the zero G-vector
                if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                {
                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double x = G_x * x_diff;
                    double y = G_y * y_diff;
                    double z = G_z * z_diff;

                    double cos_arg = 2.0*pi*( x + y + z );

// 					double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
// 					ans_k += ( cos(cos_arg) * my_exp ) / (pi * G_sq);
                    ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                } // end zero vector cond.


                double x = R_x + x_diff;
                double y = R_y + y_diff;
                double z = R_z + z_diff;

                double my_abs = sqrt( x*x + y*y + z*z );


                ans_r += erfc( kappa * my_abs ) / my_abs;




            } // end loop nz
        }
    } // end loop nx

    ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans;

}











double perturbed_ewald_HEG::Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;

                    double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                    double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                    double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );

                    // Do not include the zero G-vector
                    if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                    {
                        double G_sq = G_x_sq + G_y_sq + G_z_sq;

                        double x = G_x * x_diff;
                        double y = G_y * y_diff;
                        double z = G_z * z_diff;

                        double cos_arg = 2.0*pi*( x + y + z );

                        ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                    } // end zero vector cond.


                    double x = R_x + x_diff;
                    double y = R_y + y_diff;
                    double z = R_z + z_diff;

                    double my_abs = sqrt( x*x + y*y + z*z );


                    ans_r += erfc( kappa * my_abs ) / my_abs;



                } // end loop nz
            }
        } // end loop nx

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;


    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt - 1;

    // Return the Ewald pair interaction
    return ans;


}










double perturbed_ewald_HEG::Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    // allocate memory for final result of the force
    std::vector<double>force(3, 0.0);

    double last_force = -1000000.0;
    int cnt = 0;

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa_sq = kappa * kappa;

    while( abs( (last_force - force[0]) / last_force ) > tol )
    { // convergence cond
        cnt++;
        last_force = force[0];



        for(int iDim = 0; iDim<(*p).dim; iDim++)
        {
            force[iDim] = 0.0;
        }

        for(int nx=-cnt;nx<1+cnt;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;

            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<1+cnt;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;

                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<1+cnt;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;

                    double R_z = nz * (*p).length;

                    double r_x = (*a).get_coord(0) - (*b).get_coord(0);
                    double r_y = (*a).get_coord(1) - (*b).get_coord(1);
                    double r_z = (*a).get_coord(2) - (*b).get_coord(2);

                    double r_x_priam = r_x + R_x;
                    double r_y_priam = r_y + R_y;
                    double r_z_priam = r_z + R_z;

                    double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                    double R_priam = sqrt( R_priam_sq );

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                    force[0] += r_x_priam * tmp2;
                    force[1] += r_y_priam * tmp2;
                    force[2] += r_z_priam * tmp2;

                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double product = r_x * G_x + r_y * G_y + r_z * G_z;

                    double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                    if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                    {
                        force[0] += tmp * G_x;
                        force[1] += tmp * G_y;
                        force[2] += tmp * G_z;
                    }



                } // end nz
            } // end ny
        } // end nx;



        std::cout << "kappa: " << kappa << "\t cnt: " << cnt << "\t force[0]: " << force[0] << "\n";





    } // end of convergence cond.

    (*max_it) = cnt - 1;
    return force[0];



}











double perturbed_ewald_HEG::Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    // allocate memory for final result of the force
    std::vector<double>force(3, 0.0);

    double last_force = -1000000.0;
    int cnt = 0;

    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa_sq = kappa * kappa;

    double r_part = 0.0;
    double k_part = 0.0;

    while( true )
    { // convergence cond
        cnt = max_index;
        last_force = force[0];



        for(int iDim = 0; iDim<(*p).dim; iDim++)
        {
            force[iDim] = 0.0;
        }

        for(int nx=-cnt;nx<1+cnt;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;

            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<1+cnt;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;

                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<1+cnt;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;

                    double R_z = nz * (*p).length;

                    double r_x = (*a).get_coord(0) - (*b).get_coord(0);
                    double r_y = (*a).get_coord(1) - (*b).get_coord(1);
                    double r_z = (*a).get_coord(2) - (*b).get_coord(2);

                    double r_x_priam = r_x + R_x;
                    double r_y_priam = r_y + R_y;
                    double r_z_priam = r_z + R_z;

                    double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                    double R_priam = sqrt( R_priam_sq );

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                    force[0] += r_x_priam * tmp2;
                    force[1] += r_y_priam * tmp2;
                    force[2] += r_z_priam * tmp2;

                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double product = r_x * G_x + r_y * G_y + r_z * G_z;

                    double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                    if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                    {
                        k_part += tmp * G_x;;
                        force[0] += tmp * G_x;
                        force[1] += tmp * G_y;
                        force[2] += tmp * G_z;
                    }


                    r_part += r_x_priam * tmp2;


                } // end nz
            } // end ny
        } // end nx;



        std::cout << "kappa: " << kappa << "\t cnt: " << cnt << "\t force[0]: " << force[0] << "\t r_part: " << r_part << "\t k_part: " << k_part << "\n";



        break;

    } // end of convergence cond.

    (*max_it) = cnt - 1;
    return force[0];



}




















































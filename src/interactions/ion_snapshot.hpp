#pragma once

#include <memory>

#include "../particles/pair_action_lookup_table.hpp"
// ###################################################################################################
// ############# PIMC for electrons with an ion-background potential in BO approximation #############
// ###################################################################################################


// TBD: Implement forces, for PB-PIMC and some such
class ion_snapshot
{

public:

    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b){ return 0.0; };
    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };

    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);

    // Return the spatial difference between two positions within the simulation cell
    double get_diff(double a, double b);




	
	// Obtain information about different terms in the external potential, for benchmarking purposes
	std::vector<double> ext_pot_info(Bead* a);

	
	

    // Calculate the Madelung constant
    double get_madelung_constant(double tol, int* max_it, double kappa, int max_index);





    // Calculate the interaction energy between two beads to find optimum kappa value
    double Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);

    // modified Ewald pair interaction with a specified r_index:
    double Ewald_pair_index(Bead* a, Bead* b, int cnt);

    // Calculate the Ewald force between two beads to find the optimum kappa value
    double Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);
    double Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);



    // Dummy Methods in order to get the pair-action toolbox requirements satisfied
    std::vector<double> pair_action(Bead*&, Bead*&, Bead*&, Bead*&);
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 );
	std::vector<double> ext_pot_action_info( Bead* A_0, Bead* A_1 );
	
	// Special Kelbg potential function
	double Kelbg(double r, double lambda);
	

private:

    std::shared_ptr<Parameters> p;

};







double ion_snapshot::Kelbg(double r, double lambda)
{
	double x = r/lambda;
	return ( 1.0 - exp(-x*x) + sqrt(M_PI)*x*( 1.0 - erf(x) ) ) / r;
}





std::vector<double> ion_snapshot::ext_pot_action_info( Bead* A_0, Bead* A_1 )
{
	auto vec_a0 = A_0->get_all_coords();
	auto vec_a1 = A_1->get_all_coords();
	
	int n_ions = p->ions.get_num_ions();
	
	double ans_action = 0.0;
	double ans_action_derivative = 0.0;
	
	double ans_primitive = 0.0;
	double ans_Kelbg = 0.0;
	
	double dx0, dx1;
	
	for(int ion_id=0;ion_id<n_ions;ion_id++)
	{
		auto ion = p->ions.get_ion(ion_id);
        auto ion_coords = ion.get_coords();
		
		// Compute the thermal wave length parameter of the Kelbg potential: 
		double Kelbg_lambda = sqrt(0.5*p->epsilon*(1.0/p->mass_spec_1 + 1.0/ion.get_mass()));
		
		double r0 = 0.0;
		double r1 = 0.0;
		
		// Compute the difference between the electron and this particular ion for all dimensions
		std::vector<double> diff_vec_0, diff_vec_1;
		for(int iDim=0;iDim<p->dim;iDim++)
		{
			double tmp_0 = vec_a0[iDim]-ion_coords[iDim];
			double tmp_1 = vec_a1[iDim]-ion_coords[iDim];
			
			if( tmp_0 < -0.5*p->length ) tmp_0 += p->length;
			if( tmp_0 > 0.5*p->length ) tmp_0 -= p->length;
			
			if( tmp_1 < -0.5*p->length ) tmp_1 += p->length;
			if( tmp_1 > 0.5*p->length ) tmp_1 -= p->length;
			
			diff_vec_0.push_back( tmp_0 );
			diff_vec_1.push_back( tmp_1 );
			
			r0 += tmp_0*tmp_0;
			r1 += tmp_1*tmp_1;
		}
		
		// Now obtain the pair-potential between the components 
        auto u_pair_vec = p->table_ptr->get_pair_action(diff_vec_0, diff_vec_1);
		
		r0 = sqrt(r0);
		r1 = sqrt(r1);
		
		ans_primitive += 0.5*( 1.0/r0 + 1.0/r1 )*p->charge_spec_2 * p->charge_spec_1;
		ans_Kelbg += 0.5*( Kelbg( r0, Kelbg_lambda ) + Kelbg( r1, Kelbg_lambda ) )*p->charge_spec_2 * p->charge_spec_1;
		
		ans_action += u_pair_vec[0];
		ans_action_derivative += u_pair_vec[1];
		
		if( ion_id == 0 )
		{
			dx0 = diff_vec_0[0];
			dx1 = diff_vec_1[0];
		}
		
	} // end loop ion_id
	
	
	
	return { ans_action, ans_primitive, ans_Kelbg, dx0, dx1 };
	
}







std::vector<double> ion_snapshot::ext_pot_action( Bead* A_0, Bead* A_1 )
{
	auto vec_a0 = A_0->get_all_coords();
	auto vec_a1 = A_1->get_all_coords();
	
	int n_ions = p->ions.get_num_ions();
	
	double ans_action = 0.0;
	double ans_action_derivative = 0.0;
	
	
	for(int ion_id=0;ion_id<n_ions;ion_id++)
	{
		auto ion = p->ions.get_ion(ion_id);
        auto ion_coords = ion.get_coords();
		
		// Compute the difference between the electron and this particular ion for all dimensions
		std::vector<double> diff_vec_0, diff_vec_1;
		for(int iDim=0;iDim<p->dim;iDim++)
		{
			double tmp_0 = vec_a0[iDim]-ion_coords[iDim];
			double tmp_1 = vec_a1[iDim]-ion_coords[iDim];
			
			if( tmp_0 < -0.5*p->length ) tmp_0 += p->length;
			if( tmp_0 > 0.5*p->length ) tmp_0 -= p->length;
			
			if( tmp_1 < -0.5*p->length ) tmp_1 += p->length;
			if( tmp_1 > 0.5*p->length ) tmp_1 -= p->length;
			
			diff_vec_0.push_back( tmp_0 );
			diff_vec_1.push_back( tmp_1 );
			
			
		}
		
		// Now obtain the pair-potential between the components 
        auto u_pair_vec = p->table_ptr->get_pair_action(diff_vec_0, diff_vec_1);
		
		ans_action += u_pair_vec[0];
		ans_action_derivative += u_pair_vec[1];
		
	} // end loop ion_id
	
	
	
	return { ans_action, ans_action_derivative };
	
}



























void ion_snapshot::init(Parameters* new_p)
{
    p = std::make_shared<Parameters>(*new_p);
}






std::vector<double> ion_snapshot::ext_pot_info(Bead* a)
{
    /**
     * This method calculated the ewald sum for fixed ion positions, which are contained in the ion-container.
     */
	
	std::vector<double> outcome;

    double ans = 0.0;
    double last_ans = 10000.0;

    int cnt = 0;
    int max_index = 20;
    double length = p->length;
    double inv_length = 1.0 / p->length;
    double tol = 1e-10;
    double G_fac = 2.0 * M_PI / p->length;

    double kappa = p->kappa_int;

	

    int n_ions = p->ions.get_num_ions();
    auto bead_coords = a->get_all_coords();



    double U_pair = 0.0;
    // Iterate over all ions in the Box and use the nearest neighbor convention to obtain the pair-potential
    std::vector<double> GX(p->k_index);
    std::vector<double> GY(p->k_index);
    std::vector<double> GZ(p->k_index);

	double ans_nn = 0.0;
	double ans_rest = 0.0;
	
	double ans_Coulomb = 0.0;
	
	std::cout << "~~~~~ ext_pot_info() verbose ~~~~~" << "\n";
	std::cout << "q_electron: " << p->charge_spec_1 << "\tq_ion: " << p->charge_spec_2 << "\n";
	std::cout << "n_ions: " << n_ions << "\n";
	std::cout << "n_bead: " << p->n_bead << "\n";
	
	

    for(int ion_id = 0; ion_id < n_ions; ion_id++)
    {
// 		std::cout << "-----ion_id: " << ion_id << "\n";
		
		double ans_r = 0.0;
		double ans_k = 0.0;
		
		double ans_r_nn = 0.0;
		double ans_r_rest = 0.0;
		
        auto ion = p->ions.get_ion(ion_id);
        auto ion_coords = ion.get_coords();
		
		
		// Compute the thermal wave length parameter of the Kelbg potential: 
		double Kelbg_lambda = sqrt(0.5*p->epsilon*(1.0/p->mass_spec_1 + 1.0/ion.get_mass()));
		
		
        std::vector<double> diff_vec = {bead_coords[0] - ion_coords[0], bead_coords[1] - ion_coords[1],
                                        bead_coords[2] - ion_coords[2]};

        // Check if nearest image projection is necessary
        for (int dim = 0; dim < p->dim; dim++)
        {
// 			std::cout << "diff_vec_before: " << diff_vec[dim] << "\n";
			
            if (diff_vec[dim] > 0.5 * length)
            {

                diff_vec[dim] -= length;
                ion_coords[dim] -= length;
            }

            if (diff_vec[dim] < -0.5 * length)
            {
                diff_vec[dim] += length;
                ion_coords[dim] += length;
            }
            
//             std::cout << "diff_vec_after: " << diff_vec[dim] << "\n";
        }

        // Now obtain the pair-potential between the components to add it later to the result or the specific ion
         auto u_pair_vec = p->table_ptr->get_pair_action(diff_vec, diff_vec);
 		ans_nn += u_pair_vec[0];


        // Pre-Computation of the cosine factors
        for (int iK = 0; iK < p->k_index + 1; iK++)
        {
            GX[iK] = cos(iK * G_fac * diff_vec[0]);
            GY[iK] = cos(iK * G_fac * diff_vec[1]);
            GZ[iK] = cos(iK * G_fac * diff_vec[2]);
        }

        // First evaluate the reciprocal space sum
        double ans_k_new = 0.0;
        for (int iX = -p->k_index; iX < p->k_index + 1; iX++) {
            int x_index = abs(iX);
            for (int iY = -p->k_index; iY < p->k_index + 1; iY++) {
                int y_index = abs(iY);
                for (int iZ = -p->k_index; iZ < p->k_index + 1; iZ++) {
                    int z_index = abs(iZ);
                    double cos_term = GX[x_index] * GY[y_index] * GZ[z_index];
                    double my_exp = p->get_Ewald_exp(iX, iY, iZ);
                    if ((iX != 0) || (iY != 0) || (iZ != 0)) {
                        ans_k_new += cos_term * my_exp / pi;
                    }
                }

            }
        }

        int max_r = 1 + int(p->r_index / 4);
        double cutoff = p->r_index * 0.250 * p->length;

        // Then evaluate the real-space sum
        for (int iX = -max_r; iX < max_r + 1; iX++)
        {
            double x = diff_vec[0] + p->length * iX;
            for (int iY = -max_r; iY < max_r + 1; iY++)
            {
                double y = diff_vec[1] + p->length * iY;
                for (int iZ = -max_r; iZ < max_r + 1; iZ++)
                {
                    double z = diff_vec[2] + p->length * iZ;
                    double r_abs = sqrt(x * x + y * y + z * z);

                    if (r_abs <= cutoff) {

                        if ((fabs(x) <= p->length * 0.5) && (fabs(y) <= p->length * 0.5) && (fabs(z) <= p->length * 0.5))
                        {
                            ans_r -= erf(r_abs * kappa) / r_abs;
							ans_Coulomb += p->charge_spec_2 * p->charge_spec_1 / r_abs;
// 							std::cout << "Coulomb-contrib/eps: " << p->charge_spec_2 * p->charge_spec_1 / r_abs / p->epsilon << "\n";
                        } else
                        {
                            ans_r += erfc(r_abs * kappa) / r_abs;
                        }
                    }
                }
            }
        }


        // Add the k-sum part, the real-space part and the pair-action part together
        ans += (ans_k_new / (p->volume) - pi/(kappa * kappa * p->volume) + ans_r) * p->charge_spec_2 * p->charge_spec_1;
		ans_nn += ans_r_nn * p->charge_spec_2 * p->charge_spec_1;
		ans_rest += (ans_k_new / (p->volume) - pi/(kappa * kappa * p->volume) + ans_r - ans_nn) * p->charge_spec_2 * p->charge_spec_1;

    } // end loop over all ions
    
    
    
    
    // As the final step, add external harmonic potential
    
	double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double cos_arg = ( 2.0*pi/(*p).length * ( (*p).pq_x*x_a + (*p).pq_y*y_a + (*p).pq_z*z_a ) );
    double cos_arg2 = ( 2.0*pi/p->length * ( p->pq_x2*x_a + p->pq_y2*y_a + p->pq_z2*z_a ) );
    double cos_arg3 = ( 2.0*pi/p->length * ( p->pq_x3*x_a + p->pq_y3*y_a + p->pq_z3*z_a ) );

    
    double harmonic_pot =   2.0*( cos( cos_arg )*(*p).vq + cos( cos_arg2 )*p->vq2 + cos( cos_arg3 )*p->vq3 );
    
    double Madelung_contribution = 0.50*(*p).madelung;
    
    
    // Write the different contributions to the external potential to the outcome vector:
    outcome.push_back( ans_nn ); // ### 1
	outcome.push_back( ans ); // ### 2
	outcome.push_back( harmonic_pot ); // ### 3
	outcome.push_back( Madelung_contribution ); // ### 4
    outcome.push_back( ans_Coulomb ); // ### 5

    return outcome;
}











double ion_snapshot::ext_pot(Bead* a)
{
    /**
     * This method calculated the ewald sum for fixed ion positions, which are contained in the ion-container.
     */

    double ans = 0.0;
    double last_ans = 10000.0;

    int cnt = 0;
    int max_index = 20;
    double length = p->length;
    double inv_length = 1.0 / p->length;
    double tol = 1e-10;
    double G_fac = 2.0 * M_PI / p->length;
    double kappa = p->kappa_int;

	

    int n_ions = p->ions.get_num_ions();
    auto bead_coords = a->get_all_coords();



    double U_pair = 0.0;
    // Iterate over all ions in the Box and use the nearest neighbor convention to obtain the pair-potential
    std::vector<double> GX(p->k_index);
    std::vector<double> GY(p->k_index);
    std::vector<double> GZ(p->k_index);

	

    for(int ion_id = 0; ion_id < n_ions; ion_id++)
    {
		
		double ans_r = 0.0;
		double ans_k = 0.0;
		
        auto ion = p->ions.get_ion(ion_id);
        auto ion_coords = ion.get_coords();
		
		
		// Compute the thermal wave length parameter of the Kelbg potential: 
		double Kelbg_lambda = sqrt(0.5*p->epsilon*(1.0/p->mass_spec_1 + 1.0/ion.get_mass()));
		
		
        std::vector<double> diff_vec = {bead_coords[0] - ion_coords[0], bead_coords[1] - ion_coords[1],
                                        bead_coords[2] - ion_coords[2]};

        // Check if nearest image projection is necessary
        for (int dim = 0; dim < p->dim; dim++)
        {
            if (diff_vec[dim] > 0.5 * length)
            {

                diff_vec[dim] -= length;
                ion_coords[dim] -= length;
            }

            if (diff_vec[dim] < -0.5 * length)
            {
                diff_vec[dim] += length;
                ion_coords[dim] += length;
            }
        }


        // Pre-Computation of the cosine factors
        for (int iK = 0; iK < p->k_index + 1; iK++)
        {
            GX[iK] = cos(iK * G_fac * diff_vec[0]);
            GY[iK] = cos(iK * G_fac * diff_vec[1]);
            GZ[iK] = cos(iK * G_fac * diff_vec[2]);
        }

        // First evaluate the reciprocal space sum
        double ans_k_new = 0.0;
        for (int iX = -p->k_index; iX < p->k_index + 1; iX++) {
            int x_index = abs(iX);
            for (int iY = -p->k_index; iY < p->k_index + 1; iY++) {
                int y_index = abs(iY);
                for (int iZ = -p->k_index; iZ < p->k_index + 1; iZ++) {
                    int z_index = abs(iZ);
                    double cos_term = GX[x_index] * GY[y_index] * GZ[z_index];
                    double my_exp = p->get_Ewald_exp(iX, iY, iZ);
                    if ((iX != 0) || (iY != 0) || (iZ != 0)) {
                        ans_k_new += cos_term * my_exp / pi;
                    }
                }

            }
        }

        int max_r = 1 + int(p->r_index / 4);
        double cutoff = p->r_index * 0.250 * p->length;

        // Then evaluate the real-space sum
        for (int iX = -max_r; iX < max_r + 1; iX++)
        {
            double x = diff_vec[0] + p->length * iX;
            for (int iY = -max_r; iY < max_r + 1; iY++)
            {
                double y = diff_vec[1] + p->length * iY;
                for (int iZ = -max_r; iZ < max_r + 1; iZ++)
                {
                    double z = diff_vec[2] + p->length * iZ;
                    double r_abs = sqrt(x * x + y * y + z * z);

                    if (r_abs <= cutoff) {

                        if ((fabs(x) <= p->length * 0.5) && (fabs(y) <= p->length * 0.5) && (fabs(z) <= p->length * 0.5))
                        {
                             ans_r += ( - erf(r_abs * kappa) / r_abs );              // erfc() = 1-erf()
                        } else
                        {
                            ans_r += erfc(r_abs * kappa) / r_abs;
                        }
                    }
                }
            }
        }


        // Add the k-sum part, the real-space part and the pair-action part together
        ans += (ans_k_new / (p->volume) - pi/(kappa * kappa * p->volume) + ans_r) * p->charge_spec_2 * p->charge_spec_1;

		
    } // end loop over all ions
    
    
    
    
    // As the final step, add external harmonic potential
    
	double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double cos_arg = ( 2.0*pi/(*p).length * ( (*p).pq_x*x_a + (*p).pq_y*y_a + (*p).pq_z*z_a ) );
    double cos_arg2 = ( 2.0*pi/p->length * ( p->pq_x2*x_a + p->pq_y2*y_a + p->pq_z2*z_a ) );
    double cos_arg3 = ( 2.0*pi/p->length * ( p->pq_x3*x_a + p->pq_y3*y_a + p->pq_z3*z_a ) );

    
    double harmonic_pot =   2.0*( cos( cos_arg )*(*p).vq + cos( cos_arg2 )*p->vq2 + cos( cos_arg3 )*p->vq3 );
    
    double Madelung_contribution = 0.50*(*p).madelung;
    
    
    
//     std::cout << "Ion_madelung_contrib: " << Madelung_contribution << "\n";
    

    return ans + harmonic_pot + Madelung_contribution;
}







double ion_snapshot::pair_interaction(Bead* a, Bead* b)
{
    double G_fac = 2.0*pi/p->length;

    double ans_r = 0.0;
    double ans_k = 0.0;

    double kappa = p->kappa_int;

    double dx = a->get_coord(0) - b->get_coord(0);
    double dy = a->get_coord(1) - b->get_coord(1);
    double dz = a->get_coord(2) - b->get_coord(2);



    // Pre-compute the cosine-factors for the reciprocal part:
    std::vector<double> GX,GY,GZ;
    for(int iK=0;iK<p->k_index+1;iK++)
    {
        GX.push_back( cos(iK*G_fac*dx) );
        GY.push_back( cos(iK*G_fac*dy) );
        GZ.push_back( cos(iK*G_fac*dz) );
    }


    double ans_k_new = 0.0;
    // Evaluate the sum in reciprocal space:
    for(int iX=-p->k_index;iX<p->k_index+1;iX++)
    {
        int x_index = abs( iX );
        for(int iY=-p->k_index;iY<p->k_index+1;iY++)
        {
            int y_index = abs( iY );
            for(int iZ=-p->k_index;iZ<p->k_index+1;iZ++)
            {
                int z_index = abs( iZ );

//  				double G_sq = ( iX*iX + iY*iY + iZ*iZ ) / ( p->length * p->length );

                double cos_term = GX[ x_index ]*GY[ y_index ]*GZ[ z_index ];
// 				double my_exp = exp( -pi*pi*G_sq/(kappa*kappa) );
                double my_exp = p->get_Ewald_exp(iX, iY, iZ);

                if( ( iX != 0 ) || ( iY != 0 ) || ( iZ != 0 ) )
                {
                    ans_k_new +=  cos_term * my_exp / pi;
                }

            }
        }
    }

    int max_r = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    // Evaluate the sum in real space:
    for(int iX=-max_r;iX<max_r+1;iX++)
    {
        double x = dx + p->length*iX;
        for(int iY=-max_r;iY<max_r+1;iY++)
        {
            double y = dy + p->length*iY;
            for(int iZ=-max_r;iZ<max_r+1;iZ++)
            {
                double z = dz + p->length*iZ;

                double r_abs = sqrt( x*x + y*y + z*z );

                if( r_abs <= cutoff ) ans_r += erfc( r_abs*kappa ) / r_abs;
            }
        }
    }

    // 		double ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;
    double ans = ans_k_new/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

   // 		std::cout << "ans_k_new: " << ans_k_new << "\t ans_r: " << ans_r << "\t pi: " << pi/( kappa*kappa*(*p).volume ) << "\n";

    return ans;
}

double ion_snapshot::distance(Bead* a, Bead* b)
{
    return sqrt( distance_sq(a, b) );
}



double ion_snapshot::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
// 		double tmp = get_diff( (*a).get_coord(i), (*b).get_coord(i) );
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        while( tmp > (*p).length*0.5 )
        {
            tmp = tmp - (*p).length;
        }
        while( tmp < -(*p).length*0.5 )
        {
            tmp = tmp + (*p).length;
        }


        ans += tmp * tmp;
    }
    return ans;
}


double ion_snapshot::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
//	double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}

double ion_snapshot::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}


double ion_snapshot::get_diff(double a, double b)
{
    double tmp = a - b;

    return tmp;
}

double ion_snapshot::get_madelung_constant(double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;
            double R_x_sq = R_x * R_x;
            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;
                double R_y_sq = R_y * R_y;
                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;
                    double R_z_sq = R_z * R_z;


                    // Do not include the zero G-vector
                    if( !( ( nx == 0 ) && ( ny == 0 ) && ( nz == 0 ) ) )
                    {

                        double G_sq = G_x_sq + G_y_sq + G_z_sq;
                        double r_abs = sqrt( R_x_sq + R_y_sq + R_z_sq );

                        ans_k += exp( -pi*pi*G_sq/kappa_sq ) / (pi * G_sq);
                        ans_r += erfc( kappa * r_abs ) / r_abs;

                    } // end zero vector cond.


                } // end loop nz
            }
        } // end loop nx

// 		std::cout << "k_cnt: " << cnt << "\t ans: " << ans << "\n";

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r - 2.0*kappa/sqrt(pi);




    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt-1;

    // Return reciprocal part of Madelung const:
    return ans;

}




double ion_snapshot::Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;

                    double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                    double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                    double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );

                    // Do not include the zero G-vector
                    if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                    {
                        double G_sq = G_x_sq + G_y_sq + G_z_sq;

                        double x = G_x * x_diff;
                        double y = G_y * y_diff;
                        double z = G_z * z_diff;

                        double cos_arg = 2.0*pi*( x + y + z );

                        ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                    } // end zero vector cond.


                    double x = R_x + x_diff;
                    double y = R_y + y_diff;
                    double z = R_z + z_diff;

                    double my_abs = sqrt( x*x + y*y + z*z );


                    ans_r += erfc( kappa * my_abs ) / my_abs;



                } // end loop nz
            }
        } // end loop nx

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;


    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt - 1;

    // Return the Ewald pair interaction
    return ans;


}


double ion_snapshot::Ewald_pair_index(Bead* a, Bead* b, int cnt)
{
    double ans;

    double ans_r = 0.0;
    double ans_k = 0.0;

// 	int cnt = (*p).r_index;
    double inverse_length = 1.0 / (*p).length;
    double kappa = (*p).kappa_int;
    double kappa_sq = kappa*kappa;

    for(int nx=-cnt;nx<cnt+1;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<cnt+1;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<cnt+1;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;
                double R_z = nz * (*p).length;

                double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );


                // Do not include the zero G-vector
                if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                {
                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double x = G_x * x_diff;
                    double y = G_y * y_diff;
                    double z = G_z * z_diff;

                    double cos_arg = 2.0*pi*( x + y + z );

// 					double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
// 					ans_k += ( cos(cos_arg) * my_exp ) / (pi * G_sq);
                    ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                } // end zero vector cond.


                double x = R_x + x_diff;
                double y = R_y + y_diff;
                double z = R_z + z_diff;

                double my_abs = sqrt( x*x + y*y + z*z );


                ans_r += erfc( kappa * my_abs ) / my_abs;




            } // end loop nz
        }
    } // end loop nx

    ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans;

}

double ion_snapshot::Ewald_force(Bead *a, Bead *b, double tol, int *max_it, double kappa, int max_index)
{
    return 0;
}

double ion_snapshot::Ewald_force_cnt(Bead *a, Bead *b, double tol, int *max_it, double kappa, int max_index)
{
    return 0;
}

void ion_snapshot::ext_force_on_a(Bead *a, std::vector<double> *force)
{

}

void ion_snapshot::pair_force_on_a(Bead *a, Bead *b, std::vector<double> *force)
{

}



std::vector<double> ion_snapshot::pair_action(Bead *&, Bead *&, Bead *&, Bead *&)
{
    std::vector<double> dummy_res{0.0, 0.0, 0.0, 0.0};
    return dummy_res;
}

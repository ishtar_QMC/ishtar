#pragma once


// ###############################################################################################
// ##### homogeneous electron gas in PBC with Yakub/Ronchi interaction ###########################
// ###############################################################################################


class yakub_HEG
{
public:
    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b){ return 0.0; };
    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };
    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);


    double yakub_rm;
    double yakub_const;
    double yk_inv_cube;
    double yk_last;

    int N_tot; // total number of particles in the system;
	
	// Return the pair-action compotent of the total action. Zero in this case
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 ){ std::vector result{0.0,0.0,0.0,0.0}; return result; }
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }

private:


    // Pointer to the Parameters
    Parameters* p;




};


void yakub_HEG::init( Parameters* new_p )
{
    p = new_p;
    N_tot = (*p).N_tot;

    // obtain the characterisitc yakub-radius:
    yakub_rm = (*p).length * pow( 3.0*0.25/pi, 1.0/3.0 );

    // obtain the constant part of the potential:
    yakub_const = - 3.0*0.25*( 1.0 + N_tot*1.0/5.0 ) / yakub_rm; // self-derived expression
    //	std::cout << "yakub_const: " << yakub_const << "\t N_tot: " << N_tot << std::endl;

    // obtain the inverse cube of yakub_rm:
    yk_inv_cube = 1.0 / pow( yakub_rm, 3.0 );


    yk_last = 1.5/yakub_rm;
}




double yakub_HEG::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}



double yakub_HEG::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

// 	double norm = 1.0;
    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
//

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }


    return ans / norm;

}



double yakub_HEG::ext_pot(Bead* a)
{
    return yakub_const;
}


void yakub_HEG::ext_force_on_a(Bead* a, std::vector<double>* force)
{
    for(int i=0;i<(*p).dim;i++)
    {
        (*force)[i] = 0.0;
    }
}


void yakub_HEG::pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force)
{
    double L = (*p).length;

    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double x_b = (*b).get_coord( 0 );
    double y_b = (*b).get_coord( 1 );
    double z_b = (*b).get_coord( 2 );

    double f_x = 0.0;
    double f_y = 0.0;
    double f_z = 0.0;

    for(int nx=-1;nx<=1;nx++)
    {
        double r_x = x_a - x_b - nx*L;

        for(int ny=-1;ny<=1;ny++)
        {
            double r_y = y_a - y_b - ny*L;

            for(int nz=-1;nz<=1;nz++)
            {
                double r_z = z_a - z_b - nz*L;

                double r_ans_sq = ( r_x*r_x + r_y*r_y + r_z*r_z );
                double r_ans = sqrt( r_ans_sq );


                if( r_ans < yakub_rm ) // only count those images within the yakub-sphere
                {
                    double tmp = ( yk_inv_cube + 1.0/( r_ans*r_ans_sq ) );
                    f_x += -r_x * tmp;
                    f_y += -r_y * tmp;
                    f_z += -r_z * tmp;
                } // end yakub-sphere condition



            } // end loop nz
        } // end loop ny
    } // end loop nz


    (*force)[0] = f_x;
    (*force)[1] = f_y;
    (*force)[2] = f_z;


}




double yakub_HEG::distance(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        ans += tmp * tmp;
    }
    return sqrt(ans);
}



double yakub_HEG::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        ans += tmp * tmp;
    }
    return ans;
}



double yakub_HEG::pair_interaction(Bead* a, Bead* b)
{

    double L = (*p).length;

    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double x_b = (*b).get_coord( 0 );
    double y_b = (*b).get_coord( 1 );
    double z_b = (*b).get_coord( 2 );

    double ans = 0.0;

    for(int nx=-1;nx<=1;nx++)
    {
        double r_x = x_a - x_b - nx*L;

        for(int ny=-1;ny<=1;ny++)
        {
            double r_y = y_a - y_b - ny*L;

            for(int nz=-1;nz<=1;nz++)
            {
                double r_z = z_a - z_b - nz*L;

                double r_ans_sq = ( r_x*r_x + r_y*r_y + r_z*r_z );
                double r_ans = sqrt( r_ans_sq );


                if( r_ans < yakub_rm ) // only count those images within the yakub-sphere
                {
                    ans += 1.0/r_ans + r_ans_sq*0.50*yk_inv_cube - yk_last;
                } // end yakub-sphere condition



            } // end loop nz
        } // end loop ny
    } // end loop nz


    return ans;

}

















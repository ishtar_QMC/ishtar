/*
 * Class: Diffusion toolbox
 */



template <class inter> class diffusion_toolbox
{
public:
	// Set the pointer to a configuration
	void init(worm_configuration* new_config);

	
	/*
	 * The beads of particles 'start_particle' end 'end_particle' have been changed:
	 * - next_start and next_end are ids of the missing beads on the slices
	 * - blist_start and blist_end are pointers to the affected beadlists
	 * - start_bead and end_bead are the new changed beads
	 * - scale is imaginary time scaling factor
	 */
	void change_matrix(std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end, double scale, int start_particle, int end_particle, Bead* start_bead, Bead* end_bead, Matrix* old_matrix, Matrix* new_matrix);
	
	
	
	/*
	 * The diffusion matrix of the head is changed during the Swap update
	 * - next_start and next_end are ids of the missing beads on the slices
	 * - blist_start and blist_end are pointers to the affected beadlists
	 * - old_ and new_head_particle are particle IDs of old and new head, end_particle the particle ID of the changed bead on end slice of the transition
	 * - Bead 'end' is the new end bead of the transition
	 * 
	 * Due to the change of the head, the old head now servers as start bead for a transition and the new head not any more!
	 */
	void swap_matrix(std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end , double scale, int old_head_particle, int new_head_particle, int end_particle, Bead* end, Matrix* old_kinetic_matrix, Matrix* new_kinetic_matrix);
		
	
	
	
	
	/*
	 * The particles with numbers 'start_particle' and 'end_particle' are removed from the diffusion matrix
	 * If start or end slice are not affected, set the particle = -1
	 * 
	 */
	void delete_matrix(int start_particle, int end_particle, Matrix* old_kinetic_matrix, Matrix* new_kinetic_matrix);

	
	
	/*
	 * The beads start and end are added to a diffusion transition
	 * If only a start, but no end bead is added, se end_particle = -1
	 * 
	 */
	void add_matrix(std::vector<int> *blist_start, std::vector<int> *blist_end, Bead* start, Bead* end, double scale, int next_start, int next_end, int start_particle, int end_particle, Matrix* old_kinetic_matrix, Matrix* new_kinetic_matrix);
	
	
	
	
	
	/*
	 * Calculate the entire diffusion matrix
	 * - Use for init, and debugging
	 */
	void get_matrix(Matrix* m, std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end, double scale);
	
	
	
	// Calculate the entire diffusion matrix, but take a pointer to the worm_config:
	void ptr_get_matrix( worm_configuration* ptr_config, Matrix* m, std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end, double scale);
	
	
	
	

	
	
private:
	// Information about the configuration:
	worm_configuration* config;
	
	
	inter my_interaction;
	
};


template <class inter> void diffusion_toolbox<inter>::init(worm_configuration* new_config)
{
	config = new_config;
	my_interaction.init( &(*config).params );
}







template <class inter> void diffusion_toolbox<inter>::add_matrix(std::vector<int> *blist_start, std::vector<int> *blist_end, Bead* start, Bead* end, double scale, int next_start, int next_end, int start_particle, int end_particle, Matrix* old_kinetic_matrix, Matrix* new_kinetic_matrix)
{
	// Obtain the old diffusion matrix
	(*new_kinetic_matrix).copy(old_kinetic_matrix);
	
	for(int iX=0;iX<(*config).params.N;iX++) // loop over the entire row from the changed start_bead:
	{
		int end_id = (*blist_end)[iX];

		if( end_id != next_end ) // only consider transition with pre-existing beads
		{
// 			double element = rho_fun(start, &beads[end_id], &params, scale);
			double element = my_interaction.rho(start, &(*config).beads[end_id], scale);
			(*new_kinetic_matrix).set_value(iX, start_particle, element);
		}
	}
	
	
	
	
	if( end_particle >= 0 ){ // Check, whether an end bead is affected
		for(int iY=0;iY<(*config).params.N;iY++) // loop over the entire column of the added end_bead
		{
			int start_id = (*blist_start)[iY];

			if( start_id != next_start ) // only consider transition with pre-existing beads
			{
// 				double element = rho_fun(end, &beads[start_id], &params, scale);
				double element = my_interaction.rho(end, &(*config).beads[start_id], scale);
				(*new_kinetic_matrix).set_value(end_particle, iY, element);
			}
		}
		
		// Consider the diffusion element from changed start and end:
		double element = my_interaction.rho(end, start, scale);
		(*new_kinetic_matrix).set_value(end_particle, start_particle, element);
	}

	return;
}




















template <class inter> void diffusion_toolbox<inter>::delete_matrix(int start_particle, int end_particle, Matrix* old_kinetic_matrix, Matrix* new_kinetic_matrix)
{
	// Obtain the old diffusion matrix
	(*new_kinetic_matrix).copy(old_kinetic_matrix);
	
	
	if( end_particle >= 0 ) // Check, whether a particle is removed on end slice of transition:
	{
		for(int iY=0;iY<(*config).params.N;iY++)
		{
			(*new_kinetic_matrix).set_value(end_particle, iY, 0.0); // Replace diffusion elements with zero
		}
	}
	
	
	if(start_particle >= 0) // Check, whether a particle is removed on start slice of transition:
	{
		for(int iX=0;iX<(*config).params.N;iX++)
		{
			(*new_kinetic_matrix).set_value(iX, start_particle, 1.0); // Replace diffusion elements with unity
		}
	}
		

	
	return;
	
}














template <class inter> void diffusion_toolbox<inter>::swap_matrix(std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end , double scale, int old_head_particle, int new_head_particle, int end_particle, Bead* end, Matrix* old_kinetic_matrix, Matrix* new_kinetic_matrix)
{
	
	// Obtain the old diffusion matrix
	(*new_kinetic_matrix).copy(old_kinetic_matrix);

	
	// Replace the former row of the old_head_particle with the matrix elements of the old_head_particle, i.e. 1
	for(int i=0;i<(*config).params.N;i++)
	{
// 		(*new_kinetic_matrix).set_value(i, new_head_particle, (*new_kinetic_matrix).get_value(i, old_head_particle) ); -> is unity anyway ;)
		(*new_kinetic_matrix).set_value(i, new_head_particle, 1.0 );
	}

	
	// Calculate the diffusion elements of the old head, which is not the head after the update any more
	// The old head is the start bead of this transition
	// Calculate the column which is modified due to Bead 'end'
	for(int i=0;i<(*config).params.N;i++)
	{
		// Obtain the id of the end bead of the transition starting from the old head:
		int end_id = (*blist_end)[i];

		if( (end_id != next_end ) && ( i != end_particle ) ) // Check whether the end bead exists and whether it has been re-sampled by swap
		{
// 			double element = rho_fun(new_head, &beads[end_id], &params, scale);
			double element = my_interaction.rho( &(*config).beads[(*config).head_id], &(*config).beads[end_id], scale);
			(*new_kinetic_matrix).set_value(i, old_head_particle, element);

		}
		else if(i == end_particle) // Calculate the matrix element when the end bead has been re-sampled by the swap:
		{
// 			double element = rho_fun(new_head, end, &params, scale);
			double element = my_interaction.rho( &(*config).beads[(*config).head_id], end, scale );
			(*new_kinetic_matrix).set_value(i, old_head_particle, element);

		}
		else // Otherwise, this transition does not exist and the element is zero
		{
			(*new_kinetic_matrix).set_value(i, old_head_particle, 0.0);
		}
		
		

		
		// Re-calculate the column which corresponds to the changed 'end' bead
		// obtain the id of the start bead of the transition
		int start_id = (*blist_start)[i];

		
		if( ( i != new_head_particle ) && ( start_id != next_start ) ) // Do not include non-existing beads and the transition of the new head after swap
		{
// 			double element = rho_fun(end, &beads[start_id], &params, scale);
			double element = my_interaction.rho( end, &(*config).beads[start_id], scale);
			(*new_kinetic_matrix).set_value(end_particle, i, element);

		}
		
		
		
		
	} // end loop over all particles
	
	
	
	
	
	
	
}











/*
 * Calculates the new diffusion matrix after beads 'start_particle' or 'end_particle' (or one of them) have been modified
 * --> start_particle and end_particle denote the particle index, NOT the position in beads[...]
 * --> set start_particle = -1 or end_particle = -1 if they are not changed
 * blist_start and blist_end point to the beadlists of the affected slices
 * next_start and next_end indicate the missing beads on the affected slices
 * scale indicates the imaginary time difference between the non-equidistant slices
 * start_bead and end_bead point to the new beads after the update
 * old_matrix and new_matrix point to the diffusion matrices themselves, new_matrix will contain the updated diffusion matrix
 */
template <class inter> void diffusion_toolbox<inter>::change_matrix(std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end, double scale, int start_particle, int end_particle, Bead* start_bead, Bead* end_bead, Matrix* old_matrix, Matrix* new_matrix )
{
	
	// Obtain the old diffusion matrix
	(*new_matrix).copy(old_matrix);

	
	// Case 1: a bead on the start slice has been changed:
	if( start_particle >= 0 )
	{
		for(int iX=0;iX<(*config).params.N;iX++) // re-calculate all matrix elements in the affected row
		{
			int end_id = (*blist_end)[iX]; // obtain the bead-id of particle iX on the end slice			
			
			if( (end_id != next_end) && ( iX != end_particle ) ) // do not calculate the diffusion element with a missing bead or a changed bead on the end slice
			{
				double element = my_interaction.rho( start_bead, &(*config).beads[end_id], scale);
				(*new_matrix).set_value(iX, start_particle, element); // update the new matrix
			}
		}
	}
	
	
	
	// Case 2: a bead on the end slice has been changed:
	if( end_particle >= 0 )
	{
		for(int iY=0;iY<(*config).params.N;iY++) // re-calculate all matrix elements in the affected columns
		{
			int start_id = (*blist_start)[iY]; // obtain the bead-id of particle iY on the start slice

			// Check, whether:
			// a) the start_bead exists
			// b) the start bead is the head of the current config, in which case there is no diffusion link to be calculated
			// c) the start bead has been modified
			if( ( ( start_id != next_start ) && ( start_id != (*config).head_id ) ) && ( iY != start_particle ) )
			{
				double element = my_interaction.rho( end_bead, &(*config).beads[start_id], scale );
				(*new_matrix).set_value(end_particle, iY, element);
			}
		}
	}
	
	
	// Case 3: Both on start and end slice a bead has been changed -> re-calculate this particular diffusion element:
	if( ( end_particle >= 0 ) && ( start_particle >= 0 ) )
	{
		double element = my_interaction.rho( start_bead, end_bead, scale );
		(*new_matrix).set_value(end_particle, start_particle, element);
	}
	
	
	
	
	
	
	
	
	
}






















template <class inter> void diffusion_toolbox<inter>::get_matrix(Matrix* m, std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end, double scale)
{
	int N = (*config).params.N;


	for(int iX=0;iX<N;iX++) // loop over all end beads
	{
		for(int iY=0;iY<N;iY++) // loop over all start beads
		{
// 			int start_id = (*config).beadlist[time][iY];
// 			int end_id = (*config).beadlist_A[end_time][iX];
			int start_id = (*blist_start)[iY];
			int end_id = (*blist_end)[iX];
			
			// default is the rho_fun between the two beads
// 			double element = rho_fun(&beads[start_id], &beads[end_id], &params, scale);
			double element = my_interaction.rho( &(*config).beads[start_id], &(*config).beads[end_id], scale );

			// if end_id does not exist, the column is filled with zero
			if(end_id == next_end) element = 0;
			
			// if start_id does not exist or is head of worm, the row is filled with unity
			if( ( start_id == (*config).head_id ) || ( start_id == next_start ) ) element = 1.0;

			(*m).set_value(iX, iY, element);
			
		}
	}

}






template <class inter> void diffusion_toolbox<inter>::ptr_get_matrix( worm_configuration* ptr_config, Matrix* m, std::vector<int>* blist_start, std::vector<int>* blist_end, int next_start, int next_end, double scale)
{
	int N = (*ptr_config).params.N;


	for(int iX=0;iX<N;iX++) // loop over all end beads
	{
		for(int iY=0;iY<N;iY++) // loop over all start beads
		{
// 			int start_id = (*config).beadlist[time][iY];
// 			int end_id = (*config).beadlist_A[end_time][iX];
			int start_id = (*blist_start)[iY];
			int end_id = (*blist_end)[iX];
			
			// default is the rho_fun between the two beads
// 			double element = rho_fun(&beads[start_id], &beads[end_id], &params, scale);
			double element = my_interaction.rho( &(*ptr_config).beads[start_id], &(*ptr_config).beads[end_id], scale );

			// if end_id does not exist, the column is filled with zero
			if(end_id == next_end) element = 0;
			
			// if start_id does not exist or is head of worm, the row is filled with unity
			if( ( start_id == (*ptr_config).head_id ) || ( start_id == next_start ) ) element = 1.0;

			(*m).set_value(iX, iY, element);
			
		}
	}

}

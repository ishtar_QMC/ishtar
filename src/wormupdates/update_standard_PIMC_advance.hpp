/*
 *  Contains the standard PIMC update Advance (G-sector)
 */



template <class inter> class update_standard_PIMC_advance
{
public:
	
	void init( bool new_debug, config_ensemble *new_ensemble );
	int execute( int m_max ); // execute the update, returns acceptance
	
	
private:
	
	// Variables of the update :
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
};



// init
template <class inter> void update_standard_PIMC_advance<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update ADVANCE (ensemble)\n";
	
	ensemble = new_ensemble;

	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );

	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_advance<inter>::execute( int m_max )
{
	
	// obtain the particle-species that is in the G-sector:
	int my_species = ensemble->G_species;
	
	// obtain a pointer to this config:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// initialize the interaction-toolbox to this config:
	inter_tools.init( config );
	
	
	// Choose the number of new beads between the old head and the new head, m:
 	int m = 1 + rdm()*m_max;

	if( debug )
	{
		std::cout << "################### update_standard_PIMC_advance, m (ensemble): " << m << "\n";
	}
	
	// Generate a list of new beads:
	std::vector<Bead> new_beads( 2+m, config->beads[ config->head_id ] );
	
	// Adjust the times of the new beads
	for(int i=0;i<1+m;i++)
	{
		int time = 1 + new_beads[ i ].get_time();
		if( time == config->params.n_bead )
		{
			time = 0;
		}
		new_beads[ 1+i ].set_time( time );
		new_beads[ 1+i ].set_real_time( time*config->params.epsilon );
	} // end loop i to 1+m
	
	// Sample the position of the new head:
	double prob_head = sampling_tools.sample_new_head( &new_beads[0], &new_beads[ 1+m ] );
	
	// Connect the old and new head:
	double prob_connect;
	std::vector<double> probs_connect( m, 0.0 ); // vector for the sampling probs for every new bead between old and new head
	
	if( config->params.system_type < config->params.n_traps ) // Connect without PBC
	{
		prob_connect = sampling_tools.trap_connect( &new_beads, m, probs_connect );
	}
	else // Connect with PBC
	{
		prob_connect = sampling_tools.PBC_connect( &new_beads, m, probs_connect );
	}
	
	
	if( debug )
	{
		std::cout << "prob_head: " << prob_head << "\t prob_connect: " << prob_connect << "\n";
	}
	
	
	// Obtain the changes in the m+1 diffusion_elements and m+2 interactions:
	double delta_u = 0.0; // OLD - NEW
	double diffusion_product = 1.0;
	
	// Consider the diffusion element of the old head separately
	double head_diffusion_element = my_interaction.rho( &new_beads[0], &new_beads[1], 1.0 );
	new_beads[ 0 ].set_diffusion_element( head_diffusion_element );
	diffusion_product *= head_diffusion_element;
	
	double diffusion_sampling_ratio = head_diffusion_element;
	
	
	
	// obtain interaction due to the same particle species (and ext_pot)
	double old_head_inter = inter_tools.old_interaction( &(*config).beadlist[ new_beads[0].get_time() ], config->head_id, -1 );
	double new_head_inter = inter_tools.new_interaction( &(*config).beadlist[ new_beads[1+m].get_time() ], &new_beads[1+m], -1 );
	
	// obtain interaction due to all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != my_species )
		{
			old_head_inter += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[0] );
			new_head_inter += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+m ] );
		}
	}
	
	// Old and new head only contribute half:
	delta_u -= 0.50 * ( old_head_inter + new_head_inter );
	
	// Special case where head_slice == tail_slice after the update, here head and tail do not interact!
	if( new_beads[ 1+m ].get_time() == config->beads[ config->tail_id ].get_time() )
	{
		double head_tail_interaction = my_interaction.pair_interaction( &(*config).beads[ config->tail_id ], &new_beads[1+m] );
		delta_u += 0.250 * head_tail_interaction;
		if( debug )
		{
			std::cout << "SPECIAL CASE, tail_slice: " << config->beads[ config->tail_id ].get_time() << "\t new_head_slice: " << new_beads[1+m].get_time() << "\n";
			std::cout << "head_tail_interaction: " << head_tail_interaction << "\n";
		}
	}
	
	
	// Special case where head_slice == tail_slice before the update:
	if( config->beads[ config->tail_id ].get_time() == config->beads[ config->head_id ].get_time() )
	{
		double head_tail_interaction = my_interaction.pair_interaction( &(*config).beads[ config->tail_id ], &(*config).beads[ config->head_id ] );
		delta_u -= 0.250 * head_tail_interaction;
	}
	

	// Loop over the m new intermediate beads:
	for(int i=0;i<m;i++)
	{
		// obtain the new interaction in the system:
		
		// interaction due to same species (and ext_pot):
		delta_u -= inter_tools.new_interaction( &(*config).beadlist[ new_beads[ 1+i ].get_time() ], &new_beads[ 1+i ], -1 );
		
		// interaction due to all other species:
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			if( iSpecies != my_species )
			{
				delta_u -= inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+i ] );
			}
		}
		
		// obtain the diffusion elements:
		double i_diffusion_element = my_interaction.rho( &new_beads[1+i], &new_beads[2+i], 1.0 );
		new_beads[1+i].set_diffusion_element( i_diffusion_element );
		
		// update the product of diffusion elements (not safe of overflow, for debug purposes only!)
		diffusion_product *= i_diffusion_element;
		
		// update the ratio of diffusion elements and sampling probabilities, bead-by-bead:
		diffusion_sampling_ratio *= ( i_diffusion_element / probs_connect[i] );
		
	} // end loop i to m
	
	
	
	// Calculate the acceptance ratio, draw a random number to decide acceptance:
	double choice = rdm();
	double delta_exponent = (1.0+m) * config->params.mu * config->params.epsilon;
	double ratio = exp( delta_exponent ) * exp( config->params.epsilon * delta_u );

	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{ 
		ratio = ratio * diffusion_sampling_ratio / prob_head;
	}
	
	
	
	
	// A few debug options, prevent overflow, etc:
	if( debug )
	{
	
		double ratio2 = exp( delta_exponent ) * diffusion_product * exp( config->params.epsilon * delta_u ) / ( prob_connect * prob_head );

		if( isnana( prob_head ) || isnana( diffusion_sampling_ratio ) )
		{
			std::cout << "Error, std_PIMC advance, diffusion_sampling_ratio: " << diffusion_sampling_ratio << "\t prob_head: " << prob_head << "\n";
			int cin;
			std::cin >> cin;
		}
		
		if( fabs( ratio/ratio2-1.0) > 1e-13 )
		{
			std::cout << "Error in std_PIMC ADVANCE, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
		
	}
	
	
	// Artificial potential to favor a given target N:
	// -> To be used carefully!
	if( config->params.N_potential_control )
	{
		double old_N_diff =  config->params.N - config->my_exp / ( config->params.mu * config->params.beta );
		double new_N_diff = config->params.N - ( config->my_exp + delta_exponent ) / ( config->params.mu * config->params.beta );
		double W_N_scale = -0.50 / ( config->params.sigma_target * config->params.sigma_target );
		double W_N_new = exp( W_N_scale * new_N_diff * new_N_diff );
		double W_N_old = exp( W_N_scale * old_N_diff * old_N_diff );

		if(debug) std::cout << "W_N_new = " << W_N_new << "\t W_N_old = " << W_N_old << "\t W_N_scale = " << W_N_scale << "\n";
		ratio = ratio * W_N_new / W_N_old;
	}

	
	// Artificial potential to prevent head and tail from running apart:
	// -> To be used carefully!
	if( config->params.head_tail_potential )
	{
		double old_diff_sq = 0.0;
		double new_diff_sq = 0.0;
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			old_diff_sq += pow( config->beads[ config->head_id ].get_coord( iDim ) - config->beads[ config->tail_id ].get_coord( iDim ), 2 );
			new_diff_sq += pow( config->beads[ config->tail_id ].get_coord( iDim ) - new_beads[ m+1 ].get_coord( iDim ), 2 );
		
			ratio *= exp( ( old_diff_sq - new_diff_sq ) / ( config->params.beta * config->params.head_tail_potential_eta ) );
		}
	}
	
	

	if(debug) // verbose:
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t delta_exponent: " << delta_exponent << "\t diffusion_product: " << diffusion_product << "\n";
		std::cout << "head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\t head_time: " << config->beads[ config->head_id ].get_time() << "\t tail_time: " << config->beads[ config->tail_id ].get_time() << "\n";
		std::cout << "new_head_time: " << new_beads[ 1+m ].get_time() << "\n";
	}
	
	
	
	
	
	// Implementation of the artificial potential on pair-exchanges:
	// -> To be used carefully!
	
	// Check, if have generated a new pair-exchange by advancing head past the tail:
		
	int old_difference = config->beads[ config->tail_id ].get_time() - config->beads[ config->head_id ].get_time();
	int new_difference = config->beads[ config->tail_id ].get_time() - new_beads[ 1+m ].get_time();
	
	int new_Npp = config->Npp;
		
	if( old_difference < 0 ) old_difference += config->params.n_bead;
	if( new_difference < 0 ) new_difference += config->params.n_bead;
	
	if( ( new_difference == 0 ) || ( ( new_difference > old_difference ) && ( old_difference > 0 ) ) )
	{
		new_Npp += 1; // update the number of pair-exchanges within the present permutation
	}
	
	
	
	
	// Modify the acceptance ratio via the potential on pair-exchanges:
	if( config->params.pp_control )
	{
		
		double inv_old_pp_weight = exp( -config->params.pp_delta * ( config->params.pp_kappa - config->Npp ) ) + 1.0;
		double inv_new_pp_weight = exp( -config->params.pp_delta * ( config->params.pp_kappa - new_Npp ) ) + 1.0;
		
		ratio = ratio * inv_old_pp_weight / inv_new_pp_weight;
		
		if( debug ) // verbose:
		{
			std::cout << "Additional change in acceptance ratio due to pp_control...\n";
			std::cout << "ratio: " << ratio << "\t old_pp_weight: " << 1.0/inv_old_pp_weight << "\t new_pp_weight: " << 1.0/inv_new_pp_weight << "\t Npp: " << config->Npp << "\t new_Npp: " << new_Npp << "\n";
		}
	}
	
	
	
	if( choice <= ratio ) // The update is accepted!
	{

		config->Npp = new_Npp; // update the number of pair-exchanges
		
		// Check, if the fermionic sign is flipped, update the sign:
		if( ( new_difference == 0 ) || ( ( new_difference > old_difference ) && ( old_difference > 0 ) ) )
		{
			if( config->params.canonical_PIMC ) return 0; // in this case, we would introduce an additional particle, which is not possible in the canonical ensemble
			config->my_sign *= -1.0;
		}
		
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product *= diffusion_product;
		
		ensemble->total_energy -= delta_u; // update the total energy 
		ensemble->total_exponent += delta_exponent; // update the total exponent
		
		config->my_exp += delta_exponent; // update the exponent of iSpecies

		// copy the new 'old_head' (new diffusion element)
		config->beads[ config->head_id ].copy( &new_beads[0] );
		
		// add the m+1 new beads to the structure
		int old_id = config->head_id;
		for(int i=0;i<m;i++)
		{
			// create a new bead
			int new_id = config->get_new_bead();
			
			config->beads[ old_id ].set_next_id( new_id );
			config->beads[ new_id ].copy( &new_beads[ 1+i ] );
			config->beads[ new_id ].set_prev_id( old_id );
			
			config->beadlist[ new_beads[ i+1 ].get_time() ].push_back( new_id );
			
			old_id = new_id;
		}

		// Add the new head:
		config->head_id = config->get_new_bead();
		config->beads[ config->head_id ].copy( &new_beads[ 1+m ] );
		config->beadlist[ new_beads[ 1+m ].get_time() ].push_back( config->head_id );
		
		// Connect the new head:
		config->beads[ config->head_id ].set_prev_id( old_id );
		config->beads[ old_id ].set_next_id( config->head_id );
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			config->standard_PIMC_print_paths( "advance_paths.dat" );
			config->standard_PIMC_print_beadlist( "advance_beadlist.dat" );
		}
		
		return 1;
	}
	else // The update is rejected!
	{
		return 0;
	}

}





























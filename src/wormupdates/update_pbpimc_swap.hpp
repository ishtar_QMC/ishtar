/*
 * Contains the class update_pbpimc_swap
 *  ### Update has been debugged and benchmarked. Yet, use in PB-PIMC simulations is NOT recommended!
 *  ### Worm algorithm is unnecessarily complicated. Use Deform update, instead!
 *  ### No implementation of bead-by-bead for sampling probs, etc. However, overflow is checked and does not happen in practice!
 */
 


template <class inter> class update_pbpimc_swap
{
public:
	
	void init( bool new_debug, config_ensemble* ensemble ); // initialize
	int execute( bool debug ); // execute the update
	
private:
	
	// Variables of the update:
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	bool debug; // Execute Update in Debug mode?
	
	
	// Constant structures to prevent dynamic memory allocation:
	
	std::vector< selection_toolbox<inter> >selection_tools;
	std::vector< sampling_toolbox<inter> >sampling_tools;
	std::vector< force_toolbox<inter> >force_tools;
	std::vector< interaction_toolbox<inter> >inter_tools;
	std::vector< diffusion_toolbox<inter> >diffusion_tools;
	
	std::vector< inter > my_interaction;

	std::vector< change_config >update;
	
	std::vector<std::vector <std::vector <double> > > const_store_the_force;
	std::vector<std::vector <std::vector <std::vector <double> > > > const_store_the_species;
	
	std::vector<Matrix> const_new_matrix_list;
	std::vector<double> const_new_determinant_list;
	std::vector<double> const_new_sign_list;
};






// init
template <class inter> void update_pbpimc_swap<inter>::init(bool new_debug, config_ensemble* new_ensemble )
{
	
	std::cout << "Universal(PB-PIMC)_swap_init\n";
	
	
	ensemble = new_ensemble;
	debug = new_debug;
	
	int n_species = ensemble->n_species;
	

	for(int iSpecies=0;iSpecies<n_species;iSpecies++) // initialize a toolbox for each species:
	{
		worm_configuration* cfg = &(*new_ensemble).species_config[ iSpecies ];

		// Update the config pointer from the selection_toolbox:
		selection_toolbox< inter > tmp_selection;
		tmp_selection.init( cfg );
		selection_tools.push_back( tmp_selection );
		

		// Update the parameters pointer from the sampling_toolbox:
		sampling_toolbox< inter > tmp_sampling;
		tmp_sampling.init( &(*cfg).params );
		sampling_tools.push_back( tmp_sampling );
		

		// Update the config pointer from the inter_tools:
		interaction_toolbox< inter > tmp_interaction;
		tmp_interaction.init( cfg );
		inter_tools.push_back( tmp_interaction );
		

		// Update the config pointer from the change_config:
		change_config tmp_update;
		tmp_update.init( cfg );
		update.push_back( tmp_update );
		

		// Update the config pointer from the diffusion_toolbox:
		diffusion_toolbox< inter > tmp_diffusion;
		tmp_diffusion.init( cfg );
		diffusion_tools.push_back( tmp_diffusion );
		

		// Update the parameters pointer from the interaction class:
		inter tmp_inter;
		tmp_inter.init( &(*cfg).params );
		my_interaction.push_back( tmp_inter );
		

		// Update the config pointer from the force_toolbox:
		force_toolbox< inter > tmp_force;
		tmp_force.init( cfg );
		force_tools.push_back( tmp_force );
	
	} // end loop iSpecies
	
	
	
	// Create a constant store_the_force structure with 3*P slots for N particles and dim dimensions:
	std::vector<double>dims( ensemble->params.dim, 0.0 );
	
	std::vector<std::vector <double> >slice_force( (*ensemble).N_tot, dims );
	const_store_the_force.assign( ensemble->params.n_bead*3, slice_force );
	
	
	// Create constant structures for the diffusion parts
	const_new_matrix_list.assign( ensemble->params.n_bead*3, (*ensemble).species_config[0].matrix_list[0] );
	const_new_determinant_list.assign( ensemble->params.n_bead*3, 1.0 );
	const_new_sign_list.assign( ensemble->params.n_bead*3, 1.0 );
	

	std::vector<std::vector<std::vector <double> > > slice_part;
	for(int iSpecies=0;iSpecies<ensemble->n_species;iSpecies++)
	{
		std::vector<std::vector <double> > species_part( ensemble->species_config[iSpecies].params.N, dims );
		slice_part.push_back( species_part );
	}
		
	const_store_the_species.assign( ensemble->params.n_bead*3, slice_part );
	
}




// Execute the Monte Carlo update:
template <class inter> int update_pbpimc_swap<inter>::execute( bool debug )
{
	int my_species = ensemble->G_species; // obtain the particle species with head and tail
	
	// obtain pointer to the G-config:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// obtain the propagator time step:
	double epsilon = config->params.epsilon;
	
	
	// obtain the time slices of head and tail:
	int head_time = config->beads[ config->head_id ].get_time();
	int tail_time = config->beads[ config->tail_id ].get_time();
	
	
	if( debug ) std::cout << "********PB-PIMC_SWAP, species: " << my_species << "\t head_time: " << head_time << "\t tail_time: " << tail_time << "\n";
	
	
	// Determine the number of missing beads between head and tail, m:
	int m = config->missing_links() - 1; // there is one more link than beads missing

	// obtain the kind of slice of the target bead, which is located on the slice of the tail:
	int target_kind = config->tail_kind;

	// obtain the time slice of the target bead:
	int target_time = config->beads[ config->tail_id ].get_time();
	
	
	// obtain general information about the head:
	int head_number = config->beads[ config->head_id ].get_number();
	std::vector<int> *head_beadlist;
	int head_next;

	double head_real_time = config->beads[ config->head_id ].get_real_time();
	double target_real_time = config->beads[ config->tail_id ].get_real_time();
	
	double delta_tau = target_real_time - head_real_time;
	if( delta_tau < 0.0 ) delta_tau += config->params.beta;
	
	double head_pot_fac = config->params.v1; // pre-factor of the potential energy of head-slice

	if( config->head_kind == 1 )
	{ // head located on ancilla slice A
		head_beadlist = &(*config).beadlist_A[head_time];
		head_pot_fac = config->params.v2;
		head_next = config->next_id_list_A[head_time];
	}
	else if( config->head_kind == 2 )
	{ 
		// head located on ancilla slice B
		head_beadlist = &(*config).beadlist_B[head_time];
		head_next = config->next_id_list_B[head_time];
	}
	else
	{ // head located on main slice
		head_beadlist = &(*config).beadlist[head_time];
		head_next = config->next_id_list[head_time];
	}
	

	// ######################################################################
	// ###### Select a target bead ##########################################
	// ######################################################################
	
	// temporary lists with the ids to be changed
	std::vector<int>number_list;
	std::vector<int>id_list;
	std::vector<Bead>new_beads;

	double forward_selection_prob;
	int selected_number; // particle id number of target bead
	
	// Select the target id:
	int selected_id = selection_tools[ my_species ].select_target_bead_PBC( &(*config).beads[ config->head_id ], &forward_selection_prob, &selected_number );
	
	
	// Check if this connection has a vanishing diffusion matrix element
	double target_rho = my_interaction[ my_species ].rho( &(*config).beads[ config->head_id ], &(*config).beads[ selected_id ], delta_tau/config->params.epsilon );
	if( target_rho < 1e-300 )
	{
		// vebose (even without "debug" option, fringe scenario!):
		std::cout << "forward_selection_prob: " << forward_selection_prob << "\t selected_id: " << selected_id << "\t delta_tau: " << delta_tau << "\n";
 		std::cout << "~~~~~~~~Vanishing partner element in the swap update, rho = " << target_rho << "\t distance: " << my_interaction[ my_species ].distance( &(*config).beads[ config->head_id ], &(*config).beads[ selected_id ] ) << "\n";
		return 0; // In this case, the update is simply rejected (detailed balance!)
	}
	
	if( debug ) std::cout << "target_selection_forward: " << forward_selection_prob << "\n";
	
	// Add the information about the end point of the artificial trajectory to the temporary lists:
	id_list.push_back(selected_id);
	number_list.push_back(selected_number);
	new_beads.push_back( config->beads[selected_id] );
	
	
	// ######################################################################
	// ###### Select m+1 beads backwards ####################################
	// ######################################################################
	// -> the last one will be the new head after the update
	
	int time = target_time; // initialize the time counter
	int cnt = target_kind; // initialize the slice kind counter
	
	for(int i=0;i<(m+1);i++)
	{
		int select_next; // id of the missing bead on the current slice
		std::vector<int>* select_blist; // pointer to the beadlist of the current slice
		double scale; // imaginary time scaling factor 
		
		Matrix* mp;

		if( cnt == 2 )
		{ // transition from ancilla slice B to A
			scale = config->params.t1;
			select_next = config->next_id_list_A[time];
			select_blist = &(*config).beadlist_A[time];
			
			mp = &(*config).matrix_list_A[time];
		}
		else if( cnt == 1 )
		{ // transition from ancilla slice A to main slice
			scale = config->params.t1;
			select_next = config->next_id_list[time];
			select_blist = &(*config).beadlist[time];
			
			mp = &(*config).matrix_list[time];
		}
		else
		{ // transition from main slice to ancilla slice B
			time--; // update the propagator number when you select a bead on kind B
			if(time<0) time = time + config->params.n_bead; // periodicity in imag. time
			scale = 2.0*config->params.t0;
			select_next = config->next_id_list_B[time];
			select_blist = &(*config).beadlist_B[time];
			
			mp = &(*config).matrix_list_B[time];
		}
		
		
		// update the slice kind counter
		cnt--;
		if(cnt == 0) cnt = 3;
		
		double p_i;
		int select_number; // selected particle id number
		
		// Select a random id:
		int select_id = selection_tools[ my_species ].PBC_select_bead_from_blist( &(*config).beads[id_list[i]], select_blist, select_next, scale, &p_i, &select_number, false );
		
		// Update the temporary lists of the artificial trajectory:
		number_list.push_back(select_number);
		id_list.push_back(select_id);
		new_beads.push_back( config->beads[select_id] );
		
		// Update the product of all normalizations with the old coordinates:
		forward_selection_prob *= p_i;
	}

	
	// obtain id and particle id of the new head after the update:
 	int new_head_id = id_list[ m+1 ];
	int new_head_number = number_list[ m+1 ];
	
	
	// Apply reverse order to the artificial trajectory
	std::reverse( id_list.begin(), id_list.end() );
	std::reverse( number_list.begin(), number_list.end() );
	std::reverse( new_beads.begin(), new_beads.end() );
	

	// ######################################################################
	// ###### Connect head with target bead #################################
	// ######################################################################
	
	double forward_sampling_prob, backward_sampling_prob;
	
	if( ensemble->params.system_type < ensemble->params.n_traps ) // sampling/ prob for a trap
	{
		backward_sampling_prob = sampling_tools[ my_species ].trap_backward_connect( &new_beads, m ); // reverse sampling prob
		
		// Starting point for the reconnect is the old head, not the new one!
		new_beads[0].copy( &(*config).beads[ config->head_id ] );
		
		forward_sampling_prob = sampling_tools[ my_species ].trap_connect( &new_beads, m ); // actual sampling prob
	}
	else // sampling/ prob for PBC
	{
		backward_sampling_prob = sampling_tools[ my_species ].PBC_backward_connect( &new_beads, m );
		
		// Starting point for the reconnect is the old head, not the new one!
		new_beads[0].copy( &(*config).beads[ config->head_id ] );
		
		forward_sampling_prob = sampling_tools[ my_species ].PBC_connect( &new_beads, m );
	}

	
	// ######################################################################
	// ###### Calculate change in energy and forces #########################
	// ######################################################################
	
	double delta_f_sq = 0.0;
	double delta_u = 0.0;

	// Calculate the change in the energy due to the change of the head_id:
	delta_u -= 0.50 * head_pot_fac * inter_tools[ my_species ].ensemble_old_interaction( ensemble, config->head_id, my_species, config->head_kind, head_beadlist, head_next ); // old head
	delta_u += 0.50 * head_pot_fac * inter_tools[ my_species ].ensemble_old_interaction( ensemble, id_list[0], my_species, config->head_kind, head_beadlist, head_next ); // new head

	cnt = config->head_kind; // initialize the slice kind counter
	
	for(int i=0;i<m;i++) // loop over all m slices where beads have been re-sampled:
	{
		if(cnt == 3) cnt = 0;
		cnt++;

		int dtime = new_beads[1+i].get_time();

		if( cnt == 1 )
		{ // ancilla slice A
			double ftmp = force_tools[ my_species ].ensemble_handle_change( ensemble, my_species, 1, &(*config).beadlist_A, id_list[i+1], &new_beads[i+1], &const_store_the_force[i], &(*config).next_id_list_A, &const_store_the_species[i] );
			delta_f_sq += (1.0 - 2.0*config->params.a1)*ftmp;

			delta_u += config->params.v2 * inter_tools[ my_species ].ensemble_change_interaction( ensemble, my_species, 1, &(*config).beadlist_A[new_beads[i+1].get_time()], &new_beads[i+1], id_list[1+i], (*config).next_id_list_A[new_beads[i+1].get_time()] );
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			double ftmp = force_tools[ my_species ].ensemble_handle_change( ensemble, my_species, 2, &(*config).beadlist_B, id_list[i+1], &new_beads[i+1], &const_store_the_force[i], &(*config).next_id_list_B, &const_store_the_species[i] );
			delta_f_sq += config->params.a1*ftmp;

			delta_u += config->params.v1 * inter_tools[ my_species ].ensemble_change_interaction( ensemble, my_species, 2, &(*config).beadlist_B[new_beads[i+1].get_time()], &new_beads[i+1], id_list[1+i], (*config).next_id_list_B[new_beads[i+1].get_time()] );
		}
		else
		{ // main slice
			double ftmp = force_tools[ my_species ].ensemble_handle_change( ensemble, my_species, 3, &(*config).beadlist, id_list[i+1], &new_beads[i+1], &const_store_the_force[i], &(*config).next_id_list, &const_store_the_species[i] );
			delta_f_sq += config->params.a1*ftmp;

			delta_u += config->params.v1 * inter_tools[ my_species ].ensemble_change_interaction( ensemble, my_species, 1, &(*config).beadlist[new_beads[i+1].get_time()], &new_beads[i+1], id_list[1+i], (*config).next_id_list[new_beads[i+1].get_time()] );
		}
	} // end of the loop over all changed slices

	
	// ######################################################################
	// ###### Calculate the normalizations with new coords ##################
	// ######################################################################
	
	// Calculate the big normalization for the reverse move:
	double debug_back = selection_tools[ my_species ].reverse_target_PBC( selected_id, &(*config).beads[ id_list[0] ], delta_tau/config->params.epsilon, debug );
	double backward_selection_prob = debug_back;
	
	if( debug ) std::cout << "target_selection_backward: " << backward_selection_prob << "\n";
	if( debug ) std::cout << "debug_back: " << debug_back << "\n";
	
	cnt = config->head_kind; // initialize the slice kind counter
	
	for(int i=0;i<(m+1);i++) // loop over all affected slices
	{
		int dtime = new_beads[i].get_time(); // time of the begin slice of the transition
		int next; // id of the missing bead
		std::vector<int>* reverse_blist; // pointer to the beadlist of begin slice
		double scale; // imaginary time scaling factor
		
		if( cnt == 1 )
		{ // transition from ancilla slice A to B
			scale = config->params.t1;
			next = config->next_id_list_A[dtime];
			reverse_blist = &(*config).beadlist_A[dtime];
		}
		else if( cnt == 2 )
		{ // transition from ancilla slice B to main slice 
			scale = 2.0*config->params.t0;
			next = config->next_id_list_B[dtime];
			reverse_blist = &(*config).beadlist_B[dtime];
		}
		else
		{ // transition from main slice to ancilla slice A
			cnt = 0;
			scale = config->params.t1;
			next = config->next_id_list[dtime];
			reverse_blist = &(*config).beadlist[dtime];
		}

		// probability to select the affected beads in the reverse move (detailed balance)
		double my_prob = selection_tools[ my_species ].get_selection_prob_reverse( reverse_blist, id_list[i], next, &new_beads[i+1], &new_beads[i], scale, false );
		backward_selection_prob *= my_prob;

		cnt++; // update the slice-kind counter
	}
	
	
	// ######################################################################
	// ###### Calculate the new diffusion matrices, etc. ####################
	// ######################################################################
	
	double new_sign = config->my_sign; // initialize the sign of the new config
	double determinant_ratio = 1.0; // initialize determinant_ratio
	
	cnt = config->head_kind; // initialize the slice kind counter
	
	for(int i=0;i<(m+1);i++) // loop over all affected slices
	{
		int ktime = new_beads[i].get_time(); // time of diffusion matrix
		int ltime = new_beads[i+1].get_time(); // time of end slice of transition
		
		Matrix *mp; // pointer to the corresponding old diffusion matrix
		double kdeterminant, ksign; // values of old determinant and sign
		
		// Obtain pointers to beadlists from start and end of transition:
		std::vector<int> *blist_start, *blist_end;
		
		// Obtain ids of non-existing beads on both affected slices
		int next_start, next_end;
		
		double scale; // imaginary time scaling factor
		
		if( cnt == 1 )
		{ // starting from ancilla slice A
			mp = &(*config).matrix_list_A[ktime];
			blist_start = &(*config).beadlist_A[ktime];
			blist_end = &(*config).beadlist_B[ltime];
			
			next_start = config->next_id_list_A[ktime];
			next_end = config->next_id_list_B[ltime];
			
			scale = config->params.t1;
			
			kdeterminant = config->determinant_list_A[ktime];
			ksign = config->sign_list_A[ktime];
		}
		else if( cnt == 2 )
		{ // starting from ancilla slice B
			mp = &(*config).matrix_list_B[ktime];
			blist_start = &(*config).beadlist_B[ktime];
			blist_end = &(*config).beadlist[ltime];
			
			next_start = config->next_id_list_B[ktime];
			next_end = config->next_id_list[ltime];
			
			scale = 2.0 * config->params.t0;
			
			kdeterminant = config->determinant_list_B[ktime];
			ksign = config->sign_list_B[ktime];
		}
		else{ // starting from main slice
			cnt = 0;
			mp = &(*config).matrix_list[ktime];
			blist_start = &(*config).beadlist[ktime];
			blist_end = &(*config).beadlist_A[ltime];
			
			next_start = config->next_id_list[ktime];
			next_end = config->next_id_list_A[ltime];
			
			scale = config->params.t1;
			
			kdeterminant = config->determinant_list[ktime];
			ksign = config->sign_list[ktime];
		}
		
		cnt++; // update the slice-kind counter

		
		
		// Perform actual changes of the diffusion matrices:
		if(i == 0) // The first affected slice requires special attention, because the head bead is changed -> Set new head row to unity
		{
 			diffusion_tools[ my_species ].swap_matrix( blist_start, blist_end, next_start, next_end ,scale, head_number, new_head_number, number_list[1+i], &new_beads[1+i], mp, &const_new_matrix_list[i] );
		}
		else // Simply update matrix due to changes in two beads, i.e. a single row and a single column
		{
			diffusion_tools[ my_species ].change_matrix( blist_start, blist_end, next_start, next_end, scale,  number_list[i], number_list[i+1], &new_beads[i], &new_beads[i+1], mp, &const_new_matrix_list[i] );
		}

		
		// Calculate new determinant and sign:
		const_new_determinant_list[i] = const_new_matrix_list[i].determinant();
		const_new_sign_list[i] = 1.0;
		if( const_new_determinant_list[i] < 0.0)
		{
			const_new_determinant_list[i] = -1.0*const_new_determinant_list[i];
			const_new_sign_list[i] = -1.0;
		}
		
		// Update the total sign of the new config:
		new_sign *= ksign * const_new_sign_list[i];
		
		// Calculate the determinant_ratio:
		determinant_ratio *= const_new_determinant_list[i] / kdeterminant;
		
	} // end loop over all affected time slices
	

	// Calculate the ratio of energy and force_sq exponential functions:
	double new_exp = exp( epsilon* (delta_u + epsilon*epsilon*(*config).params.u0*delta_f_sq ) );
	
	// Calculate the acceptance ratio, draw random number for decision:
	double choice = rdm();
	double ratio = new_exp * determinant_ratio * backward_sampling_prob * backward_selection_prob / ( forward_sampling_prob * forward_selection_prob );
	
	if( debug ) // verbose:
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t new_exp: " << new_exp << "\n";
		std::cout << "determinant_ratio: " << determinant_ratio << "\t delta_f_sq: " << delta_f_sq << "\t delta_u: " << delta_u << "\n";
		std::cout << "backward_sampling_prob: " << backward_sampling_prob << "\t backward_selection_prob: " << backward_selection_prob << "\n";
		std::cout << "forward_sampling_prob: " << forward_sampling_prob << "\t forward_selection_prob: " << forward_selection_prob << "\n";
	}
	
	// Check for possible overflows (bead-by-bead atm not implemented!). Does not happen in practice!
	if( isnana(determinant_ratio) || isnana(backward_sampling_prob) || isnana(backward_selection_prob) || isnana(forward_selection_prob) || isnana(forward_sampling_prob) )
	{
		int cin;
		std::cout << "/////////ratio: " << ratio << "\t choice: " << choice << "\t new_exp: " << new_exp << "\n";
		std::cout << "determinant_ratio: " << determinant_ratio << "\t delta_f_sq: " << delta_f_sq << "\t delta_u: " << delta_u << "\n";
		std::cout << "backward_sampling_prob: " << backward_sampling_prob << "\t backward_selection_prob: " << backward_selection_prob << "\n";
		std::cout << "forward_sampling_prob: " << forward_sampling_prob << "\t forward_selection_prob: " << forward_selection_prob << "\n";
	
		std::cin >> cin;
	}
	
	
	// Accept the update?
	if( choice <= ratio ) // The update has been accepted:
	{
		// Updated force_sq, interaction, etc.:
		config->my_sign = new_sign;
		ensemble->total_force_sq = ensemble->total_force_sq - delta_f_sq;
		ensemble->total_energy = ensemble->total_energy - delta_u;
		

		// Update m beads and the corresponding forces:
		int cnt = config->head_kind; // initialize the slice kind counter
		for(int i=0;i<m;i++)
		{
			if(cnt==3) cnt = 0;
			cnt++;
			
			int ktime = new_beads[i+1].get_time();
			int update_id = id_list[1+i];
			

			// Update the forces of all beads on the current time slice:
			if(cnt == 1)
			{ // ancilla slice A
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 1, &(*config).beadlist_A[ktime], (*config).next_id_list_A[ktime], update_id, &const_store_the_force[i], &const_store_the_species[i] );
			}
			else if(cnt == 2)
			{ // ancilla slice B
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 2, &(*config).beadlist_B[ktime], (*config).next_id_list_B[ktime], update_id, &const_store_the_force[i], &const_store_the_species[i] );
			}
			else
			{ // main slice;
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 3, &(*config).beadlist[ktime], (*config).next_id_list[ktime], update_id, &const_store_the_force[i], &const_store_the_species[i] );
			}
			
			// Copy the new bead:
			config->beads[id_list[1+i]].copy(&new_beads[1+i]);
		}

		
		// Update the kinetic matrices/ determinants on (m+1) slices:
		cnt = config->head_kind; // initialize the slice kind counter
		for(int i=0;i<(m+1);i++)
		{
			int ktime = new_beads[i].get_time();
			
			if( cnt == 1 )
			{ // ancilla slice A
				config->matrix_list_A[ktime].copy(&const_new_matrix_list[i]);
				config->determinant_list_A[ktime] = const_new_determinant_list[i];
				config->sign_list_A[ktime] = const_new_sign_list[i];
			}
			else if( cnt == 2 )
			{ // ancilla slice B
				config->matrix_list_B[ktime].copy(&const_new_matrix_list[i]);
				config->determinant_list_B[ktime] = const_new_determinant_list[i];
				config->sign_list_B[ktime] = const_new_sign_list[i];
			}
			else
			{ // main slice
				config->matrix_list[ktime].copy(&const_new_matrix_list[i]);
				config->determinant_list[ktime] = const_new_determinant_list[i];
				config->sign_list[ktime] = const_new_sign_list[i];
				cnt = 0;
			}
			

			cnt++; // update the slice-kind counter
		} // end loop over all affected "links"
		
		// Update the head:
		config->head_id = new_head_id;
		
		return 1; 
	}
	else
	{
		return 0; // the update has been rejected
	}
}




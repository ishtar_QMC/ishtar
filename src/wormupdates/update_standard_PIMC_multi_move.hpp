/*
 *  Contains the standard PIMC update Move
 */


template <class inter> class update_standard_PIMC_multi_move
{
public:
	
	void init( bool new_debug, config_ensemble* new_config_ensemble );
	int execute( double delta_scale ); // execute the update
	
	void exchange();
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // pointer to the ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
	
	
	
	bool am_I_single( worm_configuration* config, int id ); // Am I a single particle ?
	double lambda; // thermal wavelength of the electrons
	double norm;
	
	
	
};



// init
template <class inter> void update_standard_PIMC_multi_move<inter>::init( bool new_debug, config_ensemble* new_config_ensemble )
{
	std::cout << "initialize the standard-PIMC update MOVE (ensemble)\n";
	
	ensemble = new_config_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );
	
	
	lambda = sqrt( 2.0*pi*ensemble->params.epsilon );
	norm = pow( lambda, ensemble->params.dim );
	
	

	std::cout << "initialization successful!\n";
}





template <class inter> void update_standard_PIMC_multi_move<inter>::exchange()
{
	if( ensemble->species_config.size() < 2 ) return;
	
	// Create a list with all possible single electrons of species 0,1:
	std::vector<int> id0,id1;
	
	worm_configuration* config0 = &(*ensemble).species_config[0];
	for(int i=0;i<config0->beadlist[0].size();i++)
	{
		int i_ID = (*config0).beadlist[0][i];
		if( am_I_single( config0, i_ID ) )
		{
			id0.push_back( i_ID );
		}
	}
	if( id0.size() < 1 ) return;
	
	
	worm_configuration* config1 = &(*ensemble).species_config[1];
	for(int i=0;i<config1->beadlist[0].size();i++)
	{
		int i_ID = (*config1).beadlist[0][i];
		if( am_I_single( config1, i_ID ) )
		{
			id1.push_back( i_ID );
		}
	}
	if( id1.size() < 1 ) return;
	
	
	
	
	// Select random element from list0,1
	int select0 = rdm()*id0.size();
	int select1 = rdm()*id1.size();
	
	
	
	int tmp_id0 = id0[ select0 ];
	int tmp_id1 = id1[ select1 ];

	
	for(int iSlice=0;iSlice<ensemble->params.n_bead;iSlice++)
	{
		Bead* bead0 = &(*config0).beads[ tmp_id0 ];
		Bead* bead1 = &(*config1).beads[ tmp_id1 ];
		
		for(int iDim=0;iDim<ensemble->params.dim;iDim++)
		{
			double tmp = bead0->get_coord(iDim);
			
			
			bead0->set_coord( iDim, bead1->get_coord(iDim) );
			bead1->set_coord( iDim, tmp );
			
			
		}
		
		double tmp_diffusion = bead0->get_diffusion_element();
		bead0->set_diffusion_element( bead1->get_diffusion_element() );
		bead1->set_diffusion_element( tmp_diffusion );
		
		
		
		tmp_id0 = bead0->get_next_id();
		tmp_id1 = bead1->get_next_id();
	}
	
	
	return;
}







// Returns true only if the particle in question is a single particle, not an exchange cycle!
template <class inter> bool update_standard_PIMC_multi_move<inter>::am_I_single( worm_configuration* config, int id )
{
	int my_id = id;
	if( my_id == config->head_id ) return false;
	
	for(int i=0;i<config->params.n_bead;i++)
	{
		my_id = (*config).beads[ my_id ].get_next_id();
		
		if( my_id == config->head_id ) return false;
	}
	
	if( my_id == id ) return true;
	return false;
}




// TBD TBD TBD There needs to be an r_list to calculate the reverse move probability.


// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_multi_move<inter>::execute( double delta_scale )
{
	exchange();
	
	
	
	if( debug ) std::cout << "****** std-PIMC (ensemble) update multi-MOVE \n"; 
	
	if( ensemble->species_config.size() <= 2 ) return 0; // No proton to be moved!
	

	int reference_species = 2; // reference species are always protons!
	
	// obtain a pointer to the worm_config to be updated:
	worm_configuration* config = &(*ensemble).species_config[ reference_species ];
	
	// obtain a random time slice;
	int reference_slice = rdm()*ensemble->params.n_bead;
	
	if( config->beadlist[ reference_slice ].size() == 0 ) return 0;
	
	// obtain a random bead of reference_species on the reference_slice
	int reference_id = rdm()*config->beadlist[ reference_slice ].size();
	reference_id = config->beadlist[ reference_slice ][ reference_id ]; // ;)
	
	if( debug ) std::cout << "reference_species: " << reference_species << "\t reference_slice: " << reference_slice << "\t reference_id: " << reference_id << "\n";
	
	int species_lim = 2; // Select the move partner from an electron-species, not a proton!

	
	// Create a list with the distances to all other beads on this slice;
	std::vector<double> prob_list;
 	std::vector<double> new_prob_list;
	std::vector<int> species_list;
	std::vector<int> id_list;
	
	
	// obtain all coordinates of the reference bead
	Bead* reference_bead = &(*config).beads[ reference_id ];
 	std::vector<double> reference_coords = reference_bead->get_all_coords();
	
	
	
	// Randomly select the displacement of the particle:
	std::vector<double>delta( config->params.dim, 0.0 );
	for(int iDim=0;iDim<config->params.dim;iDim++)
	{
		delta[ iDim ] = delta_scale * ( 2.0*rdm() - 1.0 ) * config->params.rs;
		if( debug ) std::cout << "iDim: " << iDim << "\t delta: " << delta[ iDim ] << "\n";
	}
	
	
	
	double normalization = 0.0;
	double new_normalization = 0.0;
	
	
	for(int iSpecies=0;iSpecies<species_lim;iSpecies++)
	{
		worm_configuration* iConfig = &(*ensemble).species_config[ iSpecies ];
		for(int i=0;i<iConfig->beadlist[ reference_slice ].size();i++)
		{
			
			int i_ID = (*iConfig).beadlist[ reference_slice ][ i ];
			if( am_I_single( iConfig, i_ID ) ) // only consider "single" beads
			{
				
				double ans = 0.0;
				double new_ans = 0.0;
				
				for(int iDim=0;iDim<ensemble->params.dim;iDim++)
				{
						double diff = reference_coords[ iDim ] - iConfig->beads[ i_ID ].get_coord( iDim );
						while( diff > 0.5*ensemble->params.length ) diff -= ensemble->params.length;
						while( diff < -0.5*ensemble->params.length ) diff += ensemble->params.length;
						
						double new_diff = reference_coords[ iDim ] + delta[ iDim] - iConfig->beads[ i_ID ].get_coord( iDim );
						while( new_diff > 0.5*ensemble->params.length ) new_diff -= ensemble->params.length;
						while( new_diff < -0.5*ensemble->params.length ) new_diff += ensemble->params.length;
						
						ans += diff*diff;
						new_ans += new_diff*new_diff;
						
				} // end loop iDim
					

				/*
				double P = exp(-pi*pi/(lambda*lambda)*ans);
				double new_P = exp(-pi*pi/(lambda*lambda)*new_ans);
				*/
				
				
				if( ans < 0.01*ensemble->params.length ) ans = 0.01*ensemble->params.length;
				double P = 1.0/ans;
				
				if( new_ans < 0.01*ensemble->params.length ) new_ans = 0.01*ensemble->params.length;
				double new_P = 1.0/new_ans;
				
				
				normalization += P;
				new_normalization += new_P;
				
				
				if( debug ) 
				{
					std::cout << "---------------------------------i: " << prob_list.size() << "\n";
					std::cout << "ans: " << ans << "\tnew_ans: " << new_ans << "\tlambda: " << lambda << "\n";
					std::cout << "P: " << P << "\tnew_P: " << new_P << "\tnormalization: " << normalization << "\tnew_normalization: " << new_normalization << "\n";
					std::cout << "iSpecies: " << iSpecies << "\ti_ID: " << i_ID << "\ti_time: " << iConfig->beads[ i_ID ].get_time() << "\n";
					
				}
				
				prob_list.push_back( P );
 				new_prob_list.push_back( new_P );
				species_list.push_back( iSpecies );
				id_list.push_back( i_ID );
			
				
			} // end "am_I_single" if clause			
			
		} // end loop i
		
	} // end loop iSpecies
	
	
	
	if( debug ) std::cout << "Found size=" << prob_list.size() << " potential move-partners\n";
	
	if( prob_list.size() == 0 ) return 0; // Nothing to do here. No other single particles in the system.
	
	
	
	// Randomly select a move-partner:
	
	double myChoice = rdm()*normalization;
	int select;
	
	double ans = 0.0;
	double P_forward, P_backward;
	for(int i=0;i<prob_list.size();i++)
	{
		
		select = i;
		if( ( myChoice >= ans ) && ( myChoice <= ans+prob_list[i] ) )
		{
			P_forward = prob_list[i]/normalization;
			
			// adapt the "new_normalization"; since both the reference bead and the selected bead have been moved!
			new_normalization = new_normalization - new_prob_list[i] + prob_list[i];
			P_backward = prob_list[i]/new_normalization;
			
			break;
		}
		
		ans = ans + prob_list[i];

	} // end loop i
	
	if( debug )
	{
		std::cout << "random_number: " << myChoice / normalization << "\n";
		std::cout << "myChoice: " << myChoice << "\tnormalization: " << normalization << "\tnew_normalization: " << new_normalization << "\tselect: " << select << "\n";
		std::cout << "P_forward: " << P_forward << "\tP_backward: " << P_backward << "\n";
		std::cout << "species: " << species_list[select] << "\tid: " << id_list[select] << "\n";
	}

	
	
	/* TBD needed below!
	std::vector<int> changed_species;
	changed_species.push_back( reference_species );
	changed_species.push_back( species_list[ select ] );
	
	std::vector<int> changed_ids;
	changed_ids.push_back( reference_id );
	changed_ids.push_back( id_list[ select ] );
	*/
	
	
	
	// Initialize the interaction toolbox with the selected particle species config:
	inter_tools.init( &(*ensemble).species_config[ reference_species ] );
	

	
	

	
	
	
	// Create P copies of all affected beads and dispace them by delta, simultaneously obtain the change in the interaction:
	double delta_u = 0.0; // OLD - NEW
	
	double delta_pair = 0.0;
	double delta_pair_derivative = 0.0;
	
	double delta_m_e_pair_derivative = 0.0;
	double delta_m_p_pair_derivative = 0.0;
	
	
	double delta_ext_pot_action = 0.0;
	double delta_ext_pot_action_derivative = 0.0;
	
	
	std::vector<Bead>new_beads_A, new_beads_B; // Containers for the new beads
	
	int tmp_id_A = reference_id;
	int tmp_id_B = id_list[select];
	
	worm_configuration* config_B = &(*ensemble).species_config[ species_list[ select ] ];
	std::vector<int> info_species{ reference_species, species_list[select] };
	
	
	
	
	
	
	
	
	
	
	// ##########################################################################################################################
	// ### Start::OpenMP parallelization part
	
	
	
	std::vector<double> OpenMP_delta_u( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_pair( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_pair_derivative( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_m_e_pair_derivative( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_m_p_pair_derivative( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action_derivative( config->params.n_bead, 0.0 );
	
	
	
	std::vector<int> Alist( config->params.n_bead, 0 );
	std::vector<int> Blist( config->params.n_bead, 0 );
	
	
	for(int i=0;i<config->params.n_bead;i++)
	{
		new_beads_A.push_back( config->beads[ tmp_id_A ] );
		new_beads_B.push_back( config_B->beads[ tmp_id_B ] );
		
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			new_beads_A[ i ].set_coord( iDim, my_interaction.adjust_coordinate( new_beads_A[ i ].get_coord( iDim ) + delta[ iDim ] ) );
			new_beads_B[ i ].set_coord( iDim, my_interaction.adjust_coordinate( new_beads_B[ i ].get_coord( iDim ) + delta[ iDim ] ) );
		}
		
		Alist[i] = tmp_id_A;
		Blist[i] = tmp_id_B;
		
		tmp_id_A = config->beads[ tmp_id_A ].get_next_id();
		tmp_id_B = config_B->beads[ tmp_id_B ].get_next_id();

	}
	
	
	

	
	
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=0;i<config->params.n_bead;i++)
		{
			
			
			// Obtain the total change in the potential energy (OLD-NEW):
			std::vector<int> info_id{ Alist[i], Blist[i] };
			std::vector<Bead> info_bead{ new_beads_A[i], new_beads_B[i] };
			
			double addend = inter_tools.multi_change_interaction( ensemble, info_species, info_id, &info_bead );
			// ### OpenMP:: delta_u += addend;
			
			OpenMP_delta_u[ i ] += addend;
			
			

			
		}
		
	}
	
	
	

	if( debug ) std::cout << "-------------------------multi-INTERMEDIATE-delta_u:" << delta_u << "\n";
	
	



	
	
	// Obtain the change in the pair action, and in the external potential action:
	
	
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=0;i<config->params.n_bead;i++)
		{
			int i_id_A_1 = new_beads_A[i].get_next_id();
			int i_id_A_0 = config->beads[ i_id_A_1 ].get_prev_id();
			
			int i_id_B_1 = new_beads_B[i].get_next_id();
			int i_id_B_0 = config_B->beads[ i_id_B_1 ].get_prev_id();
			
			int next_index = 1+i;
			if( i == config->params.n_bead-1 ) next_index = 0;
			
			
			

			
			
			std::vector<int> info_id_0{ i_id_A_0, i_id_B_0 };
			std::vector<int> info_id_1{ i_id_A_1, i_id_B_1 };
			
			std::vector<Bead> info_bead_0{ new_beads_A[i], new_beads_B[i] };
			std::vector<Bead> info_bead_1{ new_beads_A[next_index], new_beads_B[next_index] };
			
			std::vector<double> dA = pair_tools.multi_change_pair_action( ensemble, info_species, info_id_0, info_id_1, &info_bead_0, &info_bead_1 );
			
			// ### OpenMP:: delta_pair += dA[0];
			// ### OpenMP:: delta_pair_derivative += dA[1];
			
			// ### OpenMP:: delta_m_e_pair_derivative += dA[2];
			// ### OpenMP:: delta_m_p_pair_derivative += dA[3];
			
			
			OpenMP_delta_pair[ i ] += dA[0];
			OpenMP_delta_pair_derivative[ i ] += dA[1];
			
			OpenMP_delta_m_e_pair_derivative[ i ] += dA[2];
			OpenMP_delta_m_p_pair_derivative[ i ] += dA[3];
			
			

			
			std::vector<double> old_ext_pot_action_A, new_ext_pot_action_A;
			std::vector<double> old_ext_pot_action_B, new_ext_pot_action_B;
			
			
			old_ext_pot_action_A = my_interaction.ext_pot_action( &(*config).beads[ i_id_A_0 ], &(*config).beads[ i_id_A_1 ] );
			new_ext_pot_action_A = my_interaction.ext_pot_action( &new_beads_A[ i ], &new_beads_A[ next_index ] );
			
			// ### OpenMP:: delta_ext_pot_action += ( old_ext_pot_action_A[0] - new_ext_pot_action_A[0] );
			// ### OpenMP:: delta_ext_pot_action_derivative += ( old_ext_pot_action_A[1] - new_ext_pot_action_A[1] );
			
			OpenMP_delta_ext_pot_action[ i ] += ( old_ext_pot_action_A[0] - new_ext_pot_action_A[0] );
			OpenMP_delta_ext_pot_action_derivative[ i ] += ( old_ext_pot_action_A[1] - new_ext_pot_action_A[1] );
			
			
			
			old_ext_pot_action_B = my_interaction.ext_pot_action( &(*config_B).beads[ i_id_B_0 ], &(*config_B).beads[ i_id_B_1 ] );
			new_ext_pot_action_B = my_interaction.ext_pot_action( &new_beads_B[ i ], &new_beads_B[ next_index ] );
			
			// ### OpenMP:: delta_ext_pot_action += ( old_ext_pot_action_B[0] - new_ext_pot_action_B[0] );
			// ### OpenMP:: delta_ext_pot_action_derivative += ( old_ext_pot_action_B[1] - new_ext_pot_action_B[1] );
			
			OpenMP_delta_ext_pot_action[ i ] += ( old_ext_pot_action_B[0] - new_ext_pot_action_B[0] );
			OpenMP_delta_ext_pot_action_derivative[ i ] += ( old_ext_pot_action_B[1] - new_ext_pot_action_B[1] );
			
		}
	
	}
	
	
	
	
	// After OMP post-processing loop!
	for(int i=0;i<config->params.n_bead;i++)
	{		
		delta_u += OpenMP_delta_u[ i ];
		delta_pair += OpenMP_delta_pair[ i ];
		delta_pair_derivative += OpenMP_delta_pair_derivative[ i ];
		delta_m_e_pair_derivative += OpenMP_delta_m_e_pair_derivative[ i ];
		delta_m_p_pair_derivative += OpenMP_delta_m_p_pair_derivative[ i ];
		delta_ext_pot_action += OpenMP_delta_ext_pot_action[ i ];
		delta_ext_pot_action_derivative += OpenMP_delta_ext_pot_action_derivative[ i ];
	}
	

		
	// ### End::OpenMP parallelization part
	// ##########################################################################################################################

	
	
	
	
	
	
	
	// Obtain a random number to decide acceptance:
	double choice = rdm();
	
	// Calculate the acceptance prob:
	double ratio = exp( config->params.epsilon * delta_u ) * exp( config->params.epsilon * delta_pair ) * exp( config->params.epsilon * delta_ext_pot_action ) *P_backward/P_forward;
	
	// verbose:
	if( debug )
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t delta_u: " << delta_u << "\t delta_pair: " << delta_pair << "\n";
		std::cout << "delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_m_e_pair_derivative: " << delta_m_e_pair_derivative << "\t delta_m_p_pair_derivative: " << delta_m_p_pair_derivative << "\n";
		std::cout << "delta_ext_pot_action: " << delta_ext_pot_action << "\t delta_ext_pot_action_derivative: " << delta_ext_pot_action_derivative << "\n";
		std::cout << "P_backward: " << P_backward << "\t P_forward: " << P_forward << "\n";
	}
	
	
	
	
// 	choice = 0;
	if( choice <= ratio ) // The update has been accepted:
	{

		// book keeping of the total potential energy:
		ensemble->total_energy -= delta_u;
		
		// book keeping of the total pair action in the system:
		ensemble->total_pair_action -= delta_pair;
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		// book keeping of the ext_pot_action:
		ensemble->total_ext_pot_action -= delta_ext_pot_action;
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		ensemble->total_m_e_pair_derivative -= delta_m_e_pair_derivative;
		ensemble->total_m_p_pair_derivative -= delta_m_p_pair_derivative;
		
		
		// Replace the coords of the dispaced single partice:
		int tmp_id_A = reference_id;
		int tmp_id_B = id_list[ select ];
		for(int i=0;i<config->params.n_bead;i++)
		{
			for(int iDim=0;iDim<config->params.dim;iDim++)
			{
				config->beads[ tmp_id_A ].set_coord( iDim, new_beads_A[ i ].get_coord( iDim ) );
				config_B->beads[ tmp_id_B ].set_coord( iDim, new_beads_B[ i ].get_coord( iDim ) );
			}
			tmp_id_A = config->beads[ tmp_id_A ].get_next_id();
			tmp_id_B = config_B->beads[ tmp_id_B ].get_next_id();
		}
		
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			std::cout << "reference_species: " << reference_species << "\n";
			config->standard_PIMC_print_paths( "multi-move_paths.dat" );
			config->standard_PIMC_print_beadlist( "multi-move_beadlist.dat" );
			
			config_B->standard_PIMC_print_paths ("multi-move_paths_B.dat" );
			config_B->standard_PIMC_print_beadlist("multi-move_beadlist_B.dat");
		}
		
// 		int behelf;
// 		std::cin >> behelf;
// 		exit(0);
		
		return 1;
	}
	else // The update has been rejected
	{
		return 0;
	}
}





























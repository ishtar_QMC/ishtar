/*
 *  Contains the standard PIMC update Recede (G-sector)
 */



template <class inter> class update_standard_PIMC_recede
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
};



// init
template <class inter> void update_standard_PIMC_recede<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update RECEDE (ensemble)\n";
	
	ensemble = new_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );

	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_recede<inter>::execute( int m_max )
{
	if( debug ) std::cout << "***execeute-recede(ensemble)***\n";
	
	// obtain the species that is in the G-sector:
	int my_species = ensemble->G_species;
	
	
	if( debug ) std::cout << "my_species: " << my_species << "\n";
	
	// obtain a pointer to that species:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// initialize the interaction-toolbox with this config:
	inter_tools.init( config );
	
	// obtain the number of beads between tail and head (forward in imag. time):
	int m_ex = 0;
	int s_id = config->tail_id;
	
	while( config->beads[ s_id ].get_next_id() != config->head_id )
	{
		s_id = config->beads[ s_id ].get_next_id();
		m_ex++;
		if( m_ex > m_max + 3 )
		{
			break;
		}
	}
	
	if( m_ex == 0 ) // configurations without any beads between head and tail are not permitted!
	{
		std::cout << "Error in update_standard_PIMC_recede, m_ex=0. head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\t head_time: " << config->beads[ config->head_id ].get_time() << "\t tail_time: " << config->beads[ config->tail_id ].get_time() << "\n";
		exit(0);
	}
	
	if(debug) // verbose:
	{
		std::cout << "~~~~~~~~~~~~~~~~ update_standard_PIMC_recede, m_max: " << m_max << "\t m_ex: " << m_ex << "\n"; 
		std::cout << "head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\t head_slice: " << config->beads[ config->head_id ].get_time() << "\t tail_slice: " << config->beads[ config->tail_id ].get_time() << "\n";
	}
	
	
	// Choose the number of beads (between old head and new head) to be deleted:
 	int m = 1 + rdm()*m_max;
	if(debug) std::cout << "--m: " << m << std::endl;
	
	// If there are not enough beads between tail and head, the update must be rejected:
	
	// The idea is that there must remain at least a single bead between head and tail
	if( m_ex <= m+1 ) // TBD TBD TBD For complete sampling of Matsubara Green function, m_ex < m should be sufficient. This is a caveat!
	{
		return 0; // the update is rejected to prevent generation of non-permissible configuration
	}
	
	// Calculate the change in the interaction and in the diffusion_elements:
	double diffusion_product = 1.0;
	double delta_u = 0.0; // OLD - NEW
	
	int i_id = config->head_id;
	std::vector<Bead>new_beads;
	std::vector<double> diffusion_elements( m+1, 0.0 ); // vector containing all the diffusion matrix elements (bead-by-bead)
	
	for(int i=0;i<1+m;i++)
	{		
		// add the involved bead to the list:
		new_beads.push_back( config->beads[ i_id ] );
		
		i_id = config->beads[ i_id ].get_prev_id();
		
		// take into account the diffusion_element:
		diffusion_product *= config->beads[ i_id ].get_diffusion_element(); // compute product on-the-fly, for debug purposes only!
		diffusion_elements[i] = config->beads[ i_id ].get_diffusion_element();
		
		// obtain the interaction ( except for last element in loop which is the new head and must be treated separately! )
		if( i < m )
		{
			// interaction due to the same species (and ext_pot):
			delta_u += inter_tools.old_interaction( &(*config).beadlist[ config->beads[ i_id ].get_time() ], i_id, -1 );
			
			// interaction due to all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( iSpecies != my_species )
				{
					delta_u += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ i_id ] );
				}
			}
		}
		
	} // end loop i to 1+m
	
	
	// i_id is now the ID of the new head:
	new_beads.push_back( config->beads[ i_id ] );
	
	// reverse the order of the new_beads vector:
	std::reverse( std::begin(new_beads), std::end(new_beads) );
	
	// interaction due to the old head ( same species and ext_pot ):
	delta_u += 0.50 * inter_tools.old_interaction( &(*config).beadlist[ config->beads[ config->head_id ].get_time() ], config->head_id, -1 );
	
	// interaction due to the new head ( same species and ext_pot ):
	delta_u += 0.50 * inter_tools.old_interaction( &(*config).beadlist[ config->beads[ i_id ].get_time() ], i_id, -1 );
	
	
	// also obtain interaction of old and new head with all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != my_species )
		{
			// old head
			delta_u += 0.50 * inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+m ] );
			
			// new_head
			delta_u += 0.50 * inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 0 ] );
		}
	}
	
	
	
	// ### Consider the special case, where head and tail are on the same timeslice before the update:
	// -> head and tail do not interact
	if( config->beads[ config->head_id ].get_time() == config->beads[ config->tail_id ].get_time() )
	{
		double head_tail_inter = my_interaction.pair_interaction( &(*config).beads[ config->tail_id ], &(*config).beads[ config->head_id ] );
		delta_u -= 0.250 * head_tail_inter; // minus sign, because head and tail do not interact!
		if( debug ) // verbose:
		{
			std::cout << "#### SPECIAL CASE head_slice == tail_slice before the update!\n";
			std::cout << "head_slice: " << config->beads[ config->head_id ].get_time() << "\t tail_slice: " << config->beads[ config->tail_id ].get_time() << "\t head_tail_inter: " << head_tail_inter << "\n"; 
		}
	}
	
	
	// ### Special case where tail and head are on the same time-slice after the update:
	// -> head and tail do not interact
	if( new_beads[0].get_time() == config->beads[ config->tail_id ].get_time() )
	{
		double new_head_tail_inter = my_interaction.pair_interaction( &new_beads[ 0 ], &(*config).beads[ config->tail_id ] );
		delta_u += 0.250 * new_head_tail_inter;
		if( debug ) // verbose:
		{
			std::cout << "#### SPECIAL CASE new_head_slice = tail_slice after the update!\n";
			std::cout << "new_head_tail_inter: " << new_head_tail_inter << "\n";
		}
	}
	

	// Calculate the reverse sampling probs for the old head and for the connection between old and new head:
	double prob_head = sampling_tools.reverse_head_prob( &new_beads[0], &(*config).beads[ config->head_id ] );
	double prob_connect;
	std::vector<double> probs_connect( m, 0.0 ); // vector containing the sampling probs of the individual beads (bead-by-bead)
	
	if( config->params.system_type < config->params.n_traps ) // connection-prob for the traps:
	{
		prob_connect = sampling_tools.trap_backward_connect( &new_beads, m, probs_connect );
	}
	else // connection-prob for PBC-systems:
	{
		prob_connect = sampling_tools.PBC_backward_connect( &new_beads, m, probs_connect );
	}
	

	// Calculate the acceptance ratio, draw rdm number for decision:
	double choice = rdm();
	
	double delta_exponent = -config->params.mu * config->params.epsilon * (1.0+m);
	double ratio = exp( delta_exponent ) * exp( config->params.epsilon * delta_u );
	

	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		double sampling_diffusion_ratio = 1.0 / diffusion_elements[m];
		for(int i=0;i<m;i++)
		{
			sampling_diffusion_ratio *= ( probs_connect[i] / diffusion_elements[i] );
		}
		ratio = ratio * sampling_diffusion_ratio * prob_head;
	}
	
	
	if( debug ) // verbose, check for overflow without the PBC_detailed_balance option:
	{
		double ratio2 = exp( delta_exponent ) * exp( config->params.epsilon * delta_u ) * prob_connect * prob_head / diffusion_product;

		if( fabs( ratio/ratio2-1.0) > 1e-13 )
		{
			std::cout << "Error in std_PIMC Recede, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
	}
	

	// Artificial potential to favor a given target N:
	// -> Use carefully!
	if( config->params.N_potential_control )
	{
		double old_N_diff =  config->params.N - config->my_exp / ( config->params.mu * config->params.beta );
		double new_N_diff = config->params.N - ( config->my_exp + delta_exponent ) / ( config->params.mu * config->params.beta );
		double W_N_scale = -0.50 / ( config->params.sigma_target * config->params.sigma_target );
		double W_N_new = exp( W_N_scale * new_N_diff * new_N_diff );
		double W_N_old = exp( W_N_scale * old_N_diff * old_N_diff );

		if(debug) std::cout << "W_N_new = " << W_N_new << "\t W_N_old = " << W_N_old << "\t W_N_scale = " << W_N_scale << "\n";
		ratio = ratio * W_N_new / W_N_old;
	}


	if( debug ) // verbose:
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t delta_exponent: " << delta_exponent << "\t delta_u: " << delta_u << "\n";
		std::cout << "prob_connect: " << prob_connect << "\t prob_head: " << prob_head << "\t diffusion_product: " << diffusion_product << "\n";
		std::cout << "head_time: " << config->beads[ config->head_id ].get_time() << "\t tail_time: " << config->beads[ config->tail_id ].get_time() << "\t new_head_time: " << new_beads[ 0 ].get_time() << std::endl;
	}
	
	// Artificial potential between head and tail:
	// -> Use carefully!
	if( config->params.head_tail_potential )
	{
		double old_diff_sq = 0.0;
		double new_diff_sq = 0.0;
		
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			old_diff_sq += pow( config->beads[ config->head_id ].get_coord( iDim ) - config->beads[ config->tail_id ].get_coord( iDim ), 2 );
			new_diff_sq += pow( config->beads[ config->tail_id ].get_coord( iDim ) - new_beads[ 0 ].get_coord( iDim ), 2 );
		}
		
		ratio *= exp( ( old_diff_sq - new_diff_sq ) / ( config->params.beta * config->params.head_tail_potential_eta ) );
	}
	

	// Artificial potential on the number of pair-exchanges:
	// -> Use carefully!
	int new_Npp = config->Npp;
	if( config->params.pp_control )
	{
		int old_difference = config->beads[ config->tail_id ].get_time() - config->beads[ config->head_id ].get_time();
		if( old_difference < 0 ) old_difference += config->params.n_bead;
		
		int new_difference = config->beads[ config->tail_id ].get_time() - new_beads[ 0 ].get_time();
		if( new_difference < 0 ) new_difference += config->params.n_bead;
		
		if( ( old_difference == 0 ) || ( ( new_difference < old_difference ) && ( new_difference > 0 ) ) )
		{
			new_Npp += -1;
		}
		
		double inv_old_pp_weight = exp( -config->params.pp_delta * ( config->params.pp_kappa - config->Npp ) ) + 1.0;
		double inv_new_pp_weight = exp( -config->params.pp_delta * ( config->params.pp_kappa - new_Npp ) ) + 1.0;
		
		ratio = ratio * inv_old_pp_weight / inv_new_pp_weight;
		
		if( debug )
		{
			std::cout << "Additional change in acceptance ratio due to pp_control...\n";
			std::cout << "ratio: " << ratio << "\t old_pp_weight: " << 1.0/inv_old_pp_weight << "\t new_pp_weight: " << 1.0/inv_new_pp_weight << "\t Npp: " << config->Npp << "\t new_Npp: " << new_Npp << "\n";
		}
	}

	
	if( choice <= ratio ) // The update is accepted!
	{
		config->Npp = new_Npp; // update the number of pair-exchanges in the configuration
		
		// Keep track of the sign of this configuration:
		int old_difference = config->beads[ config->tail_id ].get_time() - config->beads[ config->head_id ].get_time();
		if( old_difference < 0 ) old_difference += config->params.n_bead;
		
		int new_difference = config->beads[ config->tail_id ].get_time() - new_beads[ 0 ].get_time();
		if( new_difference < 0 ) new_difference += config->params.n_bead;
		
		if( ( old_difference == 0 ) || ( ( new_difference < old_difference ) && ( new_difference > 0 ) ) )
		{
			if( config->params.canonical_PIMC ) return 0; // in this case, we would remove a particle, which is not possible in the canonical ensemble
			config->my_sign *= -1.0;
		}
		
		
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product = ensemble->total_diffusion_product / diffusion_product;
	
		ensemble->total_energy -= delta_u; // update total energy of ensemble
		ensemble->total_exponent += delta_exponent; // update the (mu)-exponent of the entire ensemble
		
		config->my_exp += delta_exponent; // update mu-exponent of the selected species

		// update the id of the head:
		config->head_id = i_id;
		
		// delete selected beads from config:
		int old_id = i_id;
		for(int i=0;i<1+m;i++)
		{
			int tmp_id = old_id;
			old_id = config->beads[ tmp_id ].get_next_id();
			if(debug) std::cout << "--i: " << i << "\t old_id: " << old_id << "\n";
			config->delete_bead( old_id );
		} // end loop i to 1+m
		
		config->beads[ config->head_id ].set_next_id( -1 ); // head is end of trajectory!
		
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			config->standard_PIMC_print_paths( "recede_paths.dat" );
			config->standard_PIMC_print_beadlist( "recede_beadlist.dat" );
		}
		
		return 1;
	}
	else // The update is rejected!
	{
		return 0;
	}
}





























/*
 *  Contains the PB-PIMC update close
 *  -> m_max is the maximum number of links which can be generated
 *  ### Update has been debugged and benchmarked. Yet, use in PB-PIMC simulations is NOT recommended!
 *  ### Worm algorithm is unnecessarily complicated. Use Deform update, instead!
 *  ### No implementation of bead-by-bead for sampling probs, etc. However, overflow is checked and does not happen in practice!
 */
 
template <class inter> class update_pbpimc_close
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max, double factor, bool debug ); // execute the update
	
	bool debug; // Execute Update in Debug mode?
private:
	
	// Variables of the update:
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species

	// Constant structures to prevent dynamic memory allocation:
	
	std::vector< selection_toolbox<inter> >selection_tools;
	std::vector< sampling_toolbox<inter> >sampling_tools;
	std::vector< force_toolbox<inter> >force_tools;
	std::vector< interaction_toolbox<inter> >inter_tools;
	std::vector< diffusion_toolbox<inter> >diffusion_tools;
	
	std::vector< inter > my_interaction;

	std::vector< change_config >update;
	
	std::vector<std::vector <std::vector <double> > > const_store_the_force;
	std::vector<std::vector <std::vector <std::vector <double> > > > const_store_the_species;
	
	std::vector<Matrix> const_new_matrix_list;
	std::vector<double> const_new_determinant_list;
	std::vector<double> const_new_sign_list;
	
	// Calculate the number of missing links between head and tail:
	int missing_links();
	
	// Obtain information about beadlists of head and tail:
	void head_and_tail_blist( std::vector<int> **head_blist, std::vector<int> **tail_blist, double *head_scale, double *tail_scale );
};


// Assigns pointers to relevant beadlist and scale-factors of head and tail slice:
template <class inter> void update_pbpimc_close<inter>::head_and_tail_blist( std::vector<int> **head_blist, std::vector<int> **tail_blist, double *head_scale, double *tail_scale )
{
	worm_configuration* config = &(*ensemble).species_config[ ensemble->G_species ]; // pointer to the species with head and tail
	
	int head_time = (*config).beads[(*config).head_id].get_time();
	int tail_time = (*config).beads[(*config).tail_id].get_time();
	
	if( (*config).head_kind == 1 )
	{ // ancilla slice A
		(*head_blist) = &(*config).beadlist_A[head_time];
		(*head_scale) = (*config).params.v2;
	}
	else if( (*config).head_kind == 2 )
	{ // ancilla slice B
		(*head_blist) = &(*config).beadlist_B[head_time];
		(*head_scale) = (*config).params.v1;
	}
	else
	{ // main slice 
		(*head_blist) = &(*config).beadlist[head_time];
		(*head_scale) = (*config).params.v1;
	}
	
	
	if( (*config).tail_kind == 1 )
	{ // ancilla slice A
		(*tail_blist) = &(*config).beadlist_A[tail_time];
		(*tail_scale) = (*config).params.v2;
	}
	else if( (*config).tail_kind == 2 )
	{ // ancilla slice B
		(*tail_blist) = &(*config).beadlist_B[tail_time];
		(*tail_scale) = (*config).params.v1;
	}
	else
	{ // main slice 
		(*tail_blist) = &(*config).beadlist[tail_time];
		(*tail_scale) = (*config).params.v1;
	}

	return;
}




// init
template <class inter> void update_pbpimc_close<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{	
	ensemble = new_ensemble;
	debug = new_debug;
	
	int n_species = ensemble->n_species;
	
	for(int iSpecies=0;iSpecies<n_species;iSpecies++) // initialize a toolbox for each species:
	{
		worm_configuration* cfg = &(*new_ensemble).species_config[ iSpecies ];

		// Update the config pointer from the selection_toolbox
		selection_toolbox< inter > tmp_selection;
		tmp_selection.init( cfg );
		selection_tools.push_back( tmp_selection );
		

		// Update the parameters pointer from the sampling_toolbox
		sampling_toolbox< inter > tmp_sampling;
		tmp_sampling.init( &(*cfg).params );
		sampling_tools.push_back( tmp_sampling );
		

		// Update the config pointer from the inter_tools
		interaction_toolbox< inter > tmp_interaction;
		tmp_interaction.init( cfg );
		inter_tools.push_back( tmp_interaction );
		

		// Update the config pointer from the change_config
		change_config tmp_update;
		tmp_update.init( cfg );
		update.push_back( tmp_update );
		

		// Update the config pointer from the diffusion_toolbox
		diffusion_toolbox< inter > tmp_diffusion;
		tmp_diffusion.init( cfg );
		diffusion_tools.push_back( tmp_diffusion );
		

		// Update the parameters pointer from the interaction class:
		inter tmp_inter;
		tmp_inter.init( &(*cfg).params );
		my_interaction.push_back( tmp_inter );
		

		// Update the config pointer from the force_toolbox
		force_toolbox< inter > tmp_force;
		tmp_force.init( cfg );
		force_tools.push_back( tmp_force );
	
	} // end loop iSpecies
	
	
	
	
	// Create a constant store_the_force structure with 3*P slots for N particles and dim dimensions:
	std::vector<double>dims( (*ensemble).params.dim, 0.0 );
	
	std::vector<std::vector <double> >slice_force( (*ensemble).N_tot, dims );
	const_store_the_force.assign( (*ensemble).params.n_bead*3, slice_force );
	
	
	// Create constant structures for the diffusion parts:
	const_new_matrix_list.assign( (*ensemble).params.n_bead*3, (*ensemble).species_config[0].matrix_list[0] );
	const_new_determinant_list.assign( (*ensemble).params.n_bead*3, 1.0 );
	const_new_sign_list.assign( (*ensemble).params.n_bead*3, 1.0 );
	

	std::vector<std::vector<std::vector <double> > > slice_part;
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		std::vector<std::vector <double> > species_part( (*ensemble).species_config[iSpecies].params.N, dims );
		slice_part.push_back( species_part );
	}
		
	const_store_the_species.assign( (*ensemble).params.n_bead*3, slice_part );
}



// Return the number of missing "links" between head and tail:
template <class inter> int update_pbpimc_close<inter>::missing_links()
{
	worm_configuration* config = &(*ensemble).species_config[ ensemble->G_species ]; // pointer to the species with head and tail
	
	int m = 0; // Initialize the link counter
	int tk = (*config).head_kind; // Initialize the slice kind counter 
	int ttime = (*config).beads[(*config).head_id].get_time(); // Initialize the propagator counter
	
	while(true) // Advance forward in imaginary time until you reach the tail:
	{
		if( tk == 3 ) tk = 0; // time-slice specifier (A, B, main)
		tk++;
		
		m++; // Increase the number of encountered "links"
		
		if( tk == 3 ) // Adjust the propagator number when you reach a new main slice
		{
			ttime++;
			if(ttime == (*config).params.n_bead) ttime = 0;
		}
		
		// Break the loop when you reach slice and slice kind of tail
		if( ttime == config->beads[ config->tail_id ].get_time() )
		{
			if( tk == config->tail_kind ) break;
		}
	}
	
	return m; // Return the number of encountered missing "links"
}




// Execute the update:
template <class inter> int update_pbpimc_close<inter>::execute( int m_max, double factor, bool debug )
{
	worm_configuration* config = &(*ensemble).species_config[ ensemble->G_species ]; // pointer to the species with head and tail
	int my_species = ensemble->G_species;
	
	// Determine the number of missing total missing links, m:
	int m = missing_links();
	
	
	if( debug ) // verbose:
	{
		std::cout << "----Close(PB-PIMC)\n";
		std::cout << "m: " << m << "\t my_species: " << my_species << "\n";
	}
	
	// If more than m_max links are missing, the update is rejected (detailed balance with open)
	if( m > m_max+1 )
	{
		return 0;
	}
	
	
	// Obtain the length of an imaginary time step:
	double epsilon = config->params.epsilon;

	
	// ########################################################################
	// ######### Obtain the m-1 missing beads between head and tail: ##########
	// ########################################################################
	
	std::vector<Bead>new_beads( 1, config->beads[ config->head_id ] );
	std::vector<int>number_list( 1, config->beads[ config->head_id ].get_number() );
	int time = new_beads[0].get_time(); // Initialize propagator counter
	int cnt = config->head_kind; // Initialize slice kind counter
	
	for(int i=0;i<m-1;i++)
	{
		if(cnt==3) cnt = 0;
		cnt++;
		
		int nid; // id of missing bead:

		if( cnt == 1 )
		{ // ancilla slice A
			nid = config->next_id_list_A[time];
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			nid = config->next_id_list_B[time];
		}
		else
		{ // main slice
			time++;
			if( time == config->params.n_bead ) time = 0; // periodicity in imag. time
			nid = config->next_id_list[ time ];
		}

		new_beads.push_back( config->beads[ nid ] );
		number_list.push_back( config->beads[ nid ].get_number() );
	}

	
	// Push back the tail as the fixed end-point of the trajectory:
	new_beads.push_back( config->beads[ config->tail_id ] );
	number_list.push_back( config->beads[ config->tail_id ].get_number() );

	
	if(debug) std::cout << "Obtained the missing beads between head and tail\n";
	
	
	// ########################################################################
	// ######### Connect head and tail by sampling m-1 beads: #################
	// ########################################################################
	
	double forward_sampling_prob; // ### Currenty no bead-by-bead implementation. Risk of overflow for large P, which does not happen in practice!
	
	if( ensemble->params.system_type < ensemble->params.n_traps ) // sampling for a trap
	{
		forward_sampling_prob = sampling_tools[ my_species ].trap_connect( &new_beads, m-1 );
	}
	else // sampling for PBC
	{
		forward_sampling_prob = sampling_tools[ my_species ].PBC_connect( &new_beads, m-1 ); // There are m-1 missing beads between head and tail
	}
	
	if(debug) std::cout << "Computed forward_sampling_prob\n";
	
	
	// ########################################################################
	// ######### Calculate the change in the energy and the forces: ###########
	// ########################################################################

	double delta_f_sq = 0.0;
	double new_energy = 0.0;

	// Obtain information about slice of head and tail:
	std::vector<int> *head_blist, *tail_blist;
	double head_scale, tail_scale;
	head_and_tail_blist( &head_blist, &tail_blist, &head_scale, &tail_scale );
	
	// Head and tail are updated only with 0.50 the energy:
	new_energy += 0.50 * head_scale * inter_tools[ my_species ].ensemble_old_interaction( ensemble, config->head_id, my_species, config->head_kind, head_blist, -1 ); // the last argument means that there is no missing bead on the head-slice
	new_energy += 0.50 * tail_scale * inter_tools[ my_species ].ensemble_old_interaction( ensemble, config->tail_id, my_species, config->tail_kind, tail_blist, -1 ); // the last argument means that there is no missing bead on the tail-slice

	
	cnt = (*config).head_kind; // Initialize the slice kind counter
	for(int i=0;i<m-1;i++)
	{
		if(cnt == 3) cnt = 0;
		cnt++;
		int time = new_beads[1+i].get_time();
		
		std::vector<std::vector<double> >tmp_f;
		if( cnt == 1 )
		{ // ancilla slice A
			double ftmp = force_tools[ my_species ].ensemble_handle_new( ensemble, my_species, 1, &new_beads[i+1], &(*config).beadlist_A, &const_store_the_force[i], &(*config).next_id_list_A, &const_store_the_species[i] );
			delta_f_sq += (1.0-2.0*(*config).params.a1)*ftmp;
			
			new_energy += config->params.v2 * inter_tools[ my_species ].ensemble_new_interaction( ensemble, my_species, 1, &(*config).beadlist_A[time], &new_beads[i+1], config->next_id_list_A[time] );
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			double ftmp = force_tools[ my_species ].ensemble_handle_new( ensemble, my_species, 2, &new_beads[i+1], &(*config).beadlist_B, &const_store_the_force[i], &(*config).next_id_list_B, &const_store_the_species[i] );
			delta_f_sq += (*config).params.a1*ftmp;
			
			new_energy += config->params.v1 * inter_tools[ my_species ].ensemble_new_interaction( ensemble, my_species, 2, &(*config).beadlist_B[time], &new_beads[i+1], config->next_id_list_B[time] );
		}
		else
		{ // main slice
			double ftmp = force_tools[ my_species ].ensemble_handle_new( ensemble, my_species, 3, &new_beads[i+1], &(*config).beadlist, &const_store_the_force[i], &(*config).next_id_list, &const_store_the_species[i] );
			delta_f_sq += (*config).params.a1*ftmp;
			
			new_energy += config->params.v1 * inter_tools[ my_species ].ensemble_new_interaction( ensemble, my_species, 3, &(*config).beadlist[time], &new_beads[i+1], config->next_id_list[time] );
		}

	} // end loop over all affected slices
	

	if( debug ) std::cout << "Computed energy/ force difference\n";

	
	// ########################################################################
	// ######### Calculate the change in m diffusion matrices, etc. ###########
	// ########################################################################
	
	double backward_selection_prob = 1.0; // product of probs to select this particular trajectory in the reverse (open) update
	double determinant_ratio = 1.0;
	double new_sign = config->my_sign;
	
	cnt = config->head_kind; // Initialize slice kind counter
	for(int i=0;i<m;i++) // loop over all affected slices
	{
		
		// Obtain propagator number of start and end slice:
		int ktime = new_beads[i].get_time();
		int ktime_plus = new_beads[1+i].get_time();

		// Obtain old sign and determinant:
		double ksign, kdeterminant;

		// Obtain information about the kind of transition:
		std::vector<int> *start_blist, *end_blist;
		int next_start, next_end;
		double scale;
		
		Matrix* mp;
		if( cnt == 1 )
		{ // starting from ancilla slice A
			mp = &(*config).matrix_list_A[ktime];
			ksign = config->sign_list_A[ktime];
			kdeterminant = config->determinant_list_A[ktime];
			scale = config->params.t1;

			start_blist = &(*config).beadlist_A[ktime];
			end_blist = &(*config).beadlist_B[ktime_plus];
			next_start = config->next_id_list_A[ktime];
			next_end = config->next_id_list_B[ktime_plus];
		}
		else if( cnt == 2 )
		{ // starting from ancilla slice B
			mp = &(*config).matrix_list_B[ktime];
			ksign = config->sign_list_B[ktime];
			kdeterminant = config->determinant_list_B[ktime];
			scale = 2.0*config->params.t0;

			start_blist = &(*config).beadlist_B[ktime];
			end_blist = &(*config).beadlist[ktime_plus];
			next_start = config->next_id_list_B[ktime];
			next_end = config->next_id_list[ktime_plus];
		}
		else
		{ // starting from main slice
			cnt = 0; // reset the slice-kind counter
			mp = &(*config).matrix_list[ktime];
			ksign = config->sign_list[ktime];
			kdeterminant = config->determinant_list[ktime];
			scale = config->params.t1;

			start_blist = &(*config).beadlist[ktime];
			end_blist = &(*config).beadlist_A[ktime_plus];
			next_start = config->next_id_list[ktime];
			next_end = config->next_id_list_A[ktime_plus];
		}
		
		// obtain the new diffusion matrix with the new bead:
		diffusion_tools[ my_species ].add_matrix( start_blist, end_blist, &new_beads[i], &new_beads[1+i], scale, next_start, next_end, new_beads[i].get_number(), new_beads[1+i].get_number(), mp, &const_new_matrix_list[i] );
		
		// obtain new determinant and sign:
		const_new_sign_list[i] = 1.0;
		const_new_determinant_list[i] = const_new_matrix_list[i].determinant();
		if(const_new_determinant_list[i] < 0.0)
		{
			const_new_determinant_list[i] = -1.0*const_new_determinant_list[i];
			const_new_sign_list[i] = -1.0;
		}
		
		new_sign = new_sign*ksign*const_new_sign_list[i]; // update the total sign
		determinant_ratio = determinant_ratio*const_new_determinant_list[i]/kdeterminant; // update the total determinant ratio

			
		// Reverse selection prob (for the open update, detailed balance!):
		int end_bead_id = next_end;
		if( i == -1+m ) end_bead_id = config->tail_id;
		
		double my_prob = selection_tools[ my_species ].get_selection_prob_reverse( end_blist, end_bead_id, -1, &new_beads[i], &new_beads[i+1], scale, true );
		backward_selection_prob *= my_prob; // compute the product of the reverse selection probs on-the-fly

		cnt++; // update the slice-kind counter
		
	} // end loop over all affected slices
	
	
	// Random number to decide acceptance:
	double choice = rdm();

	// Obtain total difference in imaginary time between head and tail:
	double tail_real_time = config->beads[ config->tail_id ].get_real_time();
	double head_real_time = config->beads[ config->head_id ].get_real_time();
	double tau_diff = tail_real_time - head_real_time;
	if( tau_diff < 0.0 ) tau_diff += config->params.beta;

	// Calculate the exponential factor:
	double new_exp = exp( config->params.mu*tau_diff - epsilon* ( new_energy - epsilon*epsilon*config->params.u0*delta_f_sq ) );

	// Calculate the detailed balance factor with the update open
	double add_fac = 3.0*double( config->params.N * config->params.n_bead * m_max * ensemble->n_species );

	// Calculate the acceptance ration:
	double ratio = factor * backward_selection_prob * determinant_ratio * new_exp / ( add_fac * forward_sampling_prob );
	
	
	if( debug ) // verbose:
	{
		std::cout << "choice: " << choice << "\t ratio: " << ratio << "\n";
		std::cout << "factor: " << factor << "\t add_fac: " << add_fac << "\t backward_selection_prob: " << backward_selection_prob << "\t forward_sampling_prob: " << forward_sampling_prob << "\n";
		std::cout << "determinant_ratio: " << determinant_ratio << "\t new_factor: " << new_exp << "\t new_energy: " << new_energy << "\t delta_f_sq: " << delta_f_sq << "\n";
		std::cout << "exp(mu): " << exp( (*config).params.mu*tau_diff ) << "\n";
	}
	
	
	// Check for possible overflows, bead-by-bead is atm not implemented:
	if( isnana( determinant_ratio ) || isnana(backward_selection_prob) || isnana( forward_sampling_prob) )
	{
		std::cout << "IIII Close; determinant_ratio: " << determinant_ratio << "\t backward_selection_prob: " << backward_selection_prob << "\t forward_sampling_prob: " << forward_sampling_prob << "\n";
		int icn;
		std::cin >> icn;
	}
	

	// Decide, if the update is accepted:
	if( choice <= ratio ) // The update is accepted:
	{
		
		ensemble->diag = true; // we are now in the Z_sector
		ensemble->G_species = -1;
		
		
		// Updated forces, interaction, etc.:
		config->my_sign = new_sign;
		ensemble->total_force_sq -= delta_f_sq;
		ensemble->total_energy +=  new_energy;
		
		
		// Update the m kinetic marices etc.:
		int cnt = config->head_kind; // Initialize slice kind counter
		for(int i=0;i<m;i++)
		{
			// Obtain time of transition
			int ltime=new_beads[i].get_time();
			
			if( cnt == 1 )
			{ // ancilla slice A
				config->matrix_list_A[ltime].copy(&const_new_matrix_list[i]);
				config->determinant_list_A[ltime] = const_new_determinant_list[i];
				config->sign_list_A[ltime] = const_new_sign_list[i];
			}
			else if( cnt == 2 )
			{ // ancilla slice B
				config->matrix_list_B[ltime].copy(&const_new_matrix_list[i]);
				config->determinant_list_B[ltime] = const_new_determinant_list[i];
				config->sign_list_B[ltime] = const_new_sign_list[i];
			}
			else
			{ // main slice
				cnt = 0;
				config->matrix_list[ltime].copy(&const_new_matrix_list[i]);
				config->determinant_list[ltime] = const_new_determinant_list[i];
				config->sign_list[ltime] = const_new_sign_list[i];
			}
			
			cnt++;
		}
		

		// Update the m-1 changed beads and the forces:
		cnt = config->head_kind; // Initialize slice kind counter
		for(int i=0;i<m-1;i++)
		{
			int ktime = new_beads[i+1].get_time(); // propagator number of the affected slice
			if(cnt==3) cnt = 0;
			cnt++;

			if(cnt == 1)
			{ // ancilla slice A
				config->beads[ config->next_id_list_A[ktime] ].copy(&new_beads[1+i]);
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 1, &(*config).beadlist_A[ktime], config->next_id_list_A[ktime], -1, &const_store_the_force[i], &const_store_the_species[i] );
 				config->next_id_list_A[ktime] = -1;

			}
			else if(cnt == 2)
			{ // ancilla slice B
				config->beads[ config->next_id_list_B[ktime] ].copy(&new_beads[1+i]);
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 2, &(*config).beadlist_B[ktime], config->next_id_list_B[ktime], -1, &const_store_the_force[i], &const_store_the_species[i] );
 				config->next_id_list_B[ktime] = -1;
			}
			else
			{ // main slice;
				config->beads[ config->next_id_list[ktime] ].copy(&new_beads[1+i]);
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 3, &(*config).beadlist[ktime], config->next_id_list[ktime], -1, &const_store_the_force[i], &const_store_the_species[i] );
 				config->next_id_list[ktime] = -1;
			}
		}
		
		
		// Now this particular species is in the Z-sector, too:
		config->head_id=-1;
		config->tail_id=-1;
		config->diag=1;
		
		return 1;
	}
	else // The update is rejected!
	{
		return 0;
	}
}




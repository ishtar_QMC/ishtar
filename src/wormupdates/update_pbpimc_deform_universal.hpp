/*
 *  Contains the PB-PIMC update deform (universal)
 *  ### The central update of the PB-PIMC method!
 *  ### Works with multiple species, and in both Z- and G-sector
 */




template <class inter> class update_pbpimc_deform_universal
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute(int m_max); // execute the update
	
	
private:
	
	// Variables of the update:
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	bool debug; // Execute Update in Debug mode?
	

	// Constant structures to prevent dynamic memory allocation:
	
	std::vector< selection_toolbox<inter> >selection_tools;
	std::vector< sampling_toolbox<inter> >sampling_tools;
	std::vector< force_toolbox<inter> >force_tools;
	std::vector< interaction_toolbox<inter> >inter_tools;
	std::vector< diffusion_toolbox<inter> >diffusion_tools;
	
	std::vector< inter > my_interaction;

	std::vector< change_config >update;
	
	std::vector<std::vector <std::vector <double> > > const_store_the_force;
	std::vector<std::vector <std::vector <std::vector <double> > > > const_store_the_species;
	
	std::vector<Matrix> const_new_matrix_list;
	std::vector<double> const_new_determinant_list;
	std::vector<double> const_new_sign_list;
};



// init
template <class inter> void update_pbpimc_deform_universal<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "Universal(PB-PIMC)_deform_init\n";

	
	ensemble = new_ensemble;
	debug = new_debug;
	
	int n_species = ensemble->n_species;


	for(int iSpecies=0;iSpecies<n_species;iSpecies++) // initialize a toolbox for each species:
	{
		worm_configuration* cfg = &(*new_ensemble).species_config[ iSpecies ];

		// Update the config pointer from the selection_toolbox:
		selection_toolbox< inter > tmp_selection;
		tmp_selection.init( cfg );
		selection_tools.push_back( tmp_selection );
		

		// Update the parameters pointer from the sampling_toolbox:
		sampling_toolbox< inter > tmp_sampling;
		tmp_sampling.init( &(*cfg).params );
		sampling_tools.push_back( tmp_sampling );
		

		// Update the config pointer from the inter_tools:
		interaction_toolbox< inter > tmp_interaction;
		tmp_interaction.init( cfg );
		inter_tools.push_back( tmp_interaction );
		

		// Update the config pointer from the change_config:
		change_config tmp_update;
		tmp_update.init( cfg );
		update.push_back( tmp_update );
		

		// Update the config pointer from the diffusion_toolbox:
		diffusion_toolbox< inter > tmp_diffusion;
		tmp_diffusion.init( cfg );
		diffusion_tools.push_back( tmp_diffusion );
		

		// Update the parameters pointer from the interaction class:
		inter tmp_inter;
		tmp_inter.init( &(*cfg).params );
		my_interaction.push_back( tmp_inter );
		

		// Update the config pointer from the force_toolbox:
		force_toolbox< inter > tmp_force;
		tmp_force.init( cfg );
		force_tools.push_back( tmp_force );
	
	} // end loop iSpecies
	

	// Create a constant store_the_force structure with 3*P slots for N particles and dim dimensions:
	std::vector<double>dims( ensemble->params.dim, 0.0 );
	
	std::vector<std::vector <double> >slice_force( ensemble->N_tot, dims );
	const_store_the_force.assign( ensemble->params.n_bead*3, slice_force );
	
	
	// Create constant structures for the diffusion parts
	const_new_matrix_list.assign( ensemble->params.n_bead*3, ensemble->species_config[0].matrix_list[0] );
	const_new_determinant_list.assign( ensemble->params.n_bead*3, 1.0 );
	const_new_sign_list.assign( ensemble->params.n_bead*3, 1.0 );
	

	std::vector<std::vector<std::vector <double> > > slice_part;
	for(int iSpecies=0;iSpecies<ensemble->n_species;iSpecies++)
	{
		std::vector<std::vector <double> > species_part( ensemble->species_config[iSpecies].params.N, dims );
		slice_part.push_back( species_part );
	}
		
	const_store_the_species.assign( ensemble->params.n_bead*3, slice_part );

}




// Execute the Monte Carlo update:
template <class inter> int update_pbpimc_deform_universal<inter>::execute( int m_max )
{
	// select the species to be updated:
	int my_species = ensemble->n_species * rdm();
	
	// Obtain a pointer to the config to be changed:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	
	// select the kind of start slice:
	int kind;
	std::vector<std::vector <int> > *start_blist; // pointer to the beadlist of the start slice
	std::vector<int> *start_nlist; // pointer to the next_list of the start slice
	
	kind = selection_tools[ my_species ].random_slice_type( &start_blist, &start_nlist );
	
	// Select the total number (including ancilla stuff) of beads to be changed:
	int m = 1 + rdm()*m_max; // at least change a single bead

	// Select time:
	int start_time = rdm()*config->params.n_bead;
	
	// Select a startbead on that timeslice:
	int start_number = rdm()*config->params.N;
	int start_id = (*start_blist)[ start_time ][ start_number ];
	
	
	// If the selected startbead is missing (i.e. in an open config), we repeat the selection until we find a bead on this slice:
	while( start_id == (*start_nlist)[ start_time ] )
	{
		start_number = rdm()*config->params.N;
		start_id = (*start_blist)[ start_time ][ start_number ];
	}
	

	if(debug) // verbose:
	{
		std::cout << "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
		std::cout << "PB-PIMC universal_deform, kind: " << kind << "\n";
		std::cout << "start_number: " << start_number << "\t my_species: " << my_species << "\n";
		std::cout << "m: " << m << "\tstart_time: " << start_time << "\tstart_id: " << start_id << "\n";
	}
	

	// #####################################################################################################
	// ############### Select (m+1) other beads on the next slices: ########################################
	// #####################################################################################################
	
	std::vector<int>id_list( 1, start_id ); // contains ids of the artificial trajectory
	std::vector<int>number_list( 1, start_number ); // contains the (particle-)numbers of the artificial trjaectory
	std::vector<Bead>new_beads( 1, config->beads[ start_id ] ); // contains the beads of the artificial trajectory
	
	int time = start_time;
	std::vector<double> forward_selection_vector( m+1, 0.0 ); // contains selection probs for every bead

	int cnt = kind; // cnt is slice type identifier
	int end_kind; // store the slice type of the last slice 
	
	for(int i=0;i<(m+1);i++) // Loop over all slices of the artificial trajectory:
	{

		Matrix* mat; // pointer to the diffusion matrix of the current time slice
 		std::vector<int> *blist; // beadlist of this slice
		
		int tmp_number;  // selected particle id on this slice
		int next; // id of non-exisiting bead on this slice
		double p_i; // selection prob. for bead i
		double scale; // scales the imaginary time step for the rho_funs
		
		if( i == m ) end_kind = cnt; // remembers the kind of transition between the last slices of the artificial trajectory
		
		if( cnt == 3 )
		{ 
			// transition from main slice to A
			mat = &(*config).matrix_list[ time ];
			scale = config->params.t1;
			blist = &(*config).beadlist_A[ time ];
			next = config->next_id_list_A[ time ];
			cnt = 0;
		}
		else if( cnt == 2 )
		{ 
			// from ancilla slice B to main slice
			mat = &(*config).matrix_list_B[ time ];
			time = time + 1;
			if( time == config->params.n_bead ) time=0;
			scale = 2.0*config->params.t0;
			blist = &(*config).beadlist[ time ];
			next = config->next_id_list[ time ];
		}
		else
		{ 
			// from ancilla slice A to B
			mat = &(*config).matrix_list_A[ time ];
			scale = config->params.t1;
			blist = &(*config).beadlist_B[ time ];
			next = config->next_id_list_B[ time ];
		}
	
		// special indicates whether this is the last slice of the artificial trajectory
		bool special = true;
		if( i < m ) special = false;
		
		// select a bead on this time slice acc. to diffusion matrix elements:
 		int tmp_id = selection_tools[ my_species ].PBC_select_bead_from_blist( &new_beads[i], blist, next, scale, &p_i, &tmp_number, special );
		

		// Update the forward selection prob:
		forward_selection_vector[i] = p_i; // bead-by-bead
		
		// Update the list of new beads from this artificial trajectory:
		new_beads.push_back( config->beads[ tmp_id ] );
		
		// Update the id list from the artificial trajectory:
		id_list.push_back( tmp_id );

		// Update the particle-id list from this artificial trajectory:
		number_list.push_back( tmp_number );
		
		// Update the slice type identifier:
		cnt++;
	} // end loop i < m+1
	
	
	// #############################################################################################################################################################
	// ############## Reconnect the fixed end-points of the artificial trajectory, calculate the prob. to sample the old configuration: ############################
	// #############################################################################################################################################################
	
	double sampling_ratio; // ratio of reverse sampling to actual sampling probabilitiy, computed bead-by-bead
	
	if( ensemble->params.system_type < ensemble->params.n_traps ) // sampling/ prob for a trap:
	{
		sampling_ratio = sampling_tools[ my_species ].combined_trap_connect( &new_beads, m );
	}
	else // sampling/ prob for PBC:
	{
		sampling_ratio = sampling_tools[ my_species ].combined_PBC_connect( &new_beads, m );
	}

	
	// #############################################################################################################################################################
	// ############## Calculate the change in the energy and the forces: ###########################################################################################
	// #############################################################################################################################################################
	
	double delta_u = 0.0;
	double delta_f_sq = 0.0;
	
	cnt = kind;
	for(int i=0;i<m;i++) // loop over all changed time slices
	{
		if( cnt == 3 ) cnt = 0;
		cnt++;
		
		if( cnt == 1 )
		{ // ancilla slice A
			double ftmp = force_tools[ my_species ].ensemble_handle_change( ensemble, my_species, 1, &(*config).beadlist_A, id_list[i+1], &new_beads[i+1], &const_store_the_force[i], &(*config).next_id_list_A, &const_store_the_species[i] );
			delta_f_sq += (1.0 - 2.0*config->params.a1)*ftmp;

			delta_u += config->params.v2 * inter_tools[ my_species ].ensemble_change_interaction( ensemble, my_species, 1, &(*config).beadlist_A[new_beads[i+1].get_time()], &new_beads[i+1], id_list[1+i], config->next_id_list_A[new_beads[i+1].get_time()] );
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			double ftmp = force_tools[ my_species ].ensemble_handle_change( ensemble, my_species, 2, &(*config).beadlist_B, id_list[i+1], &new_beads[i+1], &const_store_the_force[i], &(*config).next_id_list_B, &const_store_the_species[i]);
			delta_f_sq += config->params.a1*ftmp;

			delta_u += config->params.v1 * inter_tools[ my_species ].ensemble_change_interaction( ensemble, my_species, 2, &(*config).beadlist_B[new_beads[i+1].get_time()], &new_beads[i+1], id_list[1+i], config->next_id_list_B[new_beads[i+1].get_time()] );
		}
		else
		{ // main slice
			double ftmp = force_tools[ my_species ].ensemble_handle_change( ensemble, my_species, 3, &(*config).beadlist, id_list[i+1], &new_beads[i+1], &const_store_the_force[i], &(*config).next_id_list, &const_store_the_species[i]);
			delta_f_sq += config->params.a1*ftmp;

			delta_u += config->params.v1 * inter_tools[ my_species ].ensemble_change_interaction( ensemble, my_species, 3, &(*config).beadlist[new_beads[i+1].get_time()], &new_beads[i+1], id_list[1+i], config->next_id_list[new_beads[i+1].get_time()] );
		}
	} // end loop over all changed time slices
	

	// #############################################################################################################################################################
	// ############## Calculate the change in the diffusion matrices, determinants and signs: ######################################################################
	// #############################################################################################################################################################
	
	// transition from first (unchanged) to next slice:
	if( kind == 3 )
	{ // transition from main slice to A
		double dscale = config->params.t1;
		diffusion_tools[ my_species ].change_matrix( &(*config).beadlist[new_beads[0].get_time()], &(*config).beadlist_A[new_beads[1].get_time()], config->next_id_list[new_beads[0].get_time()], config->next_id_list_A[new_beads[1].get_time()], dscale, -1, number_list[1], &new_beads[0], &new_beads[1], &(*config).matrix_list[new_beads[0].get_time()], &const_new_matrix_list[0] );
	}
	else if( kind == 2 )
	{ // transition from ancilla slice B to main slice
		double dscale = 2.0*config->params.t0;
		diffusion_tools[ my_species ].change_matrix( &(*config).beadlist_B[new_beads[0].get_time()], &(*config).beadlist[new_beads[1].get_time()], config->next_id_list_B[new_beads[0].get_time()], config->next_id_list[new_beads[1].get_time()], dscale, -1, number_list[1], &new_beads[0], &new_beads[1], &(*config).matrix_list_B[new_beads[0].get_time()], &const_new_matrix_list[0] );
	}
	else
	{ // transition from ancilla slice A to B
		double dscale = config->params.t1;
		diffusion_tools[ my_species ].change_matrix( &(*config).beadlist_A[new_beads[0].get_time()], &(*config).beadlist_B[new_beads[1].get_time()], config->next_id_list_A[new_beads[0].get_time()], config->next_id_list_B[new_beads[1].get_time()], dscale, -1, number_list[1], &new_beads[0], &new_beads[1], &(*config).matrix_list_A[new_beads[0].get_time()], &const_new_matrix_list[0] );
	}

	
	// transition from the last affected slice to the fixed (unchanged) slice:
	int last_B_index = new_beads.size()-2; // index in newbeads etc. of the last bead that has been changed
	
	if( end_kind == 3 )
	{ // transition from main slice to ancilla slice A
		double dscale = config->params.t1;
		diffusion_tools[ my_species ].change_matrix( &(*config).beadlist[new_beads[last_B_index].get_time()], &(*config).beadlist_A[new_beads[last_B_index+1].get_time()], config->next_id_list[new_beads[last_B_index].get_time()], config->next_id_list_A[new_beads[last_B_index+1].get_time()], dscale, number_list[last_B_index], -1, &new_beads[last_B_index], &new_beads[1+last_B_index], &(*config).matrix_list[new_beads[last_B_index].get_time()], &const_new_matrix_list[last_B_index] );
	}
	else if( end_kind == 2 )
	{ // transition from ancilla slice B to main slice
		double dscale = 2.0*config->params.t0;
		diffusion_tools[ my_species ].change_matrix( &(*config).beadlist_B[new_beads[last_B_index].get_time()], &(*config).beadlist[new_beads[last_B_index+1].get_time()], config->next_id_list_B[new_beads[last_B_index].get_time()], config->next_id_list[new_beads[last_B_index+1].get_time()], dscale, number_list[last_B_index], -1, &new_beads[last_B_index], &new_beads[1+last_B_index], &(*config).matrix_list_B[new_beads[last_B_index].get_time()], &const_new_matrix_list[last_B_index] );
	}
	else
	{ // transition from ancilla slice A to B
		double dscale = config->params.t1;
		diffusion_tools[ my_species ].change_matrix( &(*config).beadlist_A[new_beads[last_B_index].get_time()], &(*config).beadlist_B[new_beads[last_B_index+1].get_time()], config->next_id_list_A[new_beads[last_B_index].get_time()], config->next_id_list_B[new_beads[last_B_index+1].get_time()], dscale, number_list[last_B_index], -1, &new_beads[last_B_index], &new_beads[1+last_B_index], &(*config).matrix_list_A[new_beads[last_B_index].get_time()], &const_new_matrix_list[last_B_index] );
	}
	
	
	double new_sign = config->my_sign; // initialize the new sign
	double determinant_ratio = 1.0; // product of all determinant ratios from the affected slices	
	double selection_ratio = 1.0; // bead-by-bead!
	
	cnt = kind;
	for(int i=0;i<m+1;i++) // loop over all affected slices:
	{

		int kinetic_time = new_beads[i].get_time(); // propagator of the changed diffusion matrix
		int kinetic_time_end = new_beads[1+i].get_time(); // propagator of the end slice of this particular diffusion matrix
		
		double scale; // Scale factor for diffusion matrix element
		Matrix *mp; // Pointer to the old diffusion matrix

		std::vector<int>* start_blist; // beadlist of the start slice of the diffusion matrix
		std::vector<int>* end_blist; // ... end slice ...
		
		int next_start; // id of missing bead on start slice of diffusion matrix
		int next_end; // ... end slice ...

		
		if( cnt == 1 )
		{ // ancilla slice A
			scale = config->params.t1;
			mp = &(*config).matrix_list_A[ kinetic_time ];
			
			start_blist = &(*config).beadlist_A[ kinetic_time ];
			end_blist = &(*config).beadlist_B[ kinetic_time_end ];
			next_start = config->next_id_list_A[ kinetic_time ];
			next_end = config->next_id_list_B[ kinetic_time_end ];
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			scale = 2.0*config->params.t0;
			mp = &(*config).matrix_list_B[ kinetic_time ];
			
			start_blist = &(*config).beadlist_B[ kinetic_time ];
			end_blist = &(*config).beadlist[ kinetic_time_end ];
			next_start = config->next_id_list_B[ kinetic_time ];
			next_end = config->next_id_list[ kinetic_time_end ];
		}
		else
		{ // main slice
			scale = config->params.t1;
			cnt = 0;
			mp = &(*config).matrix_list[ kinetic_time ];
			
			start_blist = &(*config).beadlist[ kinetic_time ];
			end_blist = &(*config).beadlist_A[ kinetic_time_end ];
			next_start = config->next_id_list[ kinetic_time ];
			next_end = config->next_id_list_A[ kinetic_time_end ];
		}
		
		
		if( ( i>0 ) && ( i<m ) ) // the new matrices for the first and last affected slice have already been calculated!
		{
			diffusion_tools[ my_species ].change_matrix( start_blist, end_blist, next_start, next_end, scale, number_list[i], number_list[1+i], &new_beads[i], &new_beads[1+i], mp, &const_new_matrix_list[i] );
		}
		
		// Update signs and determinants:
		const_new_sign_list[i] = 1.0;
		const_new_determinant_list[i] = const_new_matrix_list[i].determinant();
		if(const_new_determinant_list[i] < 0.0)
		{
			const_new_determinant_list[i] = -1.0*const_new_determinant_list[i];
			const_new_sign_list[i] = -1.0;
		}

		if( cnt == 1 )
		{ // ancilla slice A
			new_sign *= const_new_sign_list[i] * config->sign_list_A[ kinetic_time ];
			determinant_ratio *= const_new_determinant_list[i] / config->determinant_list_A[ kinetic_time ];
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			new_sign *= const_new_sign_list[i] * config->sign_list_B[ kinetic_time ];
			determinant_ratio *= const_new_determinant_list[i] / config->determinant_list_B[ kinetic_time ];
		}
		else{ // main slice
			new_sign *= const_new_sign_list[i] * config->sign_list[ kinetic_time ];
			determinant_ratio *= const_new_determinant_list[i] / config->determinant_list[ kinetic_time ];
		}

		cnt++; // update the slice-kind counter
		
		
		
		bool special = true; // special indicates end of trajectory
		if( i < m ) special = false;
		
		// update the backwards selection prob:
		double my_prob = selection_tools[ my_species ].get_selection_prob_reverse(end_blist, id_list[1+i], next_end, &new_beads[i], &new_beads[i+1], scale, special );

		selection_ratio = selection_ratio * my_prob / forward_selection_vector[i]; // bead-by-bead ratio of selection probabilities (detailed balance)

	} // end loop i ( all affectes slices )
	
	
	// Obtain the propagator time step from the parameters class:
	double epsilon = config->params.epsilon;
	
	// Calculate the exponential function for the acceptance ratio:
	double new_exp = exp( epsilon*( delta_u + epsilon*epsilon*config->params.u0*delta_f_sq ) );
	
	// Calculate the acceptance ratio:
	double ratio = determinant_ratio * new_exp * sampling_ratio * selection_ratio;


	// Obtain a random number to decide the acceptance
	double choice = rdm();
	
	
	if(debug) // verbose:
	{
		std::cout << "universal_Deform, ratio: " << ratio << "\tchoice: " << choice << "\tdelta_u: " << delta_u << "\tnew_exp: " << new_exp << "\n";
		std::cout << "sampling_ratio: " << sampling_ratio << "\tselection_ratio: " << selection_ratio << "\tdeterminant_ratio: " << determinant_ratio << "\n";
		std::cout << "delta_f_sq: " << delta_f_sq << "\n";
	}
	
	
	// Check if the update is accepted:
	if( choice <= ratio )
	{
		// Updated forces, interaction, etc.:
		config->my_sign = new_sign;
		ensemble->total_force_sq = ensemble->total_force_sq - delta_f_sq;
		ensemble->total_energy = ensemble->total_energy - delta_u;
		
		// Copy all the information about diffusion matrices etc.:
		int cnt = kind;
		for(int i=0;i<(m+1);i++) // loop over all affected time slices:
		{
			int ktime = new_beads[i].get_time(); // propagator of this particular diffusion matrix
			
			if( cnt == 1 )
			{ // ancilla slice A
				config->matrix_list_A[ktime].copy( &const_new_matrix_list[i] );
				config->determinant_list_A[ktime] = const_new_determinant_list[i];
				config->sign_list_A[ktime] = const_new_sign_list[i];
			}
			else if( cnt == 2 )
			{ // ancilla slice B
				config->matrix_list_B[ktime].copy( &const_new_matrix_list[i] );
				config->determinant_list_B[ktime] = const_new_determinant_list[i];
				config->sign_list_B[ktime] = const_new_sign_list[i];
			}
			else{ // main slice
				config->matrix_list[ktime].copy( &const_new_matrix_list[i] );
				config->determinant_list[ktime] = const_new_determinant_list[i];
				config->sign_list[ktime] = const_new_sign_list[i];
				cnt = 0;
			}

			cnt++; // update the slice-kind counter
		}
		

		// Update the beads and all the forces:
		cnt = kind;
		for(int i=0;i<m;i++)
		{
			if(cnt==3) cnt = 0;
			cnt++;
			
			int ktime = new_beads[1+i].get_time(); // propagator to be updated
			int update_id = id_list[1+i]; // id in beads[..] which has been changed during the update
			
			// Update the forces of all beads on the current time slice:
			if( cnt == 1 )
			{ // ancilla slice A
			
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 1,&(*config).beadlist_A[ktime], config->next_id_list_A[ktime], update_id, &const_store_the_force[i], &const_store_the_species[i] );
			}
			else if( cnt == 2 )
			{ // ancilla slice B
				
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 2,&(*config).beadlist_B[ktime], config->next_id_list_B[ktime], update_id, &const_store_the_force[i], &const_store_the_species[i] );
			}
			else
			{ // main slice;
				
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 3,&(*config).beadlist[ktime], config->next_id_list[ktime], update_id, &const_store_the_force[i], &const_store_the_species[i] );
			}
			
			// Update the changed bead, update_id
			config->beads[ id_list[i+1] ].copy( &new_beads[i+1] );
		}
		
		
		if(debug) // verbose and debug :
		{
			std::cout << "***********************************************************************************\n";
			std::cout << "iSpecies: " << my_species << "\n";
			double pre_energy = (*ensemble).total_energy;
			double pre_force = (*ensemble).total_force_sq;
			
			double calc_energy = inter_tools[ my_species ].get_ensemble_energy( ensemble );
			double calc_force_sq = force_tools[ my_species ].get_total_force_sq( ensemble );
			
			std::cout << "pre_energy: " << pre_energy << "\t calc_energy: " << calc_energy << "\n";
			std::cout << "pre_force: " << pre_force << "\t calc_force_sq: " << calc_force_sq << "\n";
			
			if( fabs( (pre_energy - calc_energy)/calc_energy ) > 1e-6 )
			{
				std::cout << "mismatch in energy: " << fabs( (pre_energy - calc_energy)/calc_energy ) << "\n";
				exit(0);
			}
		}

		return 1;
	}
	else 
	{
		return 0; // The update has been rejected!
	}
}

/*
* Contains the class update_PBPIMC, update_probs
* 
* ### Contains code that:
* ### -> initializes updates
* ### -> initializes Monte-Carlo structure (config, ensemble, etc.)
* ### -> executed updates
* ### -> keeps track of updates (acceptance probs, etc)
* ### -> gives status message
* ### -> performs checks of updates
* 
* 
* ### Update slots:
* 
* DIAG:
* - 0) Universal_deform (PB-PIMC)
* - 1) Open (PB-PIMC)
* - 2) Empty!
* - 3) Empty!
* - 4) Empty!
* - 5) standard_PIMC_deform
* - 6) standard_PIMC_insert
* - 7) standard_PIMC_open
* - 8) standard_PIMC_move
* - 9) Empty!
* - 10) standard_PIMC_offdiag_open (for the momentum distribution)
* - 11) standard_PIMC_deform, but for protons
* - 12) standard_PIMC_move, but for protons
* - 13) standard_PIMC_offdiag_open, but for protons
* - 14) standard_PIMC_multi_move

* 
* 
* OFFDIAG:
* - 0)  Deform (PB-PIMC)
* - 1)  Close (PB-PIMC)
* - 2)  Empty!
* - 3)  Empty!
* - 4)  Swap (PB-PIMC)
* - 5)  standard_PIMC_remove
* - 6)  standard_PIMC_advance
* - 7)  standard_PIMC_recede
* - 8)  standard_PIMC_deform
* - 9)  standard_PIMC_close
* - 10) standard_PIMC_swap
* - 11) standard_PIMC_move
* - 12) standard_PIMC_wriggle (for the momentum distribution)
* - 13) standard_PIMC_offdiag_close (for the momentum distribution)
* - 14) standard_PIMC_deform, but for protons
* - 15) standard_PIMC_move, but for protons
* - 16) standard_PIMC_offdiag_close, but for protons
* - 17) standard_PIMC_wriggle, but for protons
* - 18) standard_PIMC_multi_move
* 
*/

class update_probs
{
public:
	
	double P_PBPIMC_open;
	double P_PBPIMC_close;
	double P_PBPIMC_deform_offdiag;
	double P_PBPIMC_deform;
	double P_PBPIMC_swap;

	
	double P_standard_PIMC_open;
	double P_standard_PIMC_close;
	double P_standard_PIMC_recede;
	double P_standard_PIMC_advance;
	double P_standard_PIMC_swap;
	double P_standard_PIMC_insert;
	double P_standard_PIMC_remove;
	double P_standard_PIMC_deform;
	double P_standard_PIMC_deform_offdiag;
	double P_standard_PIMC_move;
	double P_standard_PIMC_move_offdiag;
	double P_standard_PIMC_offdiag_open;
	double P_standard_PIMC_offdiag_close;
	double P_standard_PIMC_wriggle;
	
	double P_standard_PIMC_deform_proton;
	double P_standard_PIMC_deform_offdiag_proton;
	
	double P_standard_PIMC_move_proton;
	double P_standard_PIMC_move_offdiag_proton;
	
	double P_standard_PIMC_offdiag_open_proton;
	double P_standard_PIMC_offdiag_close_proton;
	double P_standard_PIMC_wriggle_proton;
	
	
	
	double P_standard_PIMC_multi_move;
	double P_standard_PIMC_multi_move_offdiag;
	
	
	
	
	double m_max_deform_factor;
	double m_max_open_factor;
};

const int n_diag = 15; // number of digaonal MC moves
const int n_offd = 19; // """" offd. """"""""


template <class inter> class update_PBPIMC
{
	
public:
	
	// Initialize the worm_configuration pointer, normalize update probs, etc.:
	void init( update_probs user_probs, worm_configuration* new_config, Parameters* p, config_ensemble* new_ensemble );
	
	// Perform an update:
	void update();
	
	// Give a status message with the acceptance ratios of the updates to the terminal:
	void status_message();
	
	// Make a check of energy etc. and re-set them:
	void make_check();
	
	void make_check_pbpimc();

	void make_check_standard_PIMC(); // ### standard PIMC for a single species, atm not used!
	void make_check_ensemble_standard_PIMC();

	
private:
	
	std::vector<int> n_accept_diag, n_try_diag, n_accept_offd, n_try_offd; // stores information about acceptance ratios of the updates
	std::vector<double> prob_diag, prob_offd; // list of update probabilities in the open and closed sector
	
	bool debug;
	
	// Control the ratio of G and Z sector
	double c_bar;
	
	// Maximum numbers of beads to be changed by updates of type open and deform:
	int m_max_open;
	int m_max_deform;
	

	int n_equil;


	
	// Counters for G- and Z-sector:
	long int z_cnt = 0;
	long int g_cnt = 0;
	
	
	// Instances of the PB-PIMC updates:


	update_pbpimc_open< inter > open;
	update_pbpimc_close< inter > close;
	update_pbpimc_swap< inter > swap;
	update_pbpimc_deform_universal< inter > universal_deform;
	
	


	

	// standard PIMC updates:
	update_standard_PIMC_deform< inter > standard_PIMC_deform;
	
	update_standard_PIMC_insert< inter > standard_PIMC_insert;
	update_standard_PIMC_remove< inter > standard_PIMC_remove;
	
	update_standard_PIMC_advance< inter > standard_PIMC_advance;
	update_standard_PIMC_recede< inter > standard_PIMC_recede;
	
	update_standard_PIMC_open< inter > standard_PIMC_open;
	update_standard_PIMC_close< inter > standard_PIMC_close;
	
	update_standard_PIMC_swap< inter > standard_PIMC_swap;
	
	update_standard_PIMC_move< inter > standard_PIMC_move;
	
	update_standard_PIMC_offdiag_open< inter > standard_PIMC_offdiag_open;
	update_standard_PIMC_offdiag_close< inter > standard_PIMC_offdiag_close;
	update_standard_PIMC_wriggle< inter > standard_PIMC_wriggle;
	
	update_standard_PIMC_multi_move< inter > standard_PIMC_multi_move;
	
	


	
	
	
	worm_configuration* config; // pointer to the configuration to be updated, for old code or canonical boltzmannon simulations with standard PIMC
	
	// Ensemble of configs to be updated:
	config_ensemble* ensemble; // contains worm_configs for each particle species
	


	
	// Instances of toolboxes:
	force_toolbox< inter > force_tools;
	interaction_toolbox< inter > inter_tools;
	diffusion_toolbox< inter > diffusion_tools;
	pair_action_toolbox< inter > pair_tools;
	
	
// 	// Create an instance of the diffusion toolbox for each species:
 	std::vector< diffusion_toolbox <inter> > diffusion_species;
	

	// Normalize the acceptance probabilities:
	void normalize_probs();
	

	void equilibrate(int n_equil);
	
	
	const double tol = 1e-7; // tolerance in the check procedures
	
	// constantly allocated matrix for the make_check() method (for each species)
	std::vector<Matrix> debug_matrix_vector;
	
	int N_boltz; // number of boltzmannons for the two_component system
	int n_multi; // number of simulation types with multiple trajectories (PB-PIMC, mPB-PIMC, stochastic blocking)
	int simulation_type; // type of simulation (e.g., standard PIMC or PB-PIMC?)
	
};



// Make a check of the current Metropolis Monte Carlo configuration:
template <class inter> void update_PBPIMC<inter>::make_check()
{
	// Make_a_check depends on the simulation_type:
	if( simulation_type >= n_multi ) // make a check for standard PIMC simulations ( boltzmannon or WA )
	{
// 		make_check_standard_PIMC(); [ this is the version without multiple species! ]
		make_check_ensemble_standard_PIMC();
	}
	else // make a check for the non-standard PIMC simulations
	{
		switch( simulation_type )
		{
			case 0: // PB-PIMC
				make_check_pbpimc();
				break;
		}
	}


}



// Check the configuration of standard PIMC:
template <class inter> void update_PBPIMC<inter>::make_check_ensemble_standard_PIMC()
{
	const double tol = 0.0001;
	
	inter my_interaction;
	my_interaction.init( &(*config).params );
	
	std::cout << "Make check for (ensemble) standard PIMC\n";
	
	if( ensemble->params.debug )
	{
		std::cout << "Printing check-config to the disk -> check_...";
		ensemble->print_standard_PIMC_ensemble_paths( "check" ); // Print the paths of all worm_configs
		ensemble->print_standard_PIMC_ensemble_beadlist( "check" ); // Print the beadlist of all worm_configs
		ensemble->print_standard_PIMC_ensemble_config( "check" );
		std::cout << " ...Done!\n";
	}

	if( ensemble->N_tot == 0 )
	{
		std::cout << "The system is empty.\n";
		return;
	}
	
	
	// If the artifical pair-exchange weight is activated, the stored number of pair-exchanges needs to be checked as well 
	if( ensemble->diag && ensemble->params.pp_control ) // makes sense only in diagonal configurations
	{
		for(auto c=ensemble->species_config.begin();c!=ensemble->species_config.end();c++) // loop over all particle species
		{
			int debug_Npp = c->obtain_Npp();
			std::cout << "--debug_Npp = " << debug_Npp << "\t stored_Npp = " << c->Npp << "\n";
			if( (debug_Npp-c->Npp) != 0 )
			{
				std::cout << "Error in Npp, debug_Npp = " << debug_Npp << "\t stored Npp = " << c->Npp << "\n";
				exit(0);
			}
		}
	}






	
	
	// Make a check of the energy:
	double debug_total_energy = inter_tools.ensemble_standard_PIMC_get_energy( ensemble );
	double diff = fabs( ( debug_total_energy - ensemble->total_energy ) / debug_total_energy );
	if( diff > tol )
	{ 
		std::cout << "Energy mismatch, debug_total_energy: " << debug_total_energy << "\t ensemble_total_energy: " << ensemble->total_energy << "\n";
		std::cout << "diff: " << diff << "\t tol: " << tol << "\n";
 		exit(0);
	}
	
	
	
	// Make a check of the pair action:
	std::vector<double> action_vector = pair_tools.get_total_pair_action( ensemble );
	double debug_total_pair_action = action_vector[0];
	double action_diff = fabs( ( debug_total_pair_action - ensemble->total_pair_action ) / (debug_total_pair_action) );
	if( ( action_diff > tol*1 ) && ( fabs(debug_total_pair_action) > tol ) )
	{
		std::cout << "Pair action mismatch, debug_total_pair_action: " << debug_total_pair_action << "\t ensemble: " << ensemble->total_pair_action << "\n";
		std::cout << "diff: " << action_diff << "\t tol: " << tol << "\n";
		exit(0);
	}
	
	std::cout << "debug_total_pair_action: " << debug_total_pair_action << "\t ensemble_pair_action: " << ensemble->total_pair_action << "\n";
	
	
	
	
	// Make a check of the pair action derivative:
	double debug_total_pair_derivative = action_vector[1];
	double derivative_diff = fabs( ( debug_total_pair_derivative - ensemble->total_pair_derivative ) / (debug_total_pair_derivative) );
	if( ( derivative_diff > tol ) && ( fabs(debug_total_pair_derivative) > tol ) )
	{
		std::cout << "Pair derivative mismatch, debug_total_pair_derivative: " << debug_total_pair_derivative << "\t ensemble: " << ensemble->total_pair_derivative << "\n";
		std::cout << "diff: " << derivative_diff << "\t tol: " << tol << "\n";
		exit(0);
	}
	
	std::cout << "debug_total_pair_derivative: " << debug_total_pair_derivative << "\t ensemble_pair_action_derivative: " << ensemble->total_pair_derivative << "\n";
	
	
	
	
	
	
	// Make a check of the m_e and m_p action derivatives:
	double debug_total_m_e_pair_derivative = action_vector[2];
	double debug_total_m_p_pair_derivative = action_vector[3];
	
	double m_e_derivative_diff = fabs( (debug_total_m_e_pair_derivative - ensemble->total_m_e_pair_derivative) / debug_total_m_e_pair_derivative );
	double m_p_derivative_diff = fabs( (debug_total_m_p_pair_derivative - ensemble->total_m_p_pair_derivative) / debug_total_m_p_pair_derivative );
	
	if( ( ( m_e_derivative_diff > tol ) && ( fabs(debug_total_m_e_pair_derivative) > tol ) ) || ( ( m_p_derivative_diff > tol ) && ( fabs(debug_total_m_p_pair_derivative) > tol ) ) )
	{
		std::cout << "Mass pair action derivative mismatch!!!!\n";
		std::cout << "debug_m_e: " << debug_total_m_e_pair_derivative << "\tensemble: " << ensemble->total_m_e_pair_derivative << "\tdiff: " << m_e_derivative_diff << "\n";
		std::cout << "debug_m_p: " << debug_total_m_p_pair_derivative << "\tensemble: " << ensemble->total_m_p_pair_derivative << "\tdiff: " << m_p_derivative_diff << "\n";
		
		
		exit(0);
		
	}
	
	std::cout << "debug_total_m_e_pair_derivative: " << debug_total_m_e_pair_derivative << "\tensemble: " << ensemble->total_m_e_pair_derivative << "\n";
	std::cout << "debug_total_m_p_pair_derivative: " << debug_total_m_p_pair_derivative << "\tensemble: " << ensemble->total_m_p_pair_derivative << "\n";
	std::cout << "tol: " << tol << "\n";
	
	
	std::cout << "m_e_derivative_diff: " << m_e_derivative_diff << "\t m_p_derivative_diff: " << m_p_derivative_diff << "\n";
	
	
	
	// Make a check of the ext_pot_action, and the corresponding derivative:
	std::vector<double> ext_pot_action_vector = pair_tools.get_total_ext_pot_action( ensemble );
	double debug_ext_pot_action = ext_pot_action_vector[0];
	double ext_pot_action_diff = fabs( ( ensemble->total_ext_pot_action - debug_ext_pot_action ) / debug_ext_pot_action );
	if( ( ext_pot_action_diff > tol ) && ( debug_ext_pot_action > tol ) )
	{
		std::cout << "Ext-pot-action mismatch, debug: " << debug_ext_pot_action << "\t ensemble: " << ensemble->total_ext_pot_action << "\n";
		std::cout << "diff: " << ext_pot_action_diff << "\ttol: " << tol << std::endl;
		exit(0);
	}
	
	std::cout << "debug_ext_pot_action: " << debug_ext_pot_action << "\t ensemble_ext_pot_action: " << ensemble->total_ext_pot_action << "\n";
	
	
	
	
	double debug_ext_pot_action_derivative = ext_pot_action_vector[1];
	double ext_pot_action_derivative_diff = fabs( ( ensemble->total_ext_pot_action_derivative - debug_ext_pot_action_derivative ) / debug_ext_pot_action_derivative );
	if( ( ext_pot_action_derivative_diff > tol ) && ( debug_ext_pot_action_derivative > tol ) )
	{
		std::cout << "Ext-pot-action-derivative mismatch, debug: " << debug_ext_pot_action_derivative << "\t ensemble: " << ensemble->total_ext_pot_action_derivative << "\n";
		std::cout << "diff: " << ext_pot_action_derivative_diff << "\ttol: " << tol << std::endl;
		exit(0);
	}
	
	std::cout << "debug_ext_pot_action_derivative: " << debug_ext_pot_action_derivative << "\t ensemble: " << ensemble->total_ext_pot_action_derivative << "\n";
	
	
	
	
	
	
	
	std::cout << "debug_total_energy: " << debug_total_energy << "\t ensemble_energy: " << ensemble->total_energy << "\n";
	ensemble->total_energy = debug_total_energy; // Reset the stored energy to avoid accumulation of precision-errors
	
	// Make a check of the total diffusion product:
	double debug_diffusion_product = ensemble->calculate_diffusion_product();
	double diffusion_diff = fabs( ( debug_diffusion_product - ensemble->total_diffusion_product ) / debug_diffusion_product );
	
	if( diffusion_diff > tol ) // ### Subject to overflows, only for debug purposes, should not be enforced!
	{
		std::cout << "diffusion_product mismatch, debug_diffusion_product: " << debug_diffusion_product << "\t total_diffusion_product: " << ensemble->total_diffusion_product << "\n";
		std::cout << "diff: " << diffusion_diff << "\t tol: " << tol << "\n";
// 		exit(0);
	}
	
	std::cout << "debug total_diffusion_product: " << debug_diffusion_product << "\t total_diffusion_product: " << ensemble->total_diffusion_product << "\n";
	ensemble->total_diffusion_product = debug_diffusion_product;


	// Make a check of the mu-exponent (chem. pot.):
	double debug_total_exponent = 0.0;
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		double iSpec_exp = inter_tools.standard_PIMC_get_exponent( &(*ensemble).species_config[ iSpecies ] );
		double iSpec_diff = fabs( ( iSpec_exp - ensemble->species_config[ iSpecies ].my_exp ) / iSpec_exp );
		if( iSpec_diff > tol )
		{
			std::cout << "Error, iSpec_diff = " << iSpec_diff << "\t tol = " << tol << "\n";
			std::cout << "iSpec_exp = " << iSpec_exp << "\t config.my_exp = " << ensemble->species_config[ iSpecies ].my_exp << "\n";
			exit(0);
		}
		ensemble->species_config[ iSpecies ].my_exp = iSpec_exp;
		debug_total_exponent += iSpec_exp;
	}
	double diff_exp = fabs( ( debug_total_exponent - ensemble->total_exponent ) / debug_total_exponent );
	
	if( diff_exp > tol )
	{
		std::cout << "Total (mu)-exponent mismatch, debug_total_exponent: " << debug_total_exponent << "\t ensemble-total-exponent: " << ensemble->total_exponent << "\n";
		std::cout << "relative-diff: " << diff_exp << "\t tol: " << tol << "\n";
 		exit(0);
	}
	
	
	std::cout << "debug_total_exponent: " << debug_total_exponent << "\t ensemble_exponent: " << ensemble->total_exponent << "\n";
	ensemble->total_exponent = debug_total_exponent; // Reset the stored exponent
	
	
	// Make a check of all diffusion matrix elements:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		worm_configuration* iConfig = &(*ensemble).species_config[ iSpecies ]; // pointer to the worm_configuration with number 'iSpecies'

		for(int iSlice=0;iSlice<iConfig->params.n_bead;iSlice++)
		{
			for(int i=0;i<iConfig->beadlist[ iSlice ].size();i++)
			{
				int i_id = iConfig->beadlist[ iSlice ][ i ];
				int i_next_id = iConfig->beads[ i_id ].get_next_id();
				int i_prev_id = iConfig->beads[ i_id ].get_prev_id();
				
				if( i_id != iConfig->tail_id )
				{
					if( i_id != iConfig->beads[ i_prev_id ].get_next_id() )
					{
						std::cout << "Error, mismatch in next / prev\n";
						exit(0);
					}
				}
				
				if( i_id != iConfig->head_id )
				{
					if( i_id != iConfig->beads[ i_next_id ].get_prev_id() )
					{
						std::cout << "Error, mismatch in prev / next\n";
						exit(0);
					}
				}
				
				if( i_next_id >= 0 ) // The head does not have a next_id and a diffusion_element 
				{
				
					double debug_element = my_interaction.rho( &(*iConfig).beads[ i_id ], &(*iConfig).beads[ i_next_id ], 1.0 );
					double i_element = (*iConfig).beads[ i_id ].get_diffusion_element();
								
					double i_diff = fabs( (debug_element-i_element) / debug_element );
					if( i_diff > tol )
					{
						std::cout << "Mismatch in diffusion element, iSlice: " << iSlice << "\t i: " << i << "\t i_id: " << i_id << "\t i_next_id: " << i_next_id << "\n";
						std::cout << "iSpecies: " << iSpecies << "\n";
						std::cout << "debug_element: " << debug_element << "\t i_element: " << i_element << "\t relative diff: " << i_diff << "\n";
						exit(0);
					}
				}
				
			} // end loop i
		} // end loop iSlice
	
	
	} // end loop iSpecies
	
	std::cout << "make_check_standard_PIMC(ensemble) was successful!\n";
}



// Check the configuration of a standard PIMC simulation (single species, not used anymore!)
// ### Not updated!
template <class inter> void update_PBPIMC<inter>::make_check_standard_PIMC()
{
	inter my_interaction;
	my_interaction.init( &(*config).params );
	
	std::cout << "Make check for standard PIMC\n";
	
	
	if( ensemble->N_tot == 0 )
	{
		std::cout << "The system is empty.\n";
		return;
	}

	std::cout << "Print configuration into file 'standard_PIMC_check_paths.dat'\n";
	config->standard_PIMC_print_paths( "standard_PIMC_check_paths.dat" );
	config->standard_PIMC_print_beadlist( "standard_PIMC_check_beadlist.dat" );
	

	// Make a check of the energy:
	double debug_energy = inter_tools.standard_PIMC_get_energy( config );
	double diff = fabs( (debug_energy - (*config).my_energy)/debug_energy );
	
	if( diff > tol )
	{
		std::cout << "ensemble.N_tot: " << ensemble->N_tot << "\n";
		std::cout << "Energy mismatch, debug_energy: " << debug_energy << "\tmy_energy: " << (*config).my_energy << "\n";
		std::cout << "relative diff: " << diff << "\n";
		
		exit(0);
	}
	
	std::cout << "--my_energy: " << config->my_energy << "\t debug_energy: " << debug_energy << "\n";
	
	// To avoid accumulation of numerical errors, reset stored energy to the new calculated value:
	config->my_energy = debug_energy;
	
	
	
	// Make a check of the mu-exponent
	double debug_exponent = inter_tools.standard_PIMC_get_exponent( config );
	double exp_diff = fabs( (debug_exponent - config->my_exp)/debug_exponent );
	if( diff > tol )
	{
		std::cout << "Mu-exponent mismatch, exp_diff: " << exp_diff << "\t debug_exponent: " << debug_exponent << "\tmy_exp: " << config->my_exp << "\n";
		std::cout << "diag: " << config->diag << "\t head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\n";
		
		exit(0);
	}
	
	
	std::cout << "--my_exp: " << config->my_exp << "\t debug_exp: " << debug_exponent << "\n";
	
	// reset the stored mu-exponent:
	config->my_exp = debug_exponent;
	
	
	// Make a check of all diffusion matrix elements
	for(int iSlice=0;iSlice<config->params.n_bead;iSlice++)
	{
		for(int i=0;i<config->beadlist[ iSlice ].size();i++)
		{
			int i_id = (*config).beadlist[ iSlice ][ i ];
			int i_next_id = (*config).beads[ i_id ].get_next_id();
			
			if( i_next_id >= 0 ) // The head does not posses a next_id and a diffusion_element 
			{
				int spec_diff = config->beads[ i_id ].get_species() - config->beads[ i_next_id ].get_species();
				if( spec_diff > 0 )
				{
					std::cout << "Mismatch in particle species, iSlice: " << iSlice << "\t i: " << i << "\t i_id: " << i_id << "\t i_next_id: " << i_next_id << "\n";
					std::cout << "spec_diff: " << spec_diff << "\t i_spec: " << config->beads[ i_id ].get_species() << "\t i_next_spec: " << config->beads[ i_next_id ].get_species() << "\n";
					exit(0);
				}
			
				double debug_element = my_interaction.rho( &(*config).beads[ i_id ], &(*config).beads[ i_next_id ], 1.0 );
				double i_element = config->beads[ i_id ].get_diffusion_element();
							
				double i_diff = fabs( (debug_element-i_element) / debug_element );
				if( i_diff > tol )
				{
					std::cout << "Mismatch in diffusion element, iSlice: " << iSlice << "\t i: " << i << "\t i_id: " << i_id << "\t i_next_id: " << i_next_id << "\n";
					std::cout << "debug_element: " << debug_element << "\t i_element: " << i_element << "\t relative diff: " << i_diff << "\n";
					exit(0);
				}
				
			}
			
			
		} // end loop i
	} // end loop iSlice
	
	
	std::cout << "make_check_standard_PIMC was successful!\n";
}



// Check a configuration from a PB-PIMC simulation:
template <class inter> void update_PBPIMC<inter>::make_check_pbpimc()
{

	
	std::cout << "Making a check of energy etc. \n";
	double diff;
	
	// Obtain the energy:
	double debug_energy = inter_tools.get_ensemble_energy( ensemble );
	
	diff = fabs( ( debug_energy - ensemble->total_energy ) / debug_energy );
	if( diff > tol )
	{
		std::cout << "Energy mismatch, debug_energy: " << debug_energy << "\tmy_energy: " << ensemble->total_energy << "\n";
		std::cout << "relative diff: " << diff << "\n";
		exit(0);
	}
	
	
	// Obtain the force_sq:
	double debug_force_sq = force_tools.get_total_force_sq( ensemble );
	diff = fabs( ( debug_force_sq - ensemble->total_force_sq ) / debug_force_sq );
	if( diff > tol )
	{
		std::cout << "Force_sq mismatch, debug_force_sq: " << debug_force_sq << "\tmy_force_sq: " << ensemble->total_force_sq << "\n";
		std::cout << "relative diff: " << diff << "\n";
		exit(0);
	}
	
	
	/* TBD Implement exponent 
	// Obtain the exponent
	double debug_exponent = inter_tools.get_exponent_offd(debug_energy, debug_force_sq);
	diff = fabs( (debug_exponent - (*config).my_exp) / debug_exponent );
	if( diff > tol )
	{
		std::cout << "Exponent mismatch, debug_exponent: " << debug_exponent << "\tmy_exp: " << (*config).my_exp << "\n";
		std::cout << "relative diff: " << diff << "\n";
		exit(0);
	}
	*/
	
	std::cout << "my_energy: " << debug_energy << "\t my_force_sq: " << debug_force_sq << "\t total_sign: " << (*ensemble).get_sign() << "\n";

	
	double debug_total_sign = 1.0;
	double ensemble_total_sign = (*ensemble).get_sign();
	
	// Obtain the diffusion matrices, determinants and signs of every species:
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		double debug_sign = 1.0;
		worm_configuration *config = &(*ensemble).species_config[iSpecies];
		debug_total_sign *= config->my_sign;
		
		for(int iSlice=0;iSlice<ensemble->params.n_bead;iSlice++) // loop over all slices
		{
			double scale = ensemble->params.t1;

			/*
			if( iSlice == 0 )
			{
				std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
				std::cout << "DIFFUSION_CHECK species: " << iSpecies <<"\tsign: " << (*config).my_sign << "\n";
				(*config).matrix_list[iSlice].print_to_screen();
			}
			*/

			// Obtain main matrix:
			diffusion_species[iSpecies].get_matrix( &debug_matrix_vector[iSpecies], &(*config).beadlist[iSlice], &(*config).beadlist_A[iSlice], config->next_id_list[iSlice], config->next_id_list_A[iSlice], scale );
			bool main_check = debug_matrix_vector[iSpecies].equal( &(*config).matrix_list[iSlice] );
		
			
			config->matrix_list[iSlice].copy( &debug_matrix_vector[iSpecies] );
		
			double main_determinant = debug_matrix_vector[iSpecies].determinant();
			double main_sign = 1.0;
			if( main_determinant < 0.0 ) main_sign = -1.0;
			debug_sign *= main_sign;
			
			
			main_determinant = fabs( main_determinant );
			diff = fabs( (main_determinant - config->determinant_list[iSlice]) / main_determinant );
			if( diff > tol )
			{
				std::cout << "iSlice: " << iSlice << "\n";
				std::cout << "determinant mismatch, main_determinant: " << main_determinant << "\tconfig: " << (*config).determinant_list[iSlice] << "\n";
				std::cout << "iSpecies: " << iSpecies << "\n";
				exit(0);
			}
		
		
			// Obtain A matrix:
			diffusion_species[iSpecies].get_matrix( &debug_matrix_vector[iSpecies], &(*config).beadlist_A[iSlice], &(*config).beadlist_B[iSlice], config->next_id_list_A[iSlice], config->next_id_list_B[iSlice], scale );
			bool A_check = debug_matrix_vector[iSpecies].equal( &(*config).matrix_list_A[iSlice] );
			
			config->matrix_list_A[iSlice].copy( &debug_matrix_vector[iSpecies] );
			
			double A_determinant = debug_matrix_vector[iSpecies].determinant();
			double A_sign = 1.0;
			if( A_determinant < 0.0 ) A_sign = -1.0;
			debug_sign *= A_sign;
			
			A_determinant = fabs( A_determinant );
			diff = fabs( (A_determinant - (*config).determinant_list_A[iSlice]) / A_determinant );
			if( diff > tol )
			{
				std::cout << "iSlice: " << iSlice << "\n";
				std::cout << "determinant mismatch, A_determinant: " << A_determinant << "\tconifg: " << config->determinant_list_A[iSlice] << "\n";
				std::cout << "iSpecies: " << iSpecies << "\n";
				exit(0);
			}
			
			
			// Obtain B matrix:
			scale = 2.0*config->params.t0;
			int plus = iSlice + 1;
			if( plus == config->params.n_bead ) plus = 0;
			
			diffusion_species[iSpecies].get_matrix( &debug_matrix_vector[iSpecies], &(*config).beadlist_B[iSlice], &(*config).beadlist[plus], config->next_id_list_B[iSlice], config->next_id_list[plus], scale );
			bool B_check = debug_matrix_vector[iSpecies].equal( &(*config).matrix_list_B[iSlice] );
			
			config->matrix_list_B[iSlice].copy( &debug_matrix_vector[iSpecies] );
			
			double B_determinant = debug_matrix_vector[iSpecies].determinant();
			double B_sign = 1.0;
			if( B_determinant < 0.0 ) B_sign = -1.0;
			debug_sign *= B_sign;
			
			B_determinant = fabs( B_determinant );
			diff = fabs( (B_determinant - (*config).determinant_list_B[iSlice]) / B_determinant );
			if( diff > tol )
			{
				std::cout << "iSlice: " << iSlice << "\n";
				std::cout << "determinant mismatch, B_determinant: " << B_determinant << "\tconifg: " << (*config).determinant_list_B[iSlice] << "\n";
				std::cout << "iSpecies: " << iSpecies << "\n";
				exit(0);
			}
			
			if( !main_check ) 
			{
				std::cout << "mismatch in diffusion matrix main.\n";
				std::cout << "iSpecies: " << iSpecies << "\n";
				exit(0);
			}
			
			if( !A_check ) 
			{
				std::cout << "mismatch in diffusion matrix A.\n";
				std::cout << "iSpecies: " << iSpecies << "\n";
				exit(0);
			}
			
			if( !B_check ) 
			{
				std::cout << "mismatch in diffusion matrix B.\n";
				std::cout << "iSpecies: " << iSpecies << "\n";
				exit(0);
			}

		} // end loop over all slices
		
		
		// Compare debug sign with my_sign:
		diff = fabs( config->my_sign - debug_sign );
		if( diff > 0.1 )
		{
			std::cout << "Sign mismatch, debug_sign: " << debug_sign << "\t my_sign: " << config->my_sign << "\n";
			std::cout << "iSpecies: " << iSpecies << "\n";
			exit(0);
		}
		
	} // end loop over all species
	
	// Compare debug sign with my_sign:
	double sdiff = fabs( ensemble_total_sign - debug_total_sign );
	if( sdiff > 0.1 )
	{
		std::cout << "Sign mismatch, debug_sign: " << debug_total_sign << "\t ensemble_total_sign: " << ensemble_total_sign << "\n";
		exit(0);
	}
	
	// Reset energy and force to avoid accumulation of errors:
	ensemble->total_force_sq = debug_force_sq;
	ensemble->total_energy = debug_energy;
	
}











// ### ATM only doing the equilibration steps!!!
template <class inter> void update_PBPIMC<inter>::equilibrate( int n_equil )
{

	// Start by performing n_equil updates before doing anything to let the system 'equilibrate'
	std::cout << "Performing n_equil = " << n_equil << " equilibration steps\n";

	
	for(int i=0;i<n_equil;i++)
	{
		update();
	}

	
	
	// Reset the update acceptance ratio book-keeping:
	for(auto c=n_accept_diag.begin();c!=n_accept_diag.end();c++) (*c) = 0;
	for(auto c=n_accept_offd.begin();c!=n_accept_offd.end();c++) (*c) = 0;
	for(auto c=n_try_diag.begin();c!=n_try_diag.end();c++) (*c) = 0;
	for(auto c=n_try_offd.begin();c!=n_try_offd.end();c++) (*c) = 0;
}





// Give a status message to the terminal:
template <class inter> void update_PBPIMC<inter>::status_message()
{

	int cpu_num = sched_getcpu();
	
	std::cout << "________________________________________________________\n";
	std::cout << "Which core am I? -- " << cpu_num << "\n";
	std::cout << "------------------------------------------------------------------\n";
	
	if( n_try_diag[0] > 0 ) std::cout << "Deform(Z):\t" << n_accept_diag[0] << " of " << n_try_diag[0] << " ( " << 100.0* double( n_accept_diag[0] ) / double( n_try_diag[0] ) << " %)\n";
	if( n_try_diag[1] > 0 ) std::cout << "Open(Z):\t" << n_accept_diag[1] << " of " << n_try_diag[1] << " ( " << 100.0* double( n_accept_diag[1] ) / double( n_try_diag[1] ) << " %)\n";
	



	
	if( n_try_diag[5] > 0 ) std::cout << "std-PIMC Deform(Z):\t" << n_accept_diag[5] << " of " << n_try_diag[5] << " ( " << 100.0* double( n_accept_diag[5] ) / double( n_try_diag[5] ) << " %)\n";
	if( n_try_diag[6] > 0 ) std::cout << "std-PIMC Insert(Z):\t" << n_accept_diag[6] << " of " << n_try_diag[6] << " ( " << 100.0* double( n_accept_diag[6] ) / double( n_try_diag[6] ) << " %)\n";
	if( n_try_diag[7] > 0 ) std::cout << "std-PIMC Open(Z):\t" << n_accept_diag[7] << " of " << n_try_diag[7] << " ( " << 100.0* double( n_accept_diag[7] ) / double( n_try_diag[7] ) << " %)\n";
	if( n_try_diag[8] > 0 ) std::cout << "std-PIMC Move(Z):\t" << n_accept_diag[8] << " of " << n_try_diag[8] << " ( " << 100.0* double( n_accept_diag[8] ) / double( n_try_diag[8] ) << " %)\n";
	
	


	
	if( n_try_offd[0] > 0 ) std::cout << "Deform(G):\t" << n_accept_offd[0] << " of " << n_try_offd[0] << " ( " << 100.0* double( n_accept_offd[0] ) / double( n_try_offd[0] ) << " %)\n";
	if( n_try_offd[1] > 0 ) std::cout << "Close(G):\t" << n_accept_offd[1] << " of " << n_try_offd[1] << " ( " << 100.0* double( n_accept_offd[1] ) / double( n_try_offd[1] ) << " %)\n";







	if( n_try_offd[4] > 0 ) std::cout << "Swap(G):\t" << n_accept_offd[4] << " of " << n_try_offd[4] << " ( " << 100.0* double( n_accept_offd[4] ) / double( n_try_offd[4] ) << " %)\n";
	
	if( n_try_offd[5] > 0 ) std::cout << "std-PIMC Remove(G):\t" << n_accept_offd[5] << " of " << n_try_offd[5] << " ( " << 100.0* double( n_accept_offd[5] ) / double( n_try_offd[5] ) << " %)\n";
	if( n_try_offd[6] > 0 ) std::cout << "std-PIMC Advance(G):\t" << n_accept_offd[6] << " of " << n_try_offd[6] << " ( " << 100.0* double( n_accept_offd[6] ) / double( n_try_offd[6] ) << " %)\n";
	if( n_try_offd[7] > 0 ) std::cout << "std-PIMC Recede(G):\t" << n_accept_offd[7] << " of " << n_try_offd[7] << " ( " << 100.0* double( n_accept_offd[7] ) / double( n_try_offd[7] ) << " %)\n";
	if( n_try_offd[8] > 0 ) std::cout << "std-PIMC Deform(G):\t" << n_accept_offd[8] << " of " << n_try_offd[8] << " ( " << 100.0* double( n_accept_offd[8] ) / double( n_try_offd[8] ) << " %)\n";
	if( n_try_offd[9] > 0 ) std::cout << "std-PIMC Close(G):\t" << n_accept_offd[9] << " of " << n_try_offd[9] << " ( " << 100.0* double( n_accept_offd[9] ) / double( n_try_offd[9] ) << " %)\n";
	if( n_try_offd[10] > 0 ) std::cout << "std-PIMC Swap(G):\t" << n_accept_offd[10] << " of " << n_try_offd[10] << " ( " << 100.0* double( n_accept_offd[10] ) / double( n_try_offd[10] ) << " %)\n";
	if( n_try_offd[11] > 0 ) std::cout << "std-PIMC Move(G):\t" << n_accept_offd[11] << " of " << n_try_offd[11] << " ( " << 100.0* double( n_accept_offd[11] ) / double( n_try_offd[11] ) << " %)\n";
	
	if( n_try_offd[12] > 0 ) std::cout << "std-PIMC Wriggle(G):\t" << n_accept_offd[12] << " of " << n_try_offd[12] << " ( " << 100.0* double( n_accept_offd[12] ) / double( n_try_offd[12] ) << " %)\n";

	if( n_try_offd[13] > 0 ) std::cout << "std-PIMC offd_close(G):\t" << n_accept_offd[13] << " of " << n_try_offd[13] << " ( " << 100.0* double( n_accept_offd[13] ) / double( n_try_offd[13] ) << " %)\n";
	if( n_try_diag[10] > 0 ) std::cout << "std-PIMC offd_open(Z):\t" << n_accept_diag[10] << " of " << n_try_diag[10] << " ( " << 100.0* double( n_accept_diag[10] ) / double( n_try_diag[10] ) << " %)\n";
	
	
	if( n_try_diag[11] > 0 ) std::cout << "std-PIMC DeformProton(Z):\t" << n_accept_diag[11] << " of " << n_try_diag[11] << " ( " << 100.0* double( n_accept_diag[11] ) / double( n_try_diag[11] ) << " %)\n";
	if( n_try_offd[14] > 0 ) std::cout << "std-PIMC DeformProton(G):\t" << n_accept_offd[14] << " of " << n_try_offd[14] << " ( " << 100.0* double( n_accept_offd[14] ) / double( n_try_offd[14] ) << " %)\n";
	
	if( n_try_diag[12] > 0 ) std::cout << "std-PIMC MoveProton(Z):\t" << n_accept_diag[12] << " of " << n_try_diag[12] << " ( " << 100.0* double( n_accept_diag[12] ) / double( n_try_diag[12] ) << " %)\n";
	if( n_try_offd[15] > 0 ) std::cout << "std-PIMC MoveProton(G):\t" << n_accept_offd[15] << " of " << n_try_offd[15] << " ( " << 100.0* double( n_accept_offd[15] ) / double( n_try_offd[15] ) << " %)\n";

	if( n_try_diag[14] > 0 ) std::cout << "std-PIMC MultiMove(Z):\t" << n_accept_diag[14] << " of " << n_try_diag[14] << " ( " << 100.0* double( n_accept_diag[14] ) / double( n_try_diag[14] ) << " %)\n";
	if( n_try_offd[18] > 0 ) std::cout << "std-PIMC MultiMove(G):\t" << n_accept_offd[18] << " of " << n_try_offd[18] << " ( " << 100.0* double( n_accept_offd[18] ) / double( n_try_offd[18] ) << " %)\n";
	
	
	
	std::cout << "#########################################################\n";
	std::cout << "Z_cnt: " << z_cnt << "\tG_cnt: " << g_cnt << "\tG/Z: " << double( g_cnt ) / double( z_cnt ) << "\n";
	std::cout << "m_max_deform: " << m_max_deform << "\t m_max_open: " << m_max_open << "\n";

	if( simulation_type >= n_multi ) // special infos for standard PIMC 
	{
		std::cout << "diag: " << ensemble->diag << "\t G-species: " << ensemble->G_species << "\t total_sign: " << ensemble->get_sign() << "\n";	
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			std::cout << "N[" << iSpecies << "] = " << ensemble->species_config[ iSpecies ].beadlist[ 0 ].size() << "\t";
		}
		std::cout << "\n";
	}

}



// Perform a Monte Carlo update:
template <class inter> void update_PBPIMC<inter>::update()
{
	double choice = rdm(); // random number to choose an update
	
	double ans = 0.0;
	int update_id = 0;
	int accept;
	
	if(debug) std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

	int diag_crit = ensemble->diag; // Are we in the diagonal or off-diagonal sector?

	if( debug ) std::cout << "diag_crit: " << diag_crit << "\n";
	
	if( diag_crit ) // Choose from the diagonal update list:
	{
		z_cnt++; // increase number of executed moves in the Z sector
		
		// Select an update_id:
		for(auto c=prob_diag.begin();c!=prob_diag.end();c++)
		{
			if( ( choice >= ans ) && ( choice <= ans+(*c) ) ) break;
			update_id++;
			ans += (*c);
		}
		
		if( update_id >= n_diag )
		{
			std::cout << "Error in update(), update_id out of list: " << update_id << "\n";
			exit(0);
		}
		
		if(debug) std::cout << "diag, update_id: " << update_id << "\n";
		
		// Execute the selected update:
		switch(update_id)
		{
			case 0:
				accept = universal_deform.execute( m_max_deform ); // PB-PIMC
				break;
			case 1:
 				accept = open.execute( m_max_open, c_bar * prob_offd[1]/prob_diag[1] ); // PB-PIMC
				break;
			case 2:
				std::cout << "1.\n";
				exit(1);
				break;
			case 3:
				std::cout << "2.\n";
				exit(1);
				break;
			case 4:
   				std::cout << "3.\n";
				exit(1);
				break;
			case 5:
				accept = standard_PIMC_deform.execute( m_max_deform, false ); // only update the electrons
				break;
			case 6: 
				accept = standard_PIMC_insert.execute( m_max_open, c_bar, prob_diag[6], prob_offd[5] );
				break;
			case 7:
				accept = standard_PIMC_open.execute( m_max_open, c_bar, prob_diag[7], prob_offd[9] );
				break;
			case 8:
				accept = standard_PIMC_move.execute( ensemble->params.move_scale, false ); // only update the electrons
				break;
			case 9:
				std::cout << "StBl, empty.\n";
				exit(1);
				break;
			case 10:
				accept = standard_PIMC_offdiag_open.execute( m_max_open, c_bar, prob_diag[10], prob_offd[13] );
				break;
			case 11:
				accept = standard_PIMC_deform.execute( m_max_deform, true ); // only update the protons
				break;
			case 12:
				accept = standard_PIMC_move.execute( ensemble->params.move_scale, true ); // only update the protons
				break;
			case 14:
				accept = standard_PIMC_multi_move.execute( ensemble->params.move_scale );
				break;
		}
		
		if(debug)
		{
			std::cout << "accept: " << accept << "\n";
			make_check();
		}
		
		// Update bookkeeping of acceptance ratios
		n_try_diag[ update_id ]++;
		n_accept_diag[ update_id ] += accept;
		
	}
	else  // Choose from the off-diagonal update list:
	{
		g_cnt++; // Increase the number of executed moves in G-sector
		
		// Select an update_id:
		for(auto c=prob_offd.begin();c!=prob_offd.end();c++)
		{
			if( ( choice >= ans ) && ( choice <= ans+(*c) ) ) break;
			update_id++;
			ans += (*c);
		}
		
		if( update_id >= n_offd )
		{
			std::cout << "Error in update(), update_id out of list: " << update_id << "\n";
			exit(0);
		}
		
		if(debug) std::cout << "offd, update_id: " << update_id << "\n";
		
		// Execute the selected update
		switch(update_id)
		{
			case 0:
 				accept = universal_deform.execute( m_max_deform ); // PB-PIMC
				break;
			case 1:
 				accept = close.execute( m_max_open, 1.0 / (c_bar * prob_offd[1]/prob_diag[1]), debug  );  // PB-PIMC
				break;
			case 2: 
				std::cout << "Update not implemented!\n";
				exit(1);
				break;
			case 3:
 				std::cout << "Update not implemented!\n";
				exit(1);
				break;
			case 4:
 				accept = swap.execute( debug );  // PB-PIMC
				break;
			case 5: 
				accept = standard_PIMC_remove.execute( m_max_open, c_bar, prob_offd[5], prob_diag[6] );
				break;
			case 6: 
				accept = standard_PIMC_advance.execute( m_max_deform );
				break;
			case 7:
				accept = standard_PIMC_recede.execute( m_max_deform );
				break;
			case 8:
				accept = standard_PIMC_deform.execute( m_max_deform, false ); // only update the electrons
				break;
			case 9: 
				accept = standard_PIMC_close.execute( m_max_open, c_bar, prob_diag[7], prob_offd[9] );
				break;
			case 10:
				accept = standard_PIMC_swap.execute( m_max_open );
				break;
			case 11:
				accept = standard_PIMC_move.execute( ensemble->params.move_scale, false ); // only update the electrons
				break;
			case 12:
				accept = standard_PIMC_wriggle.execute( m_max_deform );
				break;
			case 13:
				accept = standard_PIMC_offdiag_close.execute( m_max_open, c_bar, prob_diag[10], prob_offd[13] );
				break;
			case 14:
				accept = standard_PIMC_deform.execute( m_max_deform, true ); // only update the protons
				break;
			case 15:
				accept = standard_PIMC_move.execute( ensemble->params.move_scale, true ); // only update the protons
				break;
			case 18:
				accept = standard_PIMC_multi_move.execute( ensemble->params.move_scale );
				break;
		}
		
		if( debug )
		{
			std::cout << "accept: " << accept << "\n";
			make_check();
		}
		
		// Update bookkepping of acceptance ratios
		n_try_offd[ update_id ]++;
		n_accept_offd[ update_id ] += accept;
	
		
	} // end Z-G-if-clause

}



// Initialize the update structure:
template <class inter> void update_PBPIMC<inter>::init( update_probs user_probs, worm_configuration* new_config, Parameters* p, config_ensemble* new_ensemble )
{
	pair_tools.init( p );
	
	n_equil = p->n_equil;
	
	
	// Obtain the simulation_type:
	simulation_type = p->simulation_type;
	n_multi = p->n_multi; // number of simulation_types that are not standard PIMC
	
	// Set the pointer to the config (polarized system):
 	config = new_config;
	config->diag = 1;
	
	// Set the pointer to the ensemble of configs (unpolarized, multiple species)
	ensemble = new_ensemble;
	ensemble->diag = 1;
	



	if(p->system_type == 15)
    {
        N_boltz = p->N_spec_2;
    }
	else
    {
	    N_boltz = 0;
    }
	
	
	
	
	// Hard-coded Kelbg hydrogen implementation!
	
	if(( p->system_type == 17 ) || ( p->system_type >= 21 ))
	{
		N_boltz = ensemble->params.N_up + ensemble->params.N_down;
	}
	
	
	
	if( p->system_type >= 21 )
	{
		N_boltz = p->N_spec_2;
		std::cout << "*********************** number of heavy boltzmannons: " << N_boltz << "\n";
	}
	
	
	
	// Execute program in debug mode?
	debug = p->debug;
	// Set initial parameter for the worm algorithm (atm only polarized trap)
	c_bar = p->c_bar;

	
	// Set maximum number of changed beads for updates open (polarized trap) and deform (also unpolarized PBC)
	m_max_open =  int( config->params.n_bead*user_probs.m_max_open_factor*3 ) ;
	m_max_deform =  int( config->params.n_bead*3*user_probs.m_max_deform_factor )-1; // Deform as much as possible
	

	if( simulation_type >= n_multi ) // For standard PIMC simulation_types, m_max does not need the factor 3
	{
		m_max_deform = int( config->params.n_bead * user_probs.m_max_deform_factor ); // Allow to deform as much as possible
		m_max_open = int( config->params.n_bead * user_probs.m_max_open_factor ); // Could be adjusted?
	}
	
	std::cout << "m_max_open: " << m_max_open << "\t m_max_deform: " << m_max_deform << "\n";
	

	// ####################################################################################################################################
	// ############################################ Dynamically set appropriate update frequencies:
	// ####################################################################################################################################
	
	prob_diag.assign( n_diag, 0.0 );
	prob_offd.assign( n_offd, 0.0 );
	
	std::cout << "simulation_type: " << simulation_type << "\n";
	
	if( simulation_type >= n_multi ) // Standard PIMC simulation
	{
		switch( simulation_type-n_multi )
		{
			case 0: // standard PIMC (canonical) for boltzmannons
				prob_diag[5] = 1.0; // Deform update (also for the boltzmannons)
				break;
			case 1: // standard worm algorithm PIMC
				
				prob_diag[5 ] = user_probs.P_standard_PIMC_deform;
				prob_diag[6 ] = user_probs.P_standard_PIMC_insert;
				prob_diag[7 ] = user_probs.P_standard_PIMC_open;
				prob_diag[8 ] = user_probs.P_standard_PIMC_move;
				
				prob_offd[5 ] = user_probs.P_standard_PIMC_remove;
				prob_offd[6 ] = user_probs.P_standard_PIMC_advance;
				prob_offd[7 ] = user_probs.P_standard_PIMC_recede;
				prob_offd[8 ] = user_probs.P_standard_PIMC_deform_offdiag;
				prob_offd[9 ] = user_probs.P_standard_PIMC_close;
				prob_offd[10] = user_probs.P_standard_PIMC_swap;
				prob_offd[11] = user_probs.P_standard_PIMC_move_offdiag;
// 				prob_offd[12] = user_probs.P_standard_PIMC_wriggle;                // The 'wriggle' update has not been benchmarked as part of the grand-canonical worm algorithm!
				
				break;
			case 2: // off-diagonal PIMC for the Momentum distribution / thermal density matrix
				prob_diag[5 ] = user_probs.P_standard_PIMC_deform;
				prob_diag[8 ] = user_probs.P_standard_PIMC_move;
				prob_diag[10] = user_probs.P_standard_PIMC_offdiag_open;
				prob_diag[11] = user_probs.P_standard_PIMC_deform_proton;
				prob_diag[12] = user_probs.P_standard_PIMC_move_proton;
				
// 				prob_diag[13] = user_probs.P_standard_PIMC_offdiag_open_proton;     // TBD not yet implemented
				
				prob_diag[14] = user_probs.P_standard_PIMC_multi_move;
				
				
				
				prob_offd[8 ] = user_probs.P_standard_PIMC_deform_offdiag;
				prob_offd[10] = user_probs.P_standard_PIMC_swap;
				prob_offd[11] = user_probs.P_standard_PIMC_move_offdiag;
				prob_offd[12] = user_probs.P_standard_PIMC_wriggle;
				prob_offd[13] = user_probs.P_standard_PIMC_offdiag_close;
				prob_offd[14] = user_probs.P_standard_PIMC_deform_offdiag_proton;
				prob_offd[15] = user_probs.P_standard_PIMC_move_offdiag_proton;
				
// 				prob_offd[16] = user_probs.P_standard_PIMC_offdiag_close_proton;    // TBD not yet implemented
// 				prob_offd[17] = user_probs.P_standard_PIMC_wriggle_proton;          // TBD not yet implemented
				
				prob_offd[18] = user_probs.P_standard_PIMC_multi_move_offdiag;
				
				break;
		}
	}
	else // non-standard PIMC simulation
	{
		switch( simulation_type )
		{
			case 0: // PB-PIMC (atm without the worm algorithm, i.e, without the off-diagonal sector, prob_diag[1] is for the 'open' update
				prob_diag[0] = user_probs.P_PBPIMC_deform;// 1.0; // HEG_universal_deform, works both for PBC or the trap
				prob_diag[1] = user_probs.P_PBPIMC_open; // 1.0; // PB-PIMC Open
				
				prob_offd[0] = user_probs.P_PBPIMC_deform_offdiag;// 0.0; // deform in the G-Sector
				prob_offd[1] = user_probs.P_PBPIMC_close;// 1.0; // PB-PIMC Close
				prob_offd[4] = user_probs.P_PBPIMC_swap;
				break;
		}
	}


	// normalize the update-probs:
	normalize_probs();
	
	
	
	
	
	
	
	

	// If there are spin-down fermions, n_species is put automatically to 2
	// TBD: generalization to arbitrary number of particle species!
	ensemble->n_species = 1;
	ensemble->params.n_species = 1;
	
	if( ensemble->params.N_down > 0 )
	{
		ensemble->n_species = 2; // two species, aka spin-up and -down
		ensemble->params.n_species = 2;
	}
	if (N_boltz > 0 )
    {
	    ensemble->n_species += 1;
	    ensemble->params.n_species += 1;
    }

	// Update information of the ensemble of species:
	ensemble->params.N = -1; // number of particles per species
	
	ensemble->N_tot = p->N; // total number of particles
	int N_tot = ensemble->N_tot;
	std::cout << "total number of electrons: " << ensemble->N_tot << std::endl;
	(*config).params = (*ensemble).params;
	
	
	
	
	// for standard PIMC simulation_types
	if( ensemble->params.simulation_type >= ensemble->params.n_multi )
	{
		config->params.N = N_tot;
		std::cout << "N: " << config->params.N  << "\t simulation_type: " << simulation_type << "\t n_multi: " << n_multi << std::endl;
	}
	

	// Create a vector with the particle number of each configuration, for now by hand
	// TBD: maybe variable species number, which is initialized by external file
	std::vector<int> N_vector;
	
	N_vector.push_back( ensemble->params.N_up );
	N_vector.push_back( ensemble->params.N_down );
    N_vector.push_back( N_boltz );
	
	
	
	
	
	
	
	
	
	
	
	
	
	// #############################################################################################################################
	// ################# Initialize all data structures depending on the simulation_type, create initial config ####################
	// #############################################################################################################################
	
	if( simulation_type < n_multi ) // Initialization for the non-standard PIMC simulation_types:
	{
		// #############################################################################################################################
		// ################# Create a default configuration for each particle species ##################################################
		// #############################################################################################################################

		for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
		{
			worm_configuration config;
			config.params = (*p);
			
			// Assign the approriate particle number to the config:
			config.params.N = N_vector[ iSpecies ];
		
			Matrix debug_matrix;
			debug_matrix.init( config.params.N, config.params.N );
			
			// Extend the constantly allocated debug vector:
			debug_matrix_vector.push_back( debug_matrix );
			
			// Each particle species config. has its own sign and determinant/matrix, as well as information about G/Z
			config.my_sign = 1.0;

			config.head_id = -1;
			config.tail_id = -1;
			config.diag = 1; // Start in the diagonal sector for all species
		
			// 'Empty' vector for the beadlists
			std::vector<int>empty(config.params.N, 0);
			
			std::cout << "Create a dummy model config for species " << iSpecies << "\n";
		
			// Fill data structure for all propagators
			for(int l=0;l<config.params.n_bead;l++)
			{
				// the initial config is diagonal
				config.next_id_list.push_back(-1);
				config.next_id_list_A.push_back(-1);
				config.next_id_list_B.push_back(-1);
			
				config.beadlist.push_back( empty );
				config.beadlist_A.push_back( empty );
				config.beadlist_B.push_back( empty );
		
				config.matrix_list.push_back( debug_matrix );
				config.matrix_list_A.push_back( debug_matrix );
				config.matrix_list_B.push_back( debug_matrix );
			
				config.determinant_list.push_back( 42.0 );
				config.determinant_list_A.push_back( 42.0 );
				config.determinant_list_B.push_back( 42.0 );
			
				config.sign_list.push_back( 1.0 );
				config.sign_list_A.push_back( 1.0 );
				config.sign_list_B.push_back( 1.0 );
			} // end loop over all propagators for the lists
		
			// Create a 'classical' config with straight line trajectories
			std::cout << "Creating initial classic config.\n";
		
			// Interaction is tmp, solely to adjust the coordinate of initial config:
			inter my_interaction;
			my_interaction.init( &config.params );
		
			for(int i=0;i<config.params.N;i++) // Loop over all N particles
			{
				// Initial coords. of each particle are randomly choosen inside 0.33*length cube
				std::vector<double>nc;
				for(int iDim=0;iDim<config.params.dim;iDim++)
				{
					double rdm_coord = (2.0*rdm()-1.0)*config.params.length*0.33;
					
					// Adjust the coord:
					rdm_coord = my_interaction.adjust_coordinate( rdm_coord );
					nc.push_back(rdm_coord);
				}
			
			
				for(int l=0;l<config.params.n_bead;l++) // loop over all propagators of a particular particle
				{
					Bead j(l,nc);
				
					// Calculate IDs inside beads[]
					int main_id = config.beads.size();
					int id_A = main_id + 1;
					int id_B = main_id + 2;
				
					// Add the new bead to the beads[]
					config.beads.push_back(j);
					config.beads.push_back(j);
					config.beads.push_back(j);
					
					// Update the beadlists
					config.beadlist[l][i] = main_id;
					config.beadlist_A[l][i] = id_A;
					config.beadlist_B[l][i] = id_B;
				
					// Calculate the imaginary time position of the beads:
					double epsilon = config.params.epsilon;
					double real_time_main = epsilon * double(l);
					double real_time_A = real_time_main + epsilon * config.params.t1;
					double real_time_B = real_time_A + epsilon * config.params.t1;
				
					// Set the imaginary time of the beads:
					config.beads[main_id].set_real_time( real_time_main );
					config.beads[id_A].set_real_time( real_time_A );
					config.beads[id_B].set_real_time( real_time_B );
				
					// Set the particle ID number of the beads, they belong to particle 'i'
					config.beads[main_id].set_number(i);
					config.beads[id_A].set_number(i);
					config.beads[id_B].set_number(i);
				
				
				} // end loop over all propagators

			} // end loop over all particles
		
			// Add the dummy config to the ensemble:
			ensemble->species_config.push_back( config );
		
		} // end loop over all species
		
		std::cout << "All species have been initialized.\n";


		
	}
	else // Initialization for the standard PIMC simulation_types:
	{
		std::cout << "N_species: " << ensemble->params.n_species << "\n";

		// Create two vectors for the species dependent charges (1st and 2nd are equal)
		std::vector<double> spec_charge = {p->charge_spec_1, p->charge_spec_1, p->charge_spec_2};
		std::vector<double> spec_mass = {p->mass_spec_1, p->mass_spec_1, p->mass_spec_2};
		
		
		// loop over all particle-species:
		for(int iSpecies=0;iSpecies<ensemble->params.n_species;iSpecies++)
		{

			// Create a new worm_configuration for species 'iSpecies'
			worm_configuration new_config;
			new_config.Npp = 0; // No pair-permutation in the initial system of classical particles
			
			// Copy the simulation parameters
			new_config.params = ensemble->params;
			
			// Assign the appropriate particle number for iSpecies:
			new_config.params.N = N_vector[ iSpecies ];
		
			// Start in the diagonal sector
			new_config.head_id = -1;
			new_config.tail_id = -1;
			new_config.diag = 1;	
			new_config.my_sign = 1.0;
		
			// Interaction is tmp, solely to adjust the coordinate of initial config:
			inter my_interaction;
			my_interaction.init( &new_config.params );
			
			std::vector<int>empty( new_config.params.N, 0 );
			new_config.beadlist.assign( new_config.params.n_bead, empty ); // Create an empty list of bead-IDs


			for(int i=0;i<new_config.params.N;i++) // loop over all particles of iSpecies
			{
				// Start with classical particles (straight line), same coords on all time slices
				std::vector<double>my_coords( new_config.params.dim, 0.0 );
				for(int iDim=0;iDim<new_config.params.dim;iDim++)
				{
					my_coords[ iDim ] = (2.0*rdm()-1.0)*new_config.params.length/3.33;
					my_coords[ iDim ] = my_interaction.adjust_coordinate( my_coords[ iDim ] );
				}
				
				for(int iSlice=0;iSlice<new_config.params.n_bead;iSlice++) // loop over all time slices
				{
					double my_real_time = iSlice * new_config.params.epsilon; // obtain the real imaginary time
					
					Bead j( iSlice, my_coords, spec_charge[iSpecies], spec_mass[iSpecies]); // create a model bead with the above coords
					j.set_species( iSpecies );
					
					// For classical particles, all diffusion matrix elements are equal
					double my_diffusion_element = my_interaction.rho( &j, &j, 1.0 ); // for standard PIMC, scale always equals unity
					j.set_diffusion_element( my_diffusion_element );
					
					
					j.set_real_time( my_real_time ); // set the real imaginary time
					j.set_time( iSlice ); // set the index of the time slice
					
					int my_bead_id = new_config.beads.size(); // obtain the bead id of the current bead
					
					// obtain the id of the next bead
					int my_next_id = my_bead_id + 1;
					if( iSlice == new_config.params.n_bead-1 )
					{
						my_next_id = my_next_id - new_config.params.n_bead;
					}
					j.set_next_id( my_next_id );
					
					// obtain the id of the previous bead
					int my_prev_id = my_bead_id - 1;
					if( iSlice == 0 )
					{
						my_prev_id = my_prev_id + new_config.params.n_bead;
					}
					j.set_prev_id( my_prev_id );
				
					// push the model bead back to the beads-vector of the worm_configuration:
					new_config.beads.push_back( j );
					
					// update the beadlist-vector of the worm_configuration:
					new_config.beadlist[ iSlice ][ i ] = my_bead_id;
					
				} // end loop iSlice
			} // end loop i (all particles)
			
			
			std::cout << "Created initial config for species: " << iSpecies << "\n";
			
			// add the new config to the ensemble of species:
			ensemble->species_config.push_back( new_config );
		
		} // end loop iSpecies

	} // end non-standard PIMC system_type condition for the initialization
		
		
	// Update the config pointer from the inter_tools
	inter_tools.init( config );
		
		
	
	
	
	
	
	
	
	// #############################################################################################################################
	// ################# Initialize the updates ####################################################################################
	// #############################################################################################################################

	if( simulation_type >= n_multi )
	{
		switch( simulation_type-n_multi ) // standard PIMC simulation_types
		{
			case 0: // standard PIMC (canonical) for boltzmannons 
				standard_PIMC_deform.init( debug, ensemble );
				break;
			case 1: // standard PIMC worm algorithm
				standard_PIMC_deform.init( debug, ensemble );
				standard_PIMC_insert.init( debug, ensemble );
				standard_PIMC_remove.init( debug, ensemble );
				standard_PIMC_advance.init( debug, ensemble );
				standard_PIMC_recede.init( debug, ensemble );
				standard_PIMC_open.init( debug, ensemble );
				standard_PIMC_close.init( debug, ensemble );
				standard_PIMC_swap.init( debug, ensemble );
				standard_PIMC_move.init( debug, ensemble );
				standard_PIMC_wriggle.init( debug, ensemble );
				
				standard_PIMC_multi_move.init( debug, ensemble );
				
				break;
			case 2: // standard PIMC worm algorithm (momentum distribution, offdiag)
				standard_PIMC_deform.init( debug, ensemble );
				standard_PIMC_swap.init( debug, ensemble );
				standard_PIMC_move.init( debug, ensemble );
				standard_PIMC_wriggle.init( debug, ensemble );
				standard_PIMC_offdiag_close.init( debug, ensemble );
				standard_PIMC_offdiag_open.init( debug, ensemble );
				
				standard_PIMC_multi_move.init( debug, ensemble );
				break;
		}
	}
	else // non-standard PIMC simulation type 
	{
		switch( simulation_type )
		{
			case 0: // PB-PIMC
				universal_deform.init( debug, ensemble );
				open.init( debug, ensemble );
				close.init( debug, ensemble );
				swap.init( debug, ensemble );
				break;
		}
	}
	
	std::cout << "The updates have been initialized.\n";

	
	// TBD TBD TBD Diffusion should only be handled by the species vector
	// Update the config pointer from the diffusion_toolbox
	diffusion_tools.init( config );

	// Update the config pointer from the force_toolbox
	force_tools.init( config );
	
	// For the make_check_pbpimc()
	if( simulation_type < n_multi )
	{
		for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
		{
			diffusion_toolbox< inter > tmp_diffusion;
			tmp_diffusion.init( &(*ensemble).species_config[ iSpecies ] );
			diffusion_species.push_back( tmp_diffusion );
		}
	}


	
	
	
	
	
	// #############################################################################################################################
	// ################# Obtain initial energy etc. ################################################################################
	// #############################################################################################################################
	
	if( simulation_type >= n_multi ) // obtain initial stuff for standard PIMC
	{
		if( ensemble->params.debug )
		{
			// Print the initial config (for all species) to the disk:
			ensemble->print_standard_PIMC_ensemble_beadlist( "initial" );
			ensemble->print_standard_PIMC_ensemble_paths( "initial" );
		}
		
		// obtain total energy of the initial configuration:
		ensemble->total_energy = inter_tools.ensemble_standard_PIMC_get_energy( ensemble );
		std::cout << "initial PIMC ensemble energy: " << ensemble->total_energy << "\n";
		
		ensemble->total_diffusion_product = ensemble->calculate_diffusion_product();
		std::cout << "initial diffusion_product: " << ensemble->total_diffusion_product << "\n";
		
		
		std::vector<double> IAV3 = pair_tools.get_total_pair_action( ensemble );		
		std::cout << "Sanity-check3: " << IAV3[0] << "\t" << IAV3[1] << "\t" << IAV3[2] << "\t" << IAV3[3] << "\n";
		
		
		std::vector<double> initial_action_vector = pair_tools.get_total_pair_action( ensemble );
		
		ensemble->total_pair_action = initial_action_vector[0];
		std::cout << "initial pair action: " << ensemble->total_pair_action << "\n";
		
		ensemble->total_pair_derivative = initial_action_vector[1];
		std::cout << "initial pair derivative: " << ensemble->total_pair_derivative << "\n";
		
		
		ensemble->total_m_e_pair_derivative = initial_action_vector[2];
		ensemble->total_m_p_pair_derivative = initial_action_vector[3];
		
		std::cout << "initial m_e_derivative: " << ensemble->total_m_e_pair_derivative << "\tinitial m_p_derivative: " << ensemble->total_m_p_pair_derivative << "\n";
		
		
		
		
		std::vector<double> IAV2 = pair_tools.get_total_pair_action( ensemble );		
		std::cout << "Sanity-check2: " << IAV2[0] << "\t" << IAV2[1] << "\t" << IAV2[2] << "\t" << IAV2[3] << "\n";
		
		
		
		std::vector<double> initial_ext_pot_action_vector = pair_tools.get_total_ext_pot_action( ensemble );
		
		
		
		std::cout << "*******vector_size: " << initial_ext_pot_action_vector.size() << "\n";
		
		
		std::vector<double> IAV442 = pair_tools.get_total_pair_action( ensemble );	
		std::cout << "---Sanity-check442: " << IAV442[0] << "\t" << IAV442[1] << "\t" << IAV442[2] << "\t" << IAV442[3] << "\n";

		
		ensemble->total_ext_pot_action = initial_ext_pot_action_vector[0];
		std::cout << "initial ext_pot_action: " << ensemble->total_ext_pot_action;
		
		
		std::vector<double> IAV12 = pair_tools.get_total_pair_action( ensemble );	
		std::cout << "---Sanity-check12: " << IAV12[0] << "\t" << IAV12[1] << "\t" << IAV12[2] << "\t" << IAV12[3] << "\n";

		
		ensemble->total_ext_pot_action_derivative = initial_ext_pot_action_vector[1];
		std::cout << "initial ext_pot_action_derivative: " << ensemble->total_ext_pot_action_derivative << "\n";
		
		
		
		std::vector<double> IAV42 = pair_tools.get_total_pair_action( ensemble );		
		std::cout << "---Sanity-check42: " << IAV42[0] << "\t" << IAV42[1] << "\t" << IAV42[2] << "\t" << IAV42[3] << "\n";
		
		
		
		// obtain the initial total exponent (mu_factor):
		ensemble->total_exponent = 0.0;
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			double iSpec_exp = inter_tools.standard_PIMC_get_exponent( &(*ensemble).species_config[ iSpecies ] );
			ensemble->species_config[ iSpecies ].my_exp = iSpec_exp;
			ensemble->total_exponent += iSpec_exp;
		}
		std::cout << "initial PIMC total_exponent: " << ensemble->total_exponent << "\n";

		
		std::vector<double> IAV = pair_tools.get_total_pair_action( ensemble );		
		std::cout << "Sanity-check: " << IAV[0] << "\t" << IAV[1] << "\t" << IAV[2] << "\t" << IAV[3] << "\n";
		
		
		// Make a check of the initial config:
		make_check_ensemble_standard_PIMC();
		
	}
	else // obtain initial stuff for non-standard PIMC
	{
		
		if( simulation_type == 1 ) // Empty!
		{
			std::cout << "This simulation_type is not implemented in this branch.\n";
			exit(1);

		}
		else // PB-PIMC
		{
			// ###############################################################################################################################################
			// Obtain initial forces, energy etc. for PB-PIMC:

			// Obtain the total energy of all ensembles:
			double total_energy = inter_tools.get_ensemble_energy( ensemble );
			std::cout << "total energy: " << total_energy << std::endl;
			
			// Obtain the total force_sq of all ensembles:
			double total_force_sq = force_tools.get_total_force_sq( ensemble );
			std::cout << "total force_sq: " << total_force_sq << std::endl;
			
			// Calculate the force on each bead and all diffusion matrices (of the entire ensemble)
			for(int i=0;i<ensemble->params.n_bead;i++) // loop over all propagators
			{				
				double f_on_the_fly = 0.0;
				
				for(int iSpecies=0;iSpecies<ensemble->n_species;iSpecies++) // loop over all species
				{
					std::cout << "Init iSpecies force/diffusion: " << iSpecies << "\n";
				
					// Calculate the entire force on each particle:
					for(int k=0;k<ensemble->species_config[iSpecies].params.N;k++) // loop over particles from iSpecies
					{
						int main_id = ensemble->species_config[iSpecies].beadlist[i][k];
						int A_id = ensemble->species_config[iSpecies].beadlist_A[i][k];
						int B_id = ensemble->species_config[iSpecies].beadlist_B[i][k];
						
						std::vector<double>nforce( ensemble->species_config[iSpecies].params.dim, 0.0); // will contain the total force
						std::vector<double>tforce( ensemble->species_config[iSpecies].params.dim, 0.0); // ancilla memory
						
						std::vector<std::vector <int> > *blist, *blist_A, *blist_B;
						std::vector<int>* nlist, *nlist_A, *nlist_B;
						
						blist = &(*ensemble).species_config[iSpecies].beadlist;
						blist_A = &(*ensemble).species_config[iSpecies].beadlist_A;
						blist_B = &(*ensemble).species_config[iSpecies].beadlist_B;
						
						nlist = &(*ensemble).species_config[iSpecies].next_id_list;
						nlist_A = &(*ensemble).species_config[iSpecies].next_id_list_A;
						nlist_B = &(*ensemble).species_config[iSpecies].next_id_list_B;
						
						Bead *main_bead = &(*ensemble).species_config[iSpecies].beads[main_id];
						Bead *A_bead = &(*ensemble).species_config[iSpecies].beads[A_id];
						Bead *B_bead = &(*ensemble).species_config[iSpecies].beads[B_id];
						
						// main
						force_tools.ensemble_ptr_force_on_bead(ensemble, iSpecies, 3, blist, main_id, main_bead, nlist, &nforce, &tforce);
						ensemble->species_config[iSpecies].beads[main_id].set_force( nforce );
						f_on_the_fly += ensemble->params.a1 * vector_square( nforce );
						
						// A
						force_tools.ensemble_ptr_force_on_bead(ensemble, iSpecies, 1, blist_A, A_id, A_bead, nlist_A, &nforce, &tforce);
						ensemble->species_config[iSpecies].beads[A_id].set_force( nforce );
						f_on_the_fly += (1.0-2.0*ensemble->params.a1) * vector_square( nforce );
						
						// B
						force_tools.ensemble_ptr_force_on_bead(ensemble, iSpecies, 2, blist_B, B_id, B_bead, nlist_B, &nforce, &tforce);
						ensemble->species_config[iSpecies].beads[B_id].set_force( nforce );
						f_on_the_fly += ensemble->params.a1 * vector_square( nforce );
						
					} // end loop over all particles from iSpecies
				
					// Set kinetic matrices, determinants etc.
					int N = ensemble->species_config[iSpecies].params.N;
				
					Matrix k_main, k_A, k_B;
					k_main.init(N, N);
					k_A.init(N, N);
					k_B.init(N, N);
				
					double main_scale = (*ensemble).params.t1;
					double A_scale = main_scale;
					double B_scale = 2.0* (*ensemble).params.t0;

					diffusion_species[iSpecies].get_matrix(&k_main, &(*ensemble).species_config[iSpecies].beadlist[i], &(*ensemble).species_config[iSpecies].beadlist_A[i], -1, -1, main_scale);
					diffusion_species[iSpecies].get_matrix(&k_A, &(*ensemble).species_config[iSpecies].beadlist_A[i], &(*ensemble).species_config[iSpecies].beadlist_B[i], -1, -1, A_scale);
				
					int j = i + 1;
					if( j == ensemble->params.n_bead ) j = 0;
				
					diffusion_species[iSpecies].get_matrix(&k_B, &(*ensemble).species_config[iSpecies].beadlist_B[i], &(*ensemble).species_config[iSpecies].beadlist[j], -1, -1, B_scale);

					std::cout << "k_main******************************************************************************************\n";
					k_main.print_to_screen();
					std::cout << "k_A******************************************************************************************\n";
					k_A.print_to_screen();
					std::cout << "k_B******************************************************************************************\n";
					k_B.print_to_screen();

					double td_main = k_main.determinant();
					double td_A = k_A.determinant();
					double td_B = k_B.determinant();

					double tsign_main = 1.0;
					if(td_main < 0.0)
					{
						tsign_main = -1.0;
						td_main = -1.0 * td_main;
					}
					
					double tsign_A = 1.0;
					if(td_A < 0.0)
					{
						tsign_A = -1.0;
						td_A = -1.0 * td_A;
					}
					
					double tsign_B = 1.0;
					if(td_B < 0.0)
					{
						tsign_B = -1.0;
						td_B = -1.0 * td_B;
					}

					ensemble->species_config[iSpecies].determinant_list[i] = td_main;
					ensemble->species_config[iSpecies].determinant_list_A[i] = td_A;
					ensemble->species_config[iSpecies].determinant_list_B[i] = td_B;
					
					ensemble->species_config[iSpecies].sign_list[i] = tsign_main;
					ensemble->species_config[iSpecies].sign_list_A[i] = tsign_A;
					ensemble->species_config[iSpecies].sign_list_B[i] = tsign_B;
					
					ensemble->species_config[iSpecies].matrix_list[i].copy(&k_main);
					ensemble->species_config[iSpecies].matrix_list_A[i].copy(&k_A);
					ensemble->species_config[iSpecies].matrix_list_B[i].copy(&k_B);
				
				} // end loop over all species
			} // end loop over all propagators
			
			ensemble->total_force_sq = total_force_sq;
			ensemble->total_energy = total_energy;
			
			
			// #################################################
			// #### Special options for Stochastic-blocking ####
			// #################################################
			
			if( p->simulation_type == 2 )
			{
				// ### This knowledge has been lost forever :(
			}

		} // end PB-PIMC or Stochastic-blocking if-condition
		
	} // end standard PIMC or non-standard PIMC if-condition
	

	// #############################################################################################################################
	// ################# Perform Ewald benchmarks for the HEG ######################################################################
	// #############################################################################################################################
	
	if( ( ensemble->params.debug ) && ( ensemble->params.system_type == 3 || ensemble->params.system_type == 14 || ensemble->params.system_type == 13 || ensemble->params.system_type >= 16 ))
	{

		// Always produce benchmarks for the Ewald summation (kappa value/ convergence with Ewald level)
		std::cout << "Produce Ewald benchmarks\n";
		
		perturbed_ewald_HEG HEG;
		HEG.init( &(*ensemble).params );
		
		double tol = 1e-6;
		double kappa = (*ensemble).params.kappa_int;
		int max_index = 20;
		
		Bead* a = &(*ensemble).species_config[0].beads[ ensemble->species_config[0].beadlist[0][0] ];
		Bead* b = &(*ensemble).species_config[0].beads[ ensemble->species_config[0].beadlist[0][1] ];
		Bead* c, *d;
		
		if( ensemble->n_species > 1 )
		{
			c = &(*ensemble).species_config[1].beads[ ensemble->species_config[1].beadlist[0][0] ];
			d = &(*ensemble).species_config[1].beads[ ensemble->species_config[1].beadlist[0][1] ];
		}
		else
		{
			c = &(*ensemble).species_config[0].beads[ ensemble->species_config[0].beadlist[0][2] ];
			d = &(*ensemble).species_config[0].beads[ ensemble->species_config[0].beadlist[0][3] ];
		}
		
		// Make an overview plot for different kappas
		std::cout << "Make kappa overview-plot:\n";
		std::fstream f_a;
		f_a.open( "Ewald_int.dat", std::ios::out );
		f_a.precision(20);
		for(double kcnt=-12.0;kcnt<12.0;kcnt+=10.1)
		{
			double my_kappa = pow( 2.0, kcnt ); // kappa values should be distributed over several orders of magnitude
			int r_index_a, r_index_b;
		
			double result1 = HEG.Ewald_pair(a, b, tol, &r_index_a, my_kappa, max_index);
			double result2 = HEG.Ewald_pair(d, c, tol, &r_index_b, my_kappa, max_index);


			
			int f_index;
			double f1 = HEG.Ewald_force(a, b, tol, &f_index, my_kappa, max_index);

			f_a << my_kappa << "\t" << result1 << "\t" << r_index_a << "\t" << result2 << "\t" << r_index_b << "\t" << f1 << "\t" << f_index << "\n";
	
		} // end loop over all kappas
		f_a.close();

		// Make a convergence plot for three different kappa_int, including the ones specified in the config file
		f_a.open( "Ewald_int_conv.dat", std::ios::out );
		
		double kappa1 = 0.1*ensemble->params.kappa_int;
		double kappa2 = ensemble->params.kappa_int;
		double kappa3 = 10.0*ensemble->params.kappa_int;
		
		int r_index_cpy = ensemble->params.r_index; // Dave the pre-defined r_index from the config. file

// 		for(int i=0;i<40;i++) // Go to Ewald level 40
		for(int i=0;i<4;i++)
		{
			ensemble->params.kappa_int = kappa1;
 			double result1 = HEG.Ewald_pair_index(a, b, i);
			
			ensemble->params.kappa_int = kappa2;
 			double result2 = HEG.Ewald_pair_index(a, b, i);

			int f_index;
			double f_result2 = HEG.Ewald_force_cnt(a, b, 1e-30, &f_index, kappa2, i+1);
			double f_result1 = HEG.Ewald_force_cnt(a, b, 1e-30, &f_index, kappa1, i+1);
			double f_result3 = HEG.Ewald_force_cnt(a, b, 1e-30, &f_index, kappa3, i+1);
			
			ensemble->params.kappa_int = kappa3;
 			double result3 = HEG.Ewald_pair_index(a, b, i);

			f_a << i << "\t" << result1 << "\t" << result2 << "\t" << result3 << "\t" << f_result1 << "\t" << f_result2 << "\t" << f_result3 << std::endl;
		}
		
		f_a.close();
		
		// Reset kappa_int and the r_index to the pre-defined values from the config file
		ensemble->params.kappa_int = kappa2;


		
	} // End of Ewald summation benchmarks
	
	
	
	
	std::cout << "~~~~~~~~~~~~~~~~~~~~~system_type: " << ensemble->params.system_type << "\n";
	
	
	// ### Do an ext_pot() analysis for the Ion-snapshot classes:
	if( ensemble->params.system_type == 16 ) // Kelbg
	{
		std::cout << "************************ do an ext_pot() analysis for the snapshot ******************\n";
		std::cout << "****ensemble->params.ions.get_num_ions(): " << ensemble->params.ions.get_num_ions() << "\n";
		std::fstream f_analysis;
		f_analysis.open( "Snapshot_analysis.dat", std::ios::out );
		
		Kelbg_ion_snapshot SNAP;
		SNAP.init( &(*ensemble).params );
		
		std::vector<double> c(3,0.0);
		
		// Loop over N_grid steps through the simulation box
		int N_grid = 500;
		for(int iGrid=0;iGrid<N_grid;iGrid++)
		{
			c[0] = (iGrid+0.5)/double(N_grid) * ensemble->params.length * 1.0;
			Bead b( 0, c );
			
			std::vector<double> outcome = SNAP.ext_pot_info( &b );
			
			f_analysis << c[0];
			for(auto q=outcome.begin();q!=outcome.end();q++)
			{
				f_analysis << "\t" << (*q);
			}
			f_analysis << "\n";
			
		} // end loop iGrid
		
		
		f_analysis.close();
	}
	
	
		// ### Do an ext_pot() analysis for the Ion-snapshot classes:
	if( ensemble->params.system_type == 13 ) // HTDM
	{
		std::cout << "************************ do an ext_pot() analysis for the snapshot ******************\n";
		std::cout << "****ensemble->params.ions.get_num_ions(): " << ensemble->params.ions.get_num_ions() << "\n";
		std::fstream f_analysis, f_offdiag, f_analysis2;
		f_analysis.open( "HTDM_Snapshot_analysis.dat", std::ios::out );
		f_analysis2.open( "combined_HTDM_Snapshot_analysis.dat", std::ios::out );
		f_offdiag.open( "offdiag_HTDM_Snapshot_analysis.dat", std::ios::out );
		
		ion_snapshot SNAP;
		SNAP.init( &(*ensemble).params );
		
		std::vector<double> c(3,0.0);
		
		// Loop over N_grid steps through the simulation box
		int N_grid = 500;
		for(int iGrid=0;iGrid<N_grid;iGrid++)
		{
			c[0] = (iGrid+0.5)/double(N_grid) * ensemble->params.length * 1.0;
			Bead b( 0, c );
//  			std::vector<double> d = { c[0]+sqrt(2.0*pi*ensemble->params.epsilon), -0.0*sqrt(2.0*pi*ensemble->params.epsilon), -0.0*sqrt(2.0*pi*ensemble->params.epsilon) };
 			std::vector<double> d = { c[0]+0*sqrt(2.0*pi*ensemble->params.epsilon), sqrt(2.0*pi*ensemble->params.epsilon), -0.0*sqrt(2.0*pi*ensemble->params.epsilon) };
			
			Bead bb( 0, d );
			
			std::vector<double> outcome = SNAP.ext_pot_info( &b );
			std::vector<double> outcome2 = SNAP.ext_pot_info( &bb );
			std::vector<double> outcome_offdiag = SNAP.ext_pot_action_info( &b, &bb );
			
			f_analysis << c[0];
			for(auto q=outcome.begin();q!=outcome.end();q++)
			{
				f_analysis << "\t" << (*q);
			}
			f_analysis << "\n";
			
			f_analysis2 << c[0] << "\t" << 0.5*(outcome[0]+outcome2[0]) << "\n";
			
			
			f_offdiag << c[0];
			for(auto q=outcome_offdiag.begin();q!=outcome_offdiag.end();q++)
			{
				f_offdiag << "\t" << (*q);
			}
			f_offdiag << "\n";
			
		} // end loop iGrid
		
		
		f_analysis.close();
		f_analysis2.close();
		f_offdiag.close();
	}
	
	
	
	
	
	
	
	
	
	

	// Set acceptance ratio counters:
	n_accept_diag.assign( n_diag, 0 );
	n_try_diag.assign( n_diag, 0 );
	n_accept_offd.assign( n_offd, 0 );
	n_try_offd.assign( n_offd, 0 );





	// Check, if the stored f_sq, energy, determinants, etc. are correct:
	make_check();
	
	std::cout << "END of update_init()\n";

	
	// Adjust c_bar and perform the equilibration:
 	equilibrate( n_equil );

	// Print the simulation parameters to the screen:
 	config->params.print_to_screen();
}



// Normalize the acceptance probabilities:
template <class inter> void update_PBPIMC<inter>::normalize_probs()
{
	// Start with diagonal update list:
	double norm = 0.0;
	int cnt = 0;
	for(auto c=prob_diag.begin();c!=prob_diag.end();c++)
	{
		norm += (*c);
	}
	
	norm = 1.0/norm;
	for(auto c=prob_diag.begin();c!=prob_diag.end();c++)
	{
		(*c) *= norm;
		std::cout << cnt << "\tp_diag: " << (*c) << "\n";
		cnt++;
	}
	
	// proceed with off-diagonal update list:
	norm = 0.0;
	for(auto c=prob_offd.begin();c!=prob_offd.end();c++)
	{
		norm += (*c);
	}
	
	cnt = 0;
	norm = 1.0/norm;
	for(auto c=prob_offd.begin();c!=prob_offd.end();c++)
	{
		(*c) *= norm;
		std::cout << cnt << "\tp_offd: " << (*c) << "\n";
		cnt++;
	}
}



cmake_minimum_required(VERSION 3.0)
project(ISTHAR)

add_executable(ISHTAR main.cpp)


set (CMAKE_CXX_FLAGS "-std=c++17 -fopenmp -O3")


set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")
find_package (GSL)
FIND_PACKAGE( Boost 1.40 COMPONENTS program_options REQUIRED )
find_package(HDF5  COMPONENTS C CXX HL REQUIRED)



set(INCL ${Boost_INCLUDE_DIR})
set(INCL ${INCL} ${GSL_INCLUDE_DIRS})
set(INCL ${INCL} ${HDF5_INCLUDE_DIRS})
set(INCL ${INCL} ${HDF5_CXX_DIRS})
set(INCL ${INCL} ${HDF5_HL_DIRS})


include_directories(${INCL})

set(LIBS ${LIBS} ${GSL_LIBRARIES})
set(LIBS ${LIBS} ${Boost_LIBRARIES})
set(LIBS ${LIBS} ${HDF5_LIBRARIES})
set(LIBS ${LIBS} ${HDF5_CXX_LIBRARIES})
set(LIBS ${LIBS} ${HDF5_HL_LIBRARIES})

include(CTest)

option(BUILD_TESTS "Build test programs" OFF)

if(BUILD_TESTS)
    find_package(Catch2 REQUIRED)
    message(STATUS "Catch2: Found version '${Catch2_VERSION}'")
    message(STATUS "Including -Wall -Wextra -pendantic -Werror")
    set (CMAKE_CXX_FLAGS "-std=c++17 -Wpedantic -Wall -Wextra -g ")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DNUM_OUT")
    include(Catch)
    add_executable(test_main test/test_input.cpp)
    add_executable(test_ion_input test/test_ion_input.cpp)
    add_executable(test_lookup_table test/test_lookup_table.cpp)
    target_link_libraries(test_main Catch2::Catch2 ${LIBS})
    target_link_libraries(test_ion_input Catch2::Catch2 ${LIBS})
    target_link_libraries(test_lookup_table Catch2::Catch2 ${LIBS})
    catch_discover_tests(test_main)
    catch_discover_tests(test_ion_input)
    catch_discover_tests(test_lookup_table Catch2::Catch2)
endif(BUILD_TESTS)

target_link_libraries(ISHTAR ${LIBS})


